﻿using steganography;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using NAudio.Wave;
using System.IO;

namespace TestProject1
{
    
    
    /// <summary>
    ///This is a test class for CoreTest and is intended
    ///to contain all CoreTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CoreTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for createPreviewImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("steganography.exe")]
        public void createPreviewImageTest()
        {
            Bitmap bmp = new Bitmap(100, 100, PixelFormat.Format32bppArgb);
            bmp.Save("source.bmp", ImageFormat.Bmp);
            bmp.Dispose();

            Core_Accessor core = new Core_Accessor();
            //core.crypt.GenerateRSAKey();
            core.Cimage.SavePath = "config.xml";
            core.Cimage = new CoverImage_Accessor("source.bmp");
            core.Cimage.ScatterData = true;

            core.Cimage.UseText = true;
            core.Cimage.HiddenText = "test text";
            core.Cimage.UseEncryption = true;
            core.Cimage.Password = "ditiseengoedpassword";

            bool difference = false;

            string bitmap = core.createPreviewImage(false, difference);

            Bitmap bmp2 = new Bitmap(bitmap);
            bmp2.Save("target.bmp");
            bmp2.Dispose();

            core.RecoverCimage.OutputPath = "result." + core.RecoverCimage.HiddenFile.FileExtension.ToString().ToLower();
            core.RecoverCimage.LoadPath = "config.xml";
            core.RecoverCimage = new CoverImage_Accessor("target.bmp");
            core.RecoverCimage.ScatterData = true;
            core.RecoverCimage.UseEncryption = true;

            core.Recover(MedType.Image);
        }

        /// <summary>
        ///A test for createPreviewImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("steganography.exe")]
        public void createPreviewAudioTest()
        {
            WaveFormat format = WaveFormat.CreateIeeeFloatWaveFormat(100, 1);
            WaveFileWriter writer = new WaveFileWriter("test.wav", format);
            for (int i = 0; i < 1000; i++)
                writer.WriteSample(0);
            writer.Close();

            Core_Accessor core = new Core_Accessor();
            core.Caudio.SavePath = "config.xml";

            core.Caudio.SetFile("test.wav");

            core.Caudio.HiddenText = "test tekst";
            core.Caudio.UseText = true;
            core.Caudio.UseEncryption = true;
            core.Caudio.ScatterData = true;
            core.Caudio.Password = "ditiseengoedpassword";

            string wave = core.createPreviewAudio(false);

            File.Move(wave, "result.wav");

            core.RecoverCaudio.OutputPath = "result." + core.RecoverCaudio.HiddenFile.FileExtension.ToString().ToLower();
            core.RecoverCaudio.LoadPath = "config.xml";
            core.RecoverCaudio.SetFile("result.wav");
            core.RecoverCaudio.ScatterData = true;
            core.RecoverCaudio.UseEncryption = true;

            core.Recover(MedType.Audio);
        }

        /// <summary>
        ///A test for createPreviewImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("steganography.exe")]
        public void createPreviewVideoTest()
        {
            Core_Accessor core = new Core_Accessor();
            core.Cvideo.SavePath = "config.xml";

            core.Cvideo.SetFile(@"P:\Bureaublad\a.avi");

            core.Cvideo.HiddenText = "test tekst";
            core.Cvideo.UseText = true;
            core.Cvideo.UseEncryption = false;
            core.Cvideo.ScatterData = false;
            core.Cvideo.Password = "ditiseengoedpassword";

            string vid = core.createPreviewVideo(false);

            File.Move(vid, "result.avi");

            core.RecoverCvideo.OutputPath = "result." + core.RecoverCvideo.HiddenFile.FileExtension.ToString().ToLower();
            core.RecoverCvideo.LoadPath = "config.xml";
            core.RecoverCvideo.SetFile("result.avi");
            core.RecoverCvideo.ScatterData = false;
            core.RecoverCvideo.UseEncryption = false;

            core.Recover(MedType.Video);
        }

        /// <summary>
        ///A test for createPreviewImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("steganography.exe")]
        public void createPreviewImageTest2()
        {
            Bitmap bmp = new Bitmap(100, 100, PixelFormat.Format32bppArgb);
            bmp.Save("source.bmp", ImageFormat.Bmp);
            bmp.Dispose();

            Core_Accessor core = new Core_Accessor();
            core.Cimage.SetFile("source.bmp");
            core.Cimage.SavePath = "config.xml";

            FileStream stream = new FileStream("sourcefile.txt", FileMode.CreateNew, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream);
            for (int i = 0; i < 30; i++)
                writer.Write("a");
            writer.Close();
            stream.Close();

            core.Cimage.HiddenFile.SetFile("sourcefile.txt");

            core.Cimage.UseText = false;
            core.Cimage.UseEncryption = true;
            core.Cimage.ScatterData = false;
            core.Cimage.UseCompression = false;
            core.Cimage.Password = "";

            string bitmap = core.GeneratePreview(MedType.Image);

            Bitmap bmp2 = new Bitmap(bitmap);
            bmp2.Save("target.bmp");
            bmp2.Dispose();

            Directory.CreateDirectory("result");

            core.RecoverCimage.SetFile("target.bmp");
            core.RecoverCimage.OutputPath = "result\\result." + core.RecoverCimage.HiddenFile.FileExtension.ToString().ToLower();
            core.RecoverCimage.LoadPath = "config.xml";
            core.RecoverCimage.Password = "";
            core.RecoverCimage.ScatterData = false;
            core.RecoverCimage.UseCompression = false;
            core.RecoverCimage.UseCompression = false;
            core.RecoverCimage.UseEncryption = true;

            core.Recover(MedType.Image);
        }

        /// <summary>
        ///A test for createPreviewImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("steganography.exe")]
        public void createPreviewAudioTest2()
        {
            WaveFormat format = WaveFormat.CreateIeeeFloatWaveFormat(100, 1);
            WaveFileWriter writer = new WaveFileWriter("test.wav", format);
            for (int i = 0; i < 1000; i++)
                writer.WriteSample(0);
            writer.Close();

            Core_Accessor core = new Core_Accessor();
            core.Caudio.SavePath = "config.xml";

            core.Caudio.SetFile("test.wav");

            Bitmap bmp = new Bitmap(10, 10);
            bmp.Save("sourcefile.bmp", ImageFormat.Bmp);
            bmp.Dispose();

            core.Caudio.HiddenFile.SetFile("sourcefile.bmp");
            core.Caudio.UseText = false;
            core.Caudio.UseEncryption = false;
            core.Caudio.ScatterData = true;
            core.Caudio.Password = "ditiseengoedpassword";

            string wave = core.createPreviewAudio(false);

            File.Move(wave, "result.wav");

            core.RecoverCaudio.OutputPath = "result." + core.RecoverCaudio.HiddenFile.FileExtension.ToString().ToLower();
            core.RecoverCaudio.LoadPath = "config.xml";
            core.RecoverCaudio.SetFile("result.wav");
            core.RecoverCaudio.ScatterData = true;
            core.RecoverCimage.UseEncryption = false;

            core.Recover(MedType.Audio);
        }

        /// <summary>
        ///A test for createPreviewImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("steganography.exe")]
        public void createPreviewVideoTest2()
        {
            Core_Accessor core = new Core_Accessor();
            core.Cvideo.SavePath = "config.xml";

            core.Cvideo.SetFile(@"P:\Bureaublad\a.avi");

            Bitmap bmp = new Bitmap(10, 10);
            bmp.Save("sourcefile.bmp", ImageFormat.Bmp);
            bmp.Dispose();

            core.Cvideo.HiddenFile.SetFile("sourcefile.bmp");
            core.Cvideo.UseText = false;
            core.Cvideo.UseEncryption = false;
            core.Cvideo.ScatterData = false;
            core.Cvideo.Password = "ditiseengoedpassword";

            string vid = core.createPreviewVideo(false);

            File.Move(vid, "result.avi");

            core.RecoverCvideo.OutputPath = "result." + core.RecoverCvideo.HiddenFile.FileExtension.ToString().ToLower();
            core.RecoverCvideo.LoadPath = "config.xml";
            core.RecoverCvideo.SetFile("result.avi");
            core.RecoverCvideo.ScatterData = false;
            core.RecoverCvideo.UseEncryption = false;

            core.Recover(MedType.Video);
        }
    }
}
