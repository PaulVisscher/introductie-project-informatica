﻿namespace steganography
{
    partial class PreviewOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rbShowOutput = new System.Windows.Forms.RadioButton();
            this.rbShowDifference = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.rbShowOutput, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbShowDifference, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(150, 49);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // rbShowOutput
            // 
            this.rbShowOutput.AutoSize = true;
            this.rbShowOutput.Checked = true;
            this.rbShowOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.rbShowOutput.Location = new System.Drawing.Point(2, 2);
            this.rbShowOutput.Margin = new System.Windows.Forms.Padding(2);
            this.rbShowOutput.Name = "rbShowOutput";
            this.rbShowOutput.Size = new System.Drawing.Size(119, 14);
            this.rbShowOutput.TabIndex = 7;
            this.rbShowOutput.TabStop = true;
            this.rbShowOutput.Text = "Show normal output";
            this.rbShowOutput.UseVisualStyleBackColor = true;
            this.rbShowOutput.CheckedChanged += new System.EventHandler(this.rbShowOutput_CheckedChanged);
            this.rbShowOutput.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.rbShowOutput.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // rbShowDifference
            // 
            this.rbShowDifference.AutoSize = true;
            this.rbShowDifference.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.rbShowDifference.Location = new System.Drawing.Point(2, 20);
            this.rbShowDifference.Margin = new System.Windows.Forms.Padding(2);
            this.rbShowDifference.Name = "rbShowDifference";
            this.rbShowDifference.Size = new System.Drawing.Size(102, 17);
            this.rbShowDifference.TabIndex = 8;
            this.rbShowDifference.TabStop = true;
            this.rbShowDifference.Text = "Show difference";
            this.rbShowDifference.UseVisualStyleBackColor = true;
            this.rbShowDifference.CheckedChanged += new System.EventHandler(this.rbShowDifference_CheckedChanged);
            this.rbShowDifference.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.rbShowDifference.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // PreviewOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PreviewOptions";
            this.Size = new System.Drawing.Size(150, 49);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RadioButton rbShowOutput;
        private System.Windows.Forms.RadioButton rbShowDifference;
    }
}
