﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace steganography
{
    public enum MedType
    {
        Video,
        Audio,
        AudioOut,
        Image,
        ImageOut,
        Key,
        All,
        None
    }

    /// <summary>
    /// This class is the linking class. It connects the GUI with the rest of the program
    /// </summary>
    class Core
    {
        public CoverImage Cimage = new CoverImage();
        public CoverAudio Caudio = new CoverAudio();
        public CoverVideo Cvideo = new CoverVideo();

        public CoverImage RecoverCimage = new CoverImage();
        public CoverAudio RecoverCaudio = new CoverAudio();
        public CoverVideo RecoverCvideo = new CoverVideo();

        private string RsaKey;

        /// <summary>
        /// General handler which calls the right previewMethod
        /// </summary>
        /// <param name="mediaType">The media type (the current tab) (MedType)</param>
        /// <param name="dontSave">Bool indicating whether a preview needs to be created or an output image</param>
        /// <returns>Path to the preview/output image</returns>
        public string GeneratePreview(MedType mediaType, bool dontSave = false)
        {
            switch (mediaType)
            {
                case MedType.Video:
                    return this.createPreviewVideo(dontSave);
                case MedType.Audio:
                    return this.createPreviewAudio(dontSave);
                case MedType.Image:
                    return this.createPreviewImage(dontSave);
            }
            return "";
        }

        /// <summary>
        /// General handler which calls the right recoverMethod
        /// </summary>
        /// <param name="mediaType">The media type (the current tab) (MedType)</param>
        /// <returns>True if successful, otherwise false (bool)</returns>
        public bool Recover(MedType mediatype)
        {
            bool result = false;
            switch (mediatype)
            {
                case MedType.Video:
                    result = this.recoverVideo();
                    break;
                case MedType.Audio:
                    result = this.recoverAudio();
                    break;
                case MedType.Image:
                    result = this.recoverImage();
                    break;
            }
            return result;
        }

        /// <summary>
        /// Checks to see if any GUI settings are invalid. Creates Error-objects to inform the user if the settings are invalid.
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        /// <param name="dontSave">Bool indicating whether a preview needs to be created or an output image</param>
        /// <returns>True if all settings are valid, otherwise false</returns>
        private bool checkValidVars(CoverFile cfile, bool dontSave)
        {
            if (cfile.FileBuffer == null || cfile.FileBuffer.CanSeek == false)
            {
                CoverError cError = new CoverError();
                cError.OnThrowError(new Exception());
                return false;
            }

            if (!dontSave)
            {
                if ((cfile.HiddenFile.FileBuffer == null || cfile.HiddenFile.FileBuffer.CanSeek == false) && (cfile.UseText == false && cfile.HiddenText == null))
                {
                    InputError iError = new InputError();
                    iError.OnThrowError(new Exception());
                    return false;
                }

                if (cfile.OutputPath == "" || cfile.OutputPath == null || cfile.OutputPath == cfile.HiddenFile.FilePath)
                {
                    OutputError oError = new OutputError();
                    oError.OnThrowError(new Exception());
                    return false;
                }

                if (cfile.SavePath == "" || cfile.SavePath == null)
                {
                    SaveKeyError keyError = new SaveKeyError();
                    keyError.OnThrowError(new Exception());
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Method for the show difference option in the GUI
        /// </summary>
        /// <returns>Path to the difference image</returns>
        public string ShowImageDifference()
        {
            return this.createPreviewImage(true, true); // makes use of createPreview's magic
        }

        /// <summary>
        /// Contains all steps that are uniform in the preview/output creation of images, audio and video
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        /// <param name="stega">LSB-steganography object, different for each medium (image/audio/video)</param>
        /// <returns>Path to the preview/output image</returns>
        private bool createPreviewGeneral(CoverFile cfile, Steganography stega)
        {
            this.resetBuffer(cfile);

            if (cfile.UseText) // use the text from the textbox in the GUI and put it in a temp TXT file
            {
                TempFile tmp = new TempFile(".txt");
                Stream file = new FileStream(tmp.file, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(file);
                writer.Write(cfile.HiddenText);
                writer.Close();
                file.Close();

                cfile.HiddenFile.FileExtension = "txt";
                cfile.HiddenFile.Reset(tmp.file);
            }
            else
            {
                if (cfile.HiddenFile.FileBuffer == null)
                    return false;
            }

            if(!cfile.CheckSizeCapacity(cfile.HiddenFile))
                return false;

            TempStream ts = new TempStream();
            Stream hideDStream = ts.stream;

            cfile.HiddenFile.FileBuffer.Position = 0;

            cfile.HiddenFile.FileBuffer.CopyTo(hideDStream);

            cfile.HiddenFile.FileBuffer.Position = 0;
            hideDStream.Position = 0;

            this.RsaKey = "";
            if (cfile.UseEncryption)
            {
                hideDStream = encrypt(cfile,hideDStream);
                this.RsaKey = encodeTo64(cfile.Crypt.getRSAPrivateKey()); //  as the key is given in XML format by default and saved in a XML-file, to avoid XML-markup conflicts, the key is encoded
            }
            else if (cfile.ScatterData) // random mode?
            {
                cfile.Crypt.GenerateRSAKey();
                this.RsaKey = encodeTo64(cfile.Crypt.getRSAPrivateKey());
            }

            if (cfile.ScatterData)
                stega.SetKey(this.RsaKey, cfile.Password);

            cfile.OriginalFileSize = hideDStream.Length;

            stega.HideData(hideDStream, cfile.LSBCount, cfile.ScatterData);

            hideDStream.Position = 0;

            return true;
        }

        /// <summary>
        /// Contains the steps necesarry for the creation of a preview image (specific)
        /// </summary>
        /// <param name="dontSave">Bool indicating whether a preview needs to be created or an output image</param>
        /// <param name="difference">Bool which indicates wheteher to create a preview or a difference image</param>
        /// <returns>Path to the preview/output image</returns>
        private string createPreviewImage(bool dontSave, bool difference = false)
        {
            if (this.Cimage.FileBuffer == null)
                return "";

            this.Cimage.FileBuffer.Position = 0;

            if (!this.checkValidVars(this.Cimage, dontSave) && !dontSave)
                return "";

            ImageParams imgParams = this.Cimage.SetChannelParams();
            Bitmap image = new Bitmap(this.Cimage.FileBuffer);
            this.Cimage.FileBuffer.Position = 0;

            LSBImageSteganography stega;

            if (difference)
            {
                Bitmap bmp = createBlackBitmap(image.Width, image.Height);
                stega = new LSBImageSteganography(bmp, imgParams);
            }
            else 
                stega = new LSBImageSteganography(image, imgParams);

            if (!this.createPreviewGeneral(this.Cimage, stega) && this.Cimage.HiddenFile.FileBuffer != null)
                return "";

            stega.Save(this.Cimage.temp.file, this.getImageFormat());

            if (!dontSave) // if it's not just a preview
            {
                string extension = this.getExtension(this.Cimage);
                // create the key (XML) file
                OutputFile.Save(this.Cimage.SavePath,
                                    extension,
                                    this.Cimage.OriginalFileSize,
                                    this.Cimage.LSBCount,
                                    this.Cimage.ScatterData,
                                    this.Cimage.UseEncryption,
                                    this.RsaKey,
                                    this.Cimage.Crypt.getEncryptedKeyLength()
                               );

                stega.Save(this.Cimage.OutputPath, this.getImageFormat());
            }

            return this.Cimage.temp.file;
        }

        /// <summary>
        /// Contains the steps necesarry for the creation of a preview audio (specific)
        /// </summary>
        /// <param name="dontSave">Bool indicating whether a preview needs to be created or an output audio</param>
        /// <returns>Path to the preview/output audio</returns>
        private string createPreviewAudio(bool dontSave)
        {
            if (this.Caudio.FileBuffer == null || (this.Caudio.HiddenFile.FileBuffer == null && (this.Caudio.HiddenText == null || this.Caudio.HiddenText == "")))
                return "";

            this.Caudio.FileBuffer.Position = 0;

            if (!this.checkValidVars(this.Caudio, dontSave) && !dontSave)
                return "";

            LSBAudioSteganography stega = new LSBAudioSteganography(this.Caudio.FilePath);
            stega.SetTargetBitDepth(this.Caudio.TargetBitDepth);

            if (!this.createPreviewGeneral(this.Caudio, stega) && this.Caudio.HiddenFile.FileBuffer != null)
                return "";

            this.Caudio.temp = new TempFile(".wav");
            stega.Save(this.Caudio.temp.file, true);

            if (!dontSave) // if it's not just a preview
            {
                string extension = this.getExtension(this.Caudio);
                // create the key (XML) file
                OutputFile.Save(this.Caudio.SavePath,
                                    extension,
                                    this.Caudio.OriginalFileSize,
                                    this.Caudio.LSBCount,
                                    this.Caudio.ScatterData,
                                    this.Caudio.UseEncryption,
                                    this.RsaKey,
                                    this.Caudio.Crypt.getEncryptedKeyLength()
                               );
                stega.Save(this.Caudio.OutputPath);
            }
            return this.Caudio.temp.file;
        }

        /// <summary>
        /// Contains the steps necesarry for the creation of a preview video (specific)
        /// </summary>
        /// <param name="dontSave">Bool indicating whether a preview needs to be created or an output video</param>
        /// <returns>Path to the preview/output video</returns>
        private string createPreviewVideo(bool dontSave)
        {
            if (this.Cvideo.FileBuffer == null || (this.Cvideo.HiddenFile.FileBuffer == null && (this.Cvideo.HiddenText == null || this.Cvideo.HiddenText == "")))
                return "";
            this.Cvideo.FileBuffer.Position = 0;

            if (!this.checkValidVars(this.Cvideo, dontSave) && !dontSave)
                return "";

            LSBVideoSteganography stega = new LSBVideoSteganography(this.Cvideo.FilePath);

            if (!stega.setSourceStream())
                return "";

            if (!this.createPreviewGeneral(this.Cvideo, stega) && this.Cvideo.HiddenFile.FileBuffer != null)
                return "";

            this.Cvideo.temp = new TempFile(".avi");

            stega.Save(this.Cvideo.temp.file);

            if (!dontSave) // if it's not just a preview
            {
                string extension = this.getExtension(this.Cvideo);
                // create the key (XML) file
                OutputFile.Save(this.Cvideo.SavePath,
                                    extension,
                                    this.Cvideo.OriginalFileSize,
                                    this.Cvideo.LSBCount, this.Cvideo.ScatterData,
                                    this.Cvideo.UseEncryption,
                                    this.RsaKey,
                                    this.Cvideo.Crypt.getEncryptedKeyLength()
                               );
                stega.Save(this.Cvideo.OutputPath);
            }

            return this.Cvideo.temp.file;
        }

        /// <summary>
        /// Contains all steps that are uniform in the preview/output creation of images, audio and video
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        /// <param name="stega">LSB-steganography object, different for each medium (image/audio/video)</param>
        /// <returns>True if successful, otherwise false (bool)</returns>
        private bool recoverGeneral(CoverFile RecoverCfile, Steganography stega)
        {
            // load the key settings and set the necessary variables for the recover
            RecoverCfile.HiddenFile.Load(RecoverCfile.LoadPath);
            RecoverCfile.Load(RecoverCfile.LoadPath);

            if (RecoverCfile.ScatterData)
                stega.SetKey(RecoverCfile.HiddenFile.RSAKey, RecoverCfile.Password);

            Stream targetStream;
            RecoverCfile.OutFile.SetStream(RecoverCfile.OutputPath +"."+ RecoverCfile.HiddenFile.FileExtension.ToString().ToLower());
            RecoverCfile.temp = new TempFile();
            RecoverCfile.FileBuffer = InputFile.CreateBufferedStream(RecoverCfile.temp.file, true);
            targetStream = RecoverCfile.FileBuffer;

            if (RecoverCfile.HiddenFile.UseEncryption)
            {
                // create a temp outstream, so that it can be decrypted
                // the decrypted data will then be written to the output dir or in case it's just a preview, the filepath to it will be returned
                TempStream tmp = new TempStream();
                stega.RecoverData(tmp.stream, RecoverCfile.LSBCount, RecoverCfile.ScatterData, RecoverCfile.HiddenFile.OriginalFileSize);
                tmp.stream.Position = 0;

                try
                {
                    Stream decrypted = decrypt(RecoverCfile, tmp.stream, InputFile.DecodeFrom64(RecoverCfile.HiddenFile.RSAKey), RecoverCfile.HiddenFile.AESKeyLength);
                    decrypted.Position = 0;
                    decrypted.CopyTo(targetStream);
                    decrypted.Close();
                }
                catch (Exception e)
                {
                    RSADecryptError decryptError = new RSADecryptError();
                    decryptError.OnThrowError(e);

                    return false;
                }
            }
            else
                stega.RecoverData(targetStream, RecoverCfile.LSBCount, RecoverCfile.ScatterData, RecoverCfile.HiddenFile.OriginalFileSize);

            targetStream.Position = 0;
            RecoverCfile.OutFile.OutStream.Position = 0;
            targetStream.CopyTo(RecoverCfile.OutFile.OutStream);
            RecoverCfile.OutFile.OutStream.Close();

            return true;
        }

        /// <summary>
        /// Contains the steps necesarry for the recover of the image (specific)
        /// </summary>
        /// <returns>True if successful, otherwise false (bool)</returns>
        private bool recoverImage()
        {
            this.resetBuffer(this.RecoverCimage);

            ImageParams imgParams = this.RecoverCimage.SetChannelParams();

            this.RecoverCimage.FileBuffer.Position = 0;
            Bitmap image = new Bitmap(this.RecoverCimage.FileBuffer);
            this.RecoverCimage.FileBuffer.Position = 0;

            LSBImageSteganography stega = new LSBImageSteganography(image, imgParams);

            return this.recoverGeneral(this.RecoverCimage,stega);
        }

        /// <summary>
        /// Contains the steps necesarry for the recover of the audio (specific)
        /// </summary>
        /// <returns>True if successful, otherwise false (bool)</returns>
        private bool recoverAudio()
        {
            this.resetBuffer(this.RecoverCaudio);

            LSBAudioSteganography stega = new LSBAudioSteganography(RecoverCaudio.FilePath);

            return this.recoverGeneral(this.RecoverCaudio, stega);
        }

        /// <summary>
        /// Contains the steps necesarry for the recover of the video (specific)
        /// </summary>
        /// <returns>True if successful, otherwise false (bool)</returns>
        private bool recoverVideo()
        {
            this.resetBuffer(this.RecoverCvideo);

            LSBVideoSteganography stega = new LSBVideoSteganography(RecoverCvideo.FilePath);
            if (!stega.setSourceStream())
                return false;

            return this.recoverGeneral(this.RecoverCvideo, stega);
        }

        /// <summary>
        /// Creates a black bitmap, for the diffence option, so that the data (non-black pixels) can be seen in the difference
        /// </summary>
        /// <param name="int">Height of the bitmap (int)</param>
        /// <param name="width">Width of the bitmap (int)</param>
        /// <returns>True if successful, otherwise false (bool)</returns>
        private static Bitmap createBlackBitmap(int width, int height)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle imageSize = new Rectangle(0, 0, bmp.Width, bmp.Height);
                graph.FillRectangle(Brushes.Black, imageSize);
            }
            return bmp;
        }

        /// <summary>
        /// See CoverImage:GetImageFormat
        /// </summary>
        private ImageFormat getImageFormat()
        {
            return CoverImage.GetImageFormat(this.Cimage.FileExtension);
        }

        /// <summary>
        /// Puts the content of a string into a FileStream
        /// </summary>
        /// <param name="text">The string contained the data to put in a FileStream</param>
        /// <returns>FileStream containing the content (Stream)</returns>
        private Stream stringToFileStream(string text)
        {
            TempStream tmp = new TempStream();
            byte[] textArray = Encoding.UTF8.GetBytes(text);
            tmp.stream.Write(textArray, 0, textArray.Length);
            return tmp.stream;
        }

        /// <summary>
        /// Call the encrypt method from the Crypto class
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        /// <param name="data">Contains the password (Stream)</param>
        /// <returns>The encrypted data (Stream)</returns>
        private Stream encrypt(CoverFile cfile, Stream data)
        {
            return cfile.Crypt.StartEncrypt(data, cfile.Password);
        }

        /// <summary>
        /// Call the decrypt method from the Crypto class
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        /// <param name="data">Contains the password (Stream)</param>
        /// <param name="encryptedKeyLength">The length of the AES key</param>
        /// <returns>The decrypted data (Stream)</returns>
        private Stream decrypt(CoverFile cfile, Stream data, string rsaKey, int encryptedKeyLength)
        {
            cfile.Crypt.RSA = cfile.Crypt.GenerateRSAKey();
            cfile.Crypt.RSA.FromXmlString(rsaKey); // set the key
            return cfile.Crypt.StartDecrypt(data, cfile.Crypt.RSA, encryptedKeyLength, cfile.Password);
        }

        /// <summary>
        /// Gets the extension of a CoverFile
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        /// <returns>The file extension without leading . in lowercase (String)</returns>
        private string getExtension(CoverFile cfile)
        {
            string extension;
            if (cfile.UseText)
            {
                extension = "txt";
            }
            else
                extension = cfile.HiddenFile.FileExtension;
            return extension;
        }

        /// <summary>
        /// Is needed when hiding files directly after each other
        /// </summary>
        /// <param name="cfile">The CoverFile, this contains a CoverImage-object if MedType is Image, CoverAudio when Medtype is Audio etc.</param>
        private void resetBuffer(CoverFile cfile)
        {
            cfile.temp = new TempFile();
            cfile.Reset(cfile.FilePath);
        }

        /// <summary>
        /// Encodes a base64 string (necessary when dealing RSA keys)
        /// </summary>
        /// <param name="data">String containing the data</param>
        /// <returns>Encoded data (string)</returns>
        /// <remarks>As the key is given in XML format by default and saved in a XML-file, the key is encoded in order to avoid XML-markup conflicts</remarks>
        private static string encodeTo64(string data)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(data);
            return System.Convert.ToBase64String(toEncodeAsBytes);
        }
    }
}