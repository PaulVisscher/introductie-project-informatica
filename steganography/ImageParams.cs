﻿/**
 * @file        ImageParams.cs
 * @brief       
 *
 * @details     
 *
 * @author      Mick van Duijn
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace steganography
{
    /// <summary>
    /// Parameters of an image such as:
    /// 1) the number of channels (Channels)
    /// 2) the number of bytes per channel (BytesChannel)
    /// 3) does the image have an alpha channel or not (HasAlpha)
    /// </summary>
    struct ImageParams
    {
        public int Channels, BytesChannel;
        public bool HasAlpha;

        /// <summary>
        /// Initialize the ImageParams struct with the given values
        /// </summary>
        /// <param name="channels">The number of channels the image has</param>
        /// <param name="bytesChannel">The number of bytes each channel uses</param>
        /// <param name="hasAlpha">Does the image have an alpha channel or not</param>
        public ImageParams(int channels, int bytesChannel, bool hasAlpha)
        {
            this.Channels = channels;
            this.BytesChannel = bytesChannel;
            this.HasAlpha = hasAlpha;
        }
    }
}
