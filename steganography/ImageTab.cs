﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;


namespace steganography
{
    partial class ImageTab : TabControl
    {
        public PreviewImage prevImg;
        public OptionsImage optImg;
        public PreviewOptions prevOpt;
        public bool showDifference = false;

        public ImageTab()
        {
            InitializeComponent();

            this.prevImg = new PreviewImage(this);
            this.optImg = new OptionsImage(this);
            this.prevOpt = new PreviewOptions(this);

            this.previewTable.Controls.Add(this.prevImg, 0, 1);
            this.StegOptionsTable.Controls.Add(this.optImg, 0, 0);
            this.PreviewOptionsTable.Controls.Add(this.prevOpt, 0 ,0);

            this.outputType = MedType.ImageOut;
            this.inputType = MedType.Image;

        }

        public override void setParent(GUIform par)
        {
            base.setParent(par);
            this.cf = this.prnt.Core.Cimage;
        }

        public override void setCarrierFile(string path = "")
        {
            if (path == "")
                path = this.carrierFilePath.Text;
            else
                this.carrierFilePath.Text = path;

            if (path != "" && File.Exists(path))
            {
                if (DialogFilter.FileMatchesPatern(path, this.inputType))
                {
                    this.cf.SetFile(path);

                    try
                    {
                        Bitmap bmp = new Bitmap(path);
                        if (!( bmp.RawFormat.Equals(CoverImage.GetImageFormat(this.prnt.Core.Cimage.FileExtension)) || 
                            bmp.RawFormat.Equals(ImageFormat.Jpeg) )) 
                            throw new Exception();
                        this.prevImg.inputImagePanel.SetInputImage(bmp);

                        this.setPreview();
                    }
                    catch (Exception e)
                    {
                        ImageFormatError formatError = new ImageFormatError();
                        formatError.OnThrowError(e);
                    }
                }
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void SaveOutputPath_TextChanged(object sender, EventArgs e) 
        {
            string path = this.SaveOutputPath.Text;

            if (path != "" && Directory.Exists(Path.GetDirectoryName(path)))
            {
                this.prnt.Core.Cimage.OutputPath = path;
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void SaveKeyPath_TextChanged(object sender, EventArgs e) 
        {
            string path = this.SaveKeyPath.Text;

            if (path != "" && Directory.Exists(Path.GetDirectoryName(path)))
            {
                this.prnt.Core.Cimage.SavePath = path;
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void HideFilePath_TextChanged(object sender, EventArgs e) 
        {
            string path = this.HideFilePath.Text;

            if (path != "" && File.Exists(path))
            {
                this.prnt.Core.Cimage.HiddenFile.SetFile(path);
                this.setPreview();
            }
            
            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void inputTextbox_TextChanged(object sender, EventArgs e) 
        {
            this.prnt.Core.Cimage.HiddenText = this.inputTextbox.Text;

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void rbHideFile_CheckedChanged(object sender, EventArgs e)
        {
            base.rbHideFile_CheckedChanged(sender, e);
            this.prnt.Core.Cimage.UseText = this.rbHideText.Checked;
        }

        public override void rbHideText_CheckedChanged(object sender, EventArgs e)
        {
            base.rbHideText_CheckedChanged(sender, e);
            this.prnt.Core.Cimage.UseText = this.rbHideText.Checked;
        }

        public override void setPreview(bool saveOutput = false)
        {
            if (this.cf.FileBuffer != null && this.cf.FileBuffer.CanSeek)
                this.infoBox.Text = "Processing...";
            
            if (this.processor != null && this.processor.IsAlive)
                this.processor.Abort();

            this.processor = new Thread(new ParameterizedThreadStart(threadPreview));
            this.processor.Start(new ImgTreadObj(this.prnt.Core,this.showDifference, this.inputType, saveOutput));
        }

        private bool enableProcess()
        {
            if (this.cf.FilePath != "" && 
                this.prnt.Core.Cimage.SavePath != null && 
                this.prnt.Core.Cimage.OutputPath != null && 
                this.prnt.Core.Cimage.OutputPath != "recoveredFile" && 
                ((this.cf.HiddenFile.FileBuffer != null && this.cf.HiddenFile.FileBuffer.CanSeek) || this.prnt.Core.Cimage.HiddenText != null))
                return true;
            return false;
        }

        private void threadPreview(object p)
        {
            string path = "";
            ImgTreadObj par = (ImgTreadObj)p;

            if (par.showDiff && !par.saveOutput)
                path = par.core.ShowImageDifference();
            else
                path = par.core.GeneratePreview(par.type, !par.saveOutput);

            if (path != "")
            {
                if (!(par.showDiff && par.saveOutput))
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.prevImg.outputImagePanel.Image = new Bitmap(path);
                    });

                if (par.saveOutput)
                {
                    System.Media.SystemSounds.Beep.Play();
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.infoBox.Text = "Finished!";
                        
                    });
                }
                else
                {
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.infoBox.Text = "Done creating preview.";
                    });
                }
            }

        }
    }

    class ImgTreadObj
    {
        public Core core;
        public bool showDiff, saveOutput;
        public MedType type;

        public ImgTreadObj(Core cr, bool showdf, MedType type, bool saveOutput)
        {
            this.core = cr;
            this.showDiff = showdf;
            this.type = type;
            this.saveOutput = saveOutput;
        }
    }
}
