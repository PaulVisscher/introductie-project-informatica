﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace steganography
{
    partial class RecoverTab : UserControl
    {
        public CoverFile cf;
        public GUIform prnt;
        public MedType type;
        public Thread processor;

        public RecoverTab()
        {
            InitializeComponent();
        }

        public void setParent(GUIform gf)
        {
            this.prnt = gf;
        }

        public void KeyPath_TextChanged(object sender, EventArgs e)
        {
            string path = this.KeyPath.Text;

            if (File.Exists(path))
            {
                switch (this.type)
                {
                    case MedType.Image:
                        this.prnt.Core.RecoverCimage.LoadPath = path;
                        break;

                    case MedType.Audio:
                        this.prnt.Core.RecoverCaudio.LoadPath = path;
                        break;

                    case MedType.Video:
                        this.prnt.Core.RecoverCvideo.LoadPath = path;
                        break;
                }
            }
        }

        public void carrierFilePath_TextChanged(object sender, EventArgs e)
        {
            string path = this.carrierFilePath.Text;

            if (File.Exists(path))
            {
                if(DialogFilter.FileMatchesPatern(path, MedType.All))
                {
                    this.type = this.getMedType(path);

                    switch (this.type)
                    {
                        case MedType.Image:
                            this.prnt.Core.RecoverCimage.SetFile(path);
                            break;

                        case MedType.Audio:
                            this.prnt.Core.RecoverCaudio.SetFile(path);
                            break;

                        case MedType.Video:
                            this.prnt.Core.RecoverCvideo.SetFile(path);
                            break;
                    }

                    this.KeyPath.Enabled = true;
                    this.passwordRecBox.Enabled = true;
                    this.outputPath.Enabled = true;
                    this.SaveKeyButton.Enabled = true;
                    this.outputRecButton.Enabled = true;
                }
            }
        }

        public void passwordRecBox_TextChanged(object sender, EventArgs e)
        {
            string password = this.passwordRecBox.Text;

            switch (this.type)
            {
                case MedType.Image:
                    this.prnt.Core.RecoverCimage.Password = password;
                    break;

                case MedType.Audio:
                    this.prnt.Core.RecoverCaudio.Password = password;
                    break;

                case MedType.Video:
                    this.prnt.Core.RecoverCvideo.Password = password;
                    break;
            }
        }

        public void outputPath_TextChanged(object sender, EventArgs e)
        {
            string path = this.outputPath.Text;

            if (Directory.Exists(path))
            {
                switch (this.type)
                {
                    case MedType.Image:
                        this.prnt.Core.RecoverCimage.OutputPath = path + "\\recoveredFile";
                        break;

                    case MedType.Audio:
                        this.prnt.Core.RecoverCaudio.OutputPath = path + "\\recoveredFile";
                        break;

                    case MedType.Video:
                        this.prnt.Core.RecoverCvideo.OutputPath = path + "\\recoveredFile";
                        break;
                }

                this.buttonProcessRec.Enabled = true;
            }
        }

        private void aboutLabel_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(Config.About,
               "About this program",
               MessageBoxButtons.OK,
               MessageBoxIcon.Information
               );
        }

        private string GetFilePath(MedType type)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = DialogFilter.getFilter(type);
            ofd.ShowDialog();

            return ofd.FileName;
        }

        private MedType getMedType(string filePath)
        {
            // determine the MedType based on the file extension
            MedType[] medtypes = { MedType.Image, MedType.Audio, MedType.Video };
            foreach (MedType mt in medtypes)
            {
                if (DialogFilter.FileMatchesPatern(filePath, mt))
                {
                    return mt;
                }
            }
            return MedType.None;
        }

        private string GetFolderPath()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();

            return fbd.SelectedPath;
        }

        private void control_MouseEnter(object sender, EventArgs e)
        {
            this.infoBox.Text = Config.Info(((Control)sender).Name.ToString());
        }

        private void control_MouseLeave(object sender, EventArgs e)
        {
            this.infoBox.Text = "";
        }

        private void KeyButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.KeyPath.Text = this.GetFilePath(MedType.Key);
        }

        private void outputRecButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.outputPath.Text = this.GetFolderPath();
        }

        private void carrierRecButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.carrierFilePath.Text = this.GetFilePath(MedType.All);
        }

        private void buttonProcessRec_MouseClick(object sender, MouseEventArgs e)
        {
            this.infoBox.Text = "Processing...";

            if (this.processor != null && this.processor.IsAlive)
                this.processor.Abort();

            this.processor = new Thread(new ParameterizedThreadStart(threadPreview));
            this.processor.Start(new ParamThreadObj(this.prnt.Core, this.type, false));
        }

        private void threadPreview(object p)
        {
            ParamThreadObj par = (ParamThreadObj)p;

            System.Media.SystemSounds.Beep.Play();
            if (par.core.Recover(par.type))
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    this.infoBox.Text = "Finished!";

                });
                
            }
        }

    }
}
