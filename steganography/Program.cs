﻿/**
 * @file        Program.cs
 * @brief       Starts the whole program by delivering the Main loop.
 *
 * @author      Paul Visscher
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;


namespace steganography
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new GUIform());
        }
    }
}
