﻿/**
 * @file        BitMethods.cs
 * @brief       
 *
 * @details     
 *
 * @author      Mick van Duijn
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace steganography
{
    enum ShuffleMode
    {
        Shuffle,
        DeShuffle
    }

    /// <summary>
    /// This class handels operations at bit level
    /// </summary>
    static class BitMethods
    {
        /// <summary>
        /// Shuffles all the bits in a LargeByteArray using a RijndaelPRNG for random positions and a key
        /// so that the algorithm can be reversed. For the shuffle it uses the Fisher-Yates algorithm
        /// </summary>
        /// <param name="byteArray">The array to shuffle</param>
        /// <param name="key">The key to generate random positions with a RijndaelPRNG</param>
        /// <param name="mode">The order in which to shuffle the elements</param>
        public static void FisherYatesShuffle(ref LargeByteArray byteArray, byte[] key, string password, ShuffleMode mode)
        {
            RijndaelPRNG prng = new RijndaelPRNG(key, password);

            long start, difference;
            if (mode == ShuffleMode.Shuffle)
            {
                start = 0;
                difference = 1;
            }
            else
            {
                start = byteArray.Length - 1;
                difference = -1;
            }

            long byteArrayLength = byteArray.Length;
            for (long i = start; (i >= 0) && (i < byteArrayLength); i += difference)
                byteArray.SwapBit(i, prng.RandRange(i, i + 1));
        }


        /// <summary>
        /// Shuffles all the bits in a stream using a RijndaelPRNG for random positions and a key
        /// so that the algorithm can be reversed. For the shuffle it uses the Fisher-Yates algorithm
        /// </summary>
        /// <param name="byteArray">The stream to shuffle</param>
        /// <param name="key">The key to generate random positions with a RijndaelPRNG</param>
        /// <param name="mode">The order in which to shuffle the elements</param>
        public static void FisherYatesShuffle(StreamBitHandler streamHandler, byte[] key, string password, ShuffleMode mode)
        {
            RijndaelPRNG prng = new RijndaelPRNG(key, password);

            long start, difference;
            if (mode == ShuffleMode.Shuffle)
            {
                start = 0;
                difference = 1;
            }
            else
            {
                start = streamHandler.Length - 1;
                difference = -1;
            }

            long streamLength = streamHandler.Length;

            byte byte1, byte2;
            long byteIndex1 = 0, byteIndex2 = 0, oldByteIndex1, oldByteIndex2;

            oldByteIndex1 = start >> 3;
            oldByteIndex2 = prng.RandRange(start, start + 1) >> 3;

            byte1 = streamHandler.GetByte(oldByteIndex1);
            byte2 = streamHandler.GetByte(oldByteIndex2);

            for (long pos1 = start; pos1 >= 0 && pos1 < streamLength; pos1 += difference)
            {
                long pos2 = prng.RandRange(pos1, pos1 + 1);

                byteIndex1 = pos1 >> 3;
                byteIndex2 = pos2 >> 3;

                if (byteIndex1 != oldByteIndex1)
                {
                    streamHandler.SetByte(oldByteIndex1, byte1);
                    oldByteIndex1 = byteIndex1;
                    if (byteIndex1 != oldByteIndex2)
                        byte1 = streamHandler.GetByte(byteIndex1);
                    else
                        byte1 = byte2;
                }

                if (byteIndex2 != oldByteIndex2)
                {
                    streamHandler.SetByte(oldByteIndex2, byte2);
                    oldByteIndex2 = byteIndex2;
                    if (byteIndex2 != byteIndex1)
                        byte2 = streamHandler.GetByte(byteIndex2);
                    else
                        byte2 = byte1;
                }

                int bitIndex1, bitIndex2;

                bitIndex1 = (int)(pos1 & 7);
                bitIndex2 = (int)(pos2 & 7);

                int bit1, bit2;

                bit1 = (byte1 >> bitIndex1) & 1;
                bit2 = (byte2 >> bitIndex2) & 1;

                byte1 = (byte)((byte1 & ~(1 << bitIndex1)) | (bit2 << bitIndex1));

                if (byteIndex1 == byteIndex2)
                    byte2 = byte1;

                byte2 = (byte)((byte2 & ~(1 << bitIndex2)) | (bit1 << bitIndex2));

                if (byteIndex1 == byteIndex2)
                    byte1 = byte2;
            }

            streamHandler.SetByte(byteIndex1, byte1);
            streamHandler.SetByte(byteIndex2, byte2);
        }


        /// <summary>
        /// XOR the bytes outside the range with the ones outside and then resize the array to the new length
        /// </summary>
        /// <param name="bytes">Array to hash</param>
        /// <param name="newLength">The desired length of the new array</param>
        public static void Hash(ref byte[] bytes, int newLength)
        {
            if (bytes.Length < newLength)
                throw new Exception("Invalid byte[] length");

            int interval = bytes.Length - newLength;

            //XOR the bytes outside the range with the corresponding bytes inside the range
            for (int i = 0; i < interval; i++)
            {
                bytes[i % newLength] ^= bytes[newLength + i];
            }

            Array.Resize(ref bytes, newLength);
        }

        /// <summary>
        /// Read every byte from the position in the stream till the end and put them in the LargeByteArray
        /// </summary>
        /// <param name="dataStream">Stream to read from</param>
        public static void StreamToArray(Stream dataStream, ref LargeByteArray byteArray)
        {
            for (long pos = dataStream.Position; pos < dataStream.Length; pos++)
            {
                byteArray.SetByte(pos, (byte)dataStream.ReadByte());
            }
        }

        /// <summary>
        /// Write every byte in the LargeByteArray and write them to the targetstream
        /// </summary>
        /// <param name="dataStream">Stream to write to</param>
        public static void ArrayToStream(ref LargeByteArray byteArray, Stream dataStream)
        {
            foreach (byte b in byteArray.Bytes)
                dataStream.WriteByte(b);
        }
    }
}