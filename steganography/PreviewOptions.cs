﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace steganography
{
    partial class PreviewOptions : UserControl
    {
        ImageTab tabcontr;
        public PreviewOptions(ImageTab tc)
        {
            InitializeComponent();

            this.tabcontr = tc;
        }

        private void control_MouseEnter(object sender, EventArgs e)
        {
            this.tabcontr.control_MouseEnter(sender, e);
        }

        private void control_MouseLeave(object sender, EventArgs e)
        {
            this.tabcontr.control_MouseLeave(sender, e);
        }

        private void rbShowOutput_CheckedChanged(object sender, EventArgs e)
        {
            this.tabcontr.showDifference = this.rbShowDifference.Checked;
            this.tabcontr.setPreview();
        }

        private void rbShowDifference_CheckedChanged(object sender, EventArgs e)
        {
            this.tabcontr.showDifference = this.rbShowDifference.Checked;
            this.tabcontr.setPreview();
        }
    }
}
