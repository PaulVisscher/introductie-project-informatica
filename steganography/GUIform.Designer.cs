﻿namespace steganography
{
    partial class GUIform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MediaTabs = new System.Windows.Forms.TabControl();
            this.tabImage = new System.Windows.Forms.TabPage();
            this.imageTab = new steganography.ImageTab();
            this.tabSound = new System.Windows.Forms.TabPage();
            this.soundTab = new steganography.SoundTab();
            this.tabVideo = new System.Windows.Forms.TabPage();
            this.videoTab = new steganography.VideoTab();
            this.tabRecover = new System.Windows.Forms.TabPage();
            this.recoverTab = new steganography.RecoverTab();
            this.MediaTabs.SuspendLayout();
            this.tabImage.SuspendLayout();
            this.tabSound.SuspendLayout();
            this.tabVideo.SuspendLayout();
            this.tabRecover.SuspendLayout();
            this.SuspendLayout();
            // 
            // MediaTabs
            // 
            this.MediaTabs.Controls.Add(this.tabImage);
            this.MediaTabs.Controls.Add(this.tabSound);
            this.MediaTabs.Controls.Add(this.tabVideo);
            this.MediaTabs.Controls.Add(this.tabRecover);
            this.MediaTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MediaTabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.MediaTabs.ItemSize = new System.Drawing.Size(40, 40);
            this.MediaTabs.Location = new System.Drawing.Point(0, 0);
            this.MediaTabs.Margin = new System.Windows.Forms.Padding(0);
            this.MediaTabs.Name = "MediaTabs";
            this.MediaTabs.SelectedIndex = 0;
            this.MediaTabs.Size = new System.Drawing.Size(784, 642);
            this.MediaTabs.TabIndex = 0;
            // 
            // tabImage
            // 
            this.tabImage.BackColor = System.Drawing.SystemColors.Control;
            this.tabImage.Controls.Add(this.imageTab);
            this.tabImage.Location = new System.Drawing.Point(4, 44);
            this.tabImage.Name = "tabImage";
            this.tabImage.Size = new System.Drawing.Size(776, 594);
            this.tabImage.TabIndex = 0;
            this.tabImage.Text = "Image";
            // 
            // imageTab
            // 
            this.imageTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageTab.Location = new System.Drawing.Point(0, 0);
            this.imageTab.Margin = new System.Windows.Forms.Padding(0);
            this.imageTab.Name = "imageTab";
            this.imageTab.Size = new System.Drawing.Size(776, 594);
            this.imageTab.TabIndex = 0;
            this.imageTab.Load += new System.EventHandler(this.imageTab_Load);
            // 
            // tabSound
            // 
            this.tabSound.BackColor = System.Drawing.SystemColors.Control;
            this.tabSound.Controls.Add(this.soundTab);
            this.tabSound.Location = new System.Drawing.Point(4, 44);
            this.tabSound.Name = "tabSound";
            this.tabSound.Size = new System.Drawing.Size(776, 594);
            this.tabSound.TabIndex = 1;
            this.tabSound.Text = "Sound";
            // 
            // soundTab
            // 
            this.soundTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.soundTab.Location = new System.Drawing.Point(0, 0);
            this.soundTab.Name = "soundTab";
            this.soundTab.Size = new System.Drawing.Size(776, 594);
            this.soundTab.TabIndex = 0;
            // 
            // tabVideo
            // 
            this.tabVideo.BackColor = System.Drawing.SystemColors.Control;
            this.tabVideo.Controls.Add(this.videoTab);
            this.tabVideo.Location = new System.Drawing.Point(4, 44);
            this.tabVideo.Name = "tabVideo";
            this.tabVideo.Size = new System.Drawing.Size(776, 594);
            this.tabVideo.TabIndex = 2;
            this.tabVideo.Text = "Video";
            // 
            // videoTab
            // 
            this.videoTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.videoTab.Location = new System.Drawing.Point(0, 0);
            this.videoTab.Name = "videoTab";
            this.videoTab.Size = new System.Drawing.Size(776, 594);
            this.videoTab.TabIndex = 0;
            // 
            // tabRecover
            // 
            this.tabRecover.BackColor = System.Drawing.SystemColors.Control;
            this.tabRecover.Controls.Add(this.recoverTab);
            this.tabRecover.Location = new System.Drawing.Point(4, 44);
            this.tabRecover.Name = "tabRecover";
            this.tabRecover.Padding = new System.Windows.Forms.Padding(3);
            this.tabRecover.Size = new System.Drawing.Size(776, 594);
            this.tabRecover.TabIndex = 3;
            this.tabRecover.Text = "Recover";
            // 
            // recoverTab
            // 
            this.recoverTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recoverTab.Location = new System.Drawing.Point(3, 3);
            this.recoverTab.Name = "recoverTab";
            this.recoverTab.Size = new System.Drawing.Size(770, 588);
            this.recoverTab.TabIndex = 0;
            // 
            // GUIform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 642);
            this.Controls.Add(this.MediaTabs);
            this.MinimumSize = new System.Drawing.Size(800, 680);
            this.Name = "GUIform";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Steganosaurus by © Ikiwisi 2012 ";
            this.MediaTabs.ResumeLayout(false);
            this.tabImage.ResumeLayout(false);
            this.tabSound.ResumeLayout(false);
            this.tabVideo.ResumeLayout(false);
            this.tabRecover.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MediaTabs;
        private System.Windows.Forms.TabPage tabImage;
        private System.Windows.Forms.TabPage tabSound;
        private System.Windows.Forms.TabPage tabVideo;
        private System.Windows.Forms.TabPage tabRecover;
        private ImageTab imageTab;
        private SoundTab soundTab;
        private VideoTab videoTab;
        private RecoverTab recoverTab;
    }
}