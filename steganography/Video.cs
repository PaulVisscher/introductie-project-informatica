﻿/**
 * @file        Video.cs
 * @brief       Support for avi streams.
 *
 * @details     Avi read and write stream by using the aviFiledll, used for video steganography.
 * @Warning     REQUIRES a codec packages to work IE: http://www.windows7codecs.com/
 *
 * @author      Paul Visscher
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

//uncomment to use a compression avi stream
//@XXX REQUIRES  the correct codecs to be installed
//#define  __USECOMPRESSION__

using System;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace steganography
{
    

    #region AVI_STRUCT_LAYOUT

    /// <summary>
    /// The BITMAPINFOHEADER is a struct defined in 
    /// the windows API. The structure contains
    /// information about the dimensions and color format
    /// of a DIB.
    /// 
    /// @XXX Documentation based on the MSDN documentation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct BitmapInfoHeader
    {
        /// <summary>
        /// The number of bytes required by the structure
        /// </summary>
        public  uint    biSize;

        /// <summary>
        /// The width of the bitmap in pixels
        /// </summary>
        public  int     biWidth;

        /// <summary>
        /// The height of the bitmap in pixels
        /// </summary>
        public  int     biHeight;

        /// <summary>
        /// The number of planes for the target
        /// device. This value must be set to 1.
        /// </summary>
        public  ushort  biPlanes;

        /// <summary>
        /// The number of bits-per-pixel. This member
        /// determines the number of bits that define each
        /// pixel and the maximum numbers of color in the
        /// bitmap. This member must be 
        /// one of the following values:
        /// 
        ///     Value   Meaning
        ///     0       The number of bpp is specified or in implied by the JPEG or PNG format
        ///     1       The bitmap is monochrome.
        ///     4       The bitmap has a maximum of 16 colors.
        ///     8       The bitmap has a maximum of 256 colors.
        ///     16      The bitmap has a maximum of 2^16 colors.
        ///     24      The bitmap has a maximum of 2^24 colors.
        ///     32      The bitmap has a maximum of 2^32 colors.
        /// 
        /// </summary>
        public  ushort  biBitCount;

        /// <summary>
        /// The type of compression.
        /// </summary>
        public  uint    biCompression;

        /// <summary>
        /// The size of the image in bytes. This may be
        /// set zero fro BI_RGB bitmaps.
        /// </summary>
        public  uint    biSizeImage;

        /// <summary>
        /// The horizontal resolution in pixels per meter.
        /// </summary>
        public  int     biXPelsPerMeter;

        /// <summary>
        /// The vertical resolution in pixels per meter.
        /// </summary>
        public  int     biYPelsPerMeter;

        /// <summary>
        /// The number of color indexes in the color table
        /// that are actually used by the bitmap.
        /// If this value is zer0, the bitmap uses the maximum
        /// number of colors corresponding to the calue of the 
        /// biBitCount member.
        /// </summary>
        public  uint    biClrUsed;

        /// <summary>
        /// The number of color indexes that are required for displaying the bitmap. 
        /// If this value is zero, all colors are required.
        /// </summary>
        public  uint    biClrImportant;

        /// <summary>
        /// Easily create a new BITMAPINFOHEADER with
        /// the default values.
        /// </summary>
        /// <param name="height">The height of the bitmap</param>
        /// <param name="width">The width of the bitmap</param>
        /// <returns>The corresponding BITMAPINFOHEADER</returns>
        public static BitmapInfoHeader New(int height, int width)
        {
            BitmapInfoHeader h = new BitmapInfoHeader();
            h.biSize = (uint)Marshal.SizeOf(h);
            h.biWidth           = width;
            h.biHeight          = height;
            h.biPlanes          = 1;
            h.biBitCount        = (int)BitmapHeader.BitPerPixel;
            h.biCompression     = 0;
            h.biXPelsPerMeter   = 0;
            h.biYPelsPerMeter   = 0;
            h.biClrUsed         = 0;
            h.biClrImportant    = 0;
            return h;
        }
    }

    /// <summary>
    /// The BITMAPFILEHEADER is a struct defined in the 
    /// windows API. This structure contains information about the type, 
    /// size and layout of a file that contains a DIB.
    /// 
    /// @XXX This documentation is based on the MSDN documentation
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BitmapFileHeader
    {
        /// <summary>
        /// The file type; Must be "BM":
        /// Hexadecimal:  0x00004D42
        /// </summary>
        public  ushort  bfType;

        /// <summary>
        /// The size in bytes of the bitmap file
        /// </summary>
        public  uint    bfSize;

        /// <summary>
        /// Reserved, should be zero
        /// </summary>
        public  ushort  bfReserved1;

        /// <summary>
        /// Reserved, should be zero
        /// </summary>
        public  ushort  bfReserved2;

        /// <summary>
        /// The offset in bytes from the beginning
        /// of the BITMAPFILEHEADER structure to the
        /// bitmap bits.
        /// </summary>
        public  uint    bfOffBits;

        /// <summary>
        /// Easily create a BITMAPFILEHEADER with
        /// default values.
        /// </summary>
        /// <param name="size">The size of the bitmap</param>
        /// <param name="offsetBits">The number of offset bits</param>
        /// <returns>The corresponding BITMAPFILEHEADER</returns>
        public static BitmapFileHeader New(uint size, uint offsetBits)
        {
            BitmapFileHeader h = new BitmapFileHeader();
            h.bfType = (int)BitmapHeader.BMPsignature;
            h.bfSize = (int)BitmapHeader.HeaderSize + size + (uint)Marshal.SizeOf(h);
            h.bfReserved1 = 0;
            h.bfReserved2 = 0;
            h.bfOffBits = offsetBits;
            return h;
        }
    }

    /// <summary>
    /// This struct is defined in the 
    /// windows API. The structure defines
    /// the coordinates of the upper-left
    /// and lower-right corners of a 
    /// rectangle.
    /// 
    /// @XXX This documentation is based on the
    ///      MSDN documentation
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AviRect
    {
        /// <summary>
        /// The x-coordinate of the upper-left corner of
        /// the rectangle.
        /// </summary>
        public  int     left;

        /// <summary>
        /// The y-coordinate of the upper-left corner of
        /// the rectangle.
        /// </summary>
        public  int     top;

        /// <summary>
        /// The x-coordinate of the lower-right corner of
        /// the rectangle.
        /// </summary>
        public  int     right;

        /// <summary>
        /// the y-coordingate of the lower-right corner of
        /// the rectangle.
        /// </summary>
        public  int     bottom;
    }

    /// <summary>
    /// The AVISTREAMINFO is defined in the 
    /// windows API. The structure contains information
    /// for a single stream.
    /// 
    /// @XXX This documenation is based on the MSDN
    ///      documentation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AviStreamInfo
    {
        /// <summary>
        /// Four character code indicating the stream type.
        /// </summary>
        public uint     fccType;

        /// <summary>
        /// Four character code indicating the codec
        /// used to compress the video stream when it
        /// is saved. This member is not used for
        /// audio streams.
        /// </summary>
        public uint     fccHandler;

        /// <summary>
        /// Applicable flags for the stream.
        /// </summary>
        public uint     dwFlags;

        /// <summary>
        /// Capability flags.
        /// @XXX Unused
        /// </summary>
        public uint     dwCaps;

        /// <summary>
        /// Priority of the stream
        /// </summary>
        public ushort   wPriority;

        /// <summary>
        /// Language of the stream
        /// </summary>
        public ushort   wLanguage;

        /// <summary>
        /// Timer scale applicable for the stream.
        /// Dividing dwRate by dwScale gives the playback rate
        /// in number of samples per second.
        /// 
        /// For video streams this rate should be the framerate.
        /// For audio streams this rate should correspond to the
        /// audio block size.
        /// </summary>
        public uint     dwScale;

        /// <summary>
        /// The rate in integer format.
        /// </summary>
        public uint     dwRate;

        /// <summary>
        /// Sample number of the first fram of the AVI file.
        /// Normally this is zero.
        /// </summary>
        public uint     dwStart;

        /// <summary>
        /// Length of the stream
        /// </summary>
        public uint     dwLength;

        /// <summary>
        /// Audio skew
        /// </summary>
        public uint     dwInitialFrames;

        /// <summary>
        /// Recommended buffer size in bytyes for the stream
        /// </summary>
        public uint     dwSuggestedBufferSize;

        /// <summary>
        /// Quality indicator for the video data in the 
        /// stream. Quality is represented as a number between 
        /// 0  and 10000. Where 10000 is the highest quality.
        /// </summary>
        public uint     dwQuality;

        /// <summary>
        /// Size in bytes of a single data sample.
        /// 
        /// For video this number is typically zero.
        /// </summary>
        public uint     dwSampleSize;

        /// <summary>
        /// Dimensions of the video destination rectangle
        /// </summary>
        public AviRect  rcFrame;

        /// <summary>
        /// Number of times the stream has been edited.
        /// The stream handler maintains this count.
        /// </summary>
        public uint     dwEditCount;

        /// <summary>
        /// Number of times the stream format has changed.
        /// The stream handler maintains this count.
        /// </summary>
        public uint     dwFormatChangeCount;


        /// <summary>
        /// Null terminated string containing a description of the stream.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[]   szName;

        /// <summary>
        /// Easilly create a default AVISTREAMINFO
        /// </summary>
        /// <param name="fccType"></param>
        /// <param name="fccHandler"></param>
        /// <param name="dwRate"></param>
        /// <param name="sugBuff"></param>
        /// <param name="dwQuality"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns>The corresponding AVISTREAMINFO</returns>
        public static AviStreamInfo New(
            uint fccType, 
            uint fccHandler, 
            uint dwRate, 
            uint sugBuff, 
            uint dwQuality, 
            int width, 
            int height
            )
        {
            AviStreamInfo asi = new AviStreamInfo();
            asi.rcFrame.right = width;
            asi.rcFrame.bottom = height;
            asi.fccType = fccType;
            asi.fccHandler = fccHandler;
            asi.dwRate = dwRate;
            asi.dwSuggestedBufferSize = sugBuff;
            asi.dwQuality = dwQuality;

            return asi;
        }
    }

    /// <summary>
    /// The AVICOMPRESSOPTIONS structure contains information
    /// about a stream and how it is compressed and saved. 
    /// 
    /// @XXX This documenation is based on the MSDN
    ///      documentation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AviCompressOptions
    {
        /// <summary>
        /// Four-character code indicating the stream type. 
        /// </summary>
        public uint     fccType;

        /// <summary>
        /// Four-character code for the compressor handler
        /// that will compress this video stream when it is saved
        /// </summary>
        public uint     fccHandler;

        /// <summary>
        /// Maximum period between video key frames.
        /// </summary>
        public uint     dwKeyFrameEvery;

        /// <summary>
        /// Quality value passed to a video compressor.
        /// </summary>
        public uint     dwQuality;

        /// <summary>
        /// Video compressor data rate.
        /// </summary>
        public uint     dwBytesPerSecond;

        /// <summary>
        /// Flags used for compression.
        /// </summary>
        public uint     dwFlags;
        
        /// <summary>
        /// Pointer to a structure defining the data format.
        /// </summary>
        public IntPtr   lpFormat;

        /// <summary>
        /// Size, in bytes, of the data referenced by lpFormat.
        /// </summary>
        public uint     cbFormat;

        /// <summary>
        /// Video-compressor-specific data; used internally.
        /// </summary>
        public IntPtr   lpParms;

        /// <summary>
        /// Size, in bytes, of the data referenced by lpParms
        /// </summary>
        public uint     cbParms;

        /// <summary>
        /// Interleave factor for interspersing stream data with data from the first stream.
        /// </summary>
        public uint     dwInterleaveEvery;

        /// <summary>
        /// Easily create a AviCompressOptions struct
        /// </summary>
        /// <param name="handler">Compression code fcc</param>
        /// <param name="quality">The compression quality</param>
        /// <returns></returns>

        public static AviCompressOptions New(uint handler, uint quality)
        {
            AviCompressOptions cmp = new AviCompressOptions();
            cmp.fccHandler = handler;
            cmp.dwQuality = quality;
            return cmp;
        }
    }

    /// <summary>
    /// Handles the errors of the AVIfile api
    /// </summary>
    abstract class AviError
    {           
        /// <summary>
        /// Keeps record if there occured an error
        /// </summary>
        protected bool hasError = false;

        /// <summary>
        /// File open error string
        /// </summary>
        protected string fileOpen = "None"; 

        /// <summary>
        /// File open error handler
        /// </summary>
        public int IeFileOpen
        {
            set
            {
                this.hasError = ((uint)value) != 0;
                this.fileOpen = ((uint)value).ToString(); ;
            }
        }

        /// <summary>
        /// Get the error report
        /// </summary>
        public virtual string report
        {
            get
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Check if the API should continue
        /// using the AVIfile api.
        /// </summary>
        /// <returns>True if we can continue</returns>
        public bool CanContinue()
        {
            return !this.hasError;
        }
    }

    /// <summary>
    /// Handles the error of the read AviFile
    /// api
    /// </summary>
    class AviReadError : AviError
    {
        /// <summary>
        /// Error strings
        /// </summary>
        private string  getStream   = "None",
                        streamInfo  = "None",
                        getFrame    = "None";

        /// <summary>
        /// Error handlers
        /// </summary>
        public int IeGetStream
        {
            set
            {
                this.hasError   = ((uint)value) != 0;
                this.getStream  = ((uint)value).ToString();
            }
        }

        public int IeStreamInfo
        {
            set
            {
                this.hasError   = ((uint)value) != 0;
                this.streamInfo = ((uint)value).ToString();
            }
        }

        public int IeGetFrame
        {
            set
            {
                this.hasError = ((uint)value) == 0;
                this.getFrame = ((uint)value) == 0 ? "Video codec not supported, please install a codec pack." : String.Empty;
            }
        }


        public override string report
        {
            get
            {
                return  "File open exception: "     + this.fileOpen     +
                        "\nGet stream exception: "  + this.getStream    +
                        "\nStream Info exception: " + this.streamInfo   +
                        "\nGet frame exception: "   + this.getFrame;
            }
        }
    }

    class AviWriteError : AviError
    {
        /// <summary>
        /// Error strings
        /// </summary>
        private string  createStream    = "None",
                        setFormat       = "None",
                        streamWrite     = "None",
                        compStream      = "None";

        /// <summary>
        /// Error handlers
        /// </summary>
        public int IeCreateStream
        {
            set
            {
                this.hasError       = value != 0;
                this.createStream   = ((uint)value).ToString();
            }
        }

        public int IeSetStreamFormat
        {
            set
            {
                this.hasError   = value != 0;
                this.setFormat  = ((uint)value).ToString();
            }
        }

        public int IeStreamWrite
        {
            set
            {
                this.hasError       = value != 0;
                this.streamWrite    = ((uint)value).ToString();
            }
        }

        public int IeMakeCompressedStream
        {
            set
            {
                this.hasError   = value != 0;
                this.compStream = ((uint)value).ToString();
            }
        }

        public override string report
        {
            get
            {
                return  "File open exception: "             + this.fileOpen     + 
                        "\nCreate Stream exception: "       + this.createStream +
                        "\nSet Stream Format exception: "   + this.setFormat    +
                        "\nStream Write exception: "        + this.streamWrite  +
                        "\nMakeCompressedstream exception: "+ this.compStream;
            }
        }
    }


    #endregion

    #region AVIFILE_METHODS

    class AviFile
    {
        [DllImport("avifil32.dll", SetLastError = true)]
        public static extern 
            int AVIFileCreateStream(
                IntPtr              pfile,
                out IntPtr          ppavi,
                ref AviStreamInfo   psi
            );

        [DllImport("avifil32.dll")]
        public static extern 
            int AVIFileGetStream(
                IntPtr      pfile,
                out IntPtr  ppavi,
                int         fccType,
                int         lParam
            );

        [DllImport("avifil32.dll")]
        public static extern
            void AVIFileExit();

        [DllImport("avifil32.dll")]
        public static extern
            void AVIFileInit();

        [DllImport("avifil32.dll")]
        public static extern 
            int AVIFileOpen(
                out IntPtr  ppfile,
                string      szFile,
                uint        mode,
                int         pclsidHandler
            );

        [DllImport("avifil32.dll")]
        public static extern
            uint AVIFileRelease(IntPtr pfile);

        [DllImport("avifil32.dll", EntryPoint = "AVIStreamInfo")]
        public static extern
            int AVIGetStreamInfo(
                IntPtr              pavi,
                out AviStreamInfo psi,
                int                 lSize
            );

        [DllImport("avifil32.dll")]
        public static extern 
            int AVIStreamInfo(
                IntPtr              aviStream,
                ref AviStreamInfo   psi,
                int                 lSize
            );

        [DllImport("avifil32.dll")]
        public static extern 
            uint AVIStreamLength(IntPtr pavi);

        [DllImport("avifil32.dll")]
        public static extern 
            int AVIStreamRead(
                IntPtr      pavi,
                int         lStart,
                int         lSamples,
                IntPtr      lpBuffer,
                int         cbBuffer,
                out int     plBytes,
                out int     plSamples
            );

        [DllImport("avifil32.dll")]
        public static extern
            uint AVIStreamGetFrameOpen(
                IntPtr                  pAVIStream,
                ref BitmapInfoHeader    bih
            );

        [DllImport("avifil32.dll")]
        public static extern 
            int AVIStreamRelease(IntPtr pavi);

        [DllImport("avifil32.dll")]
        public static extern
            int AVIStreamSetFormat(
                IntPtr                  aviStream,
                Int32                   lPos,
                ref BitmapInfoHeader    lpFormat,
                Int32                   cbFormat
            );

        [DllImport("avifil32.dll")]
        public static extern 
            int AVIStreamWrite(
                IntPtr  aviStream,
                uint     lStart,
                int     lSamples,
                IntPtr  lpBuffer,
                int     cbBuffer,
                int     dwFlags,
                int     dummy1,
                int     dummy2
            );

        [DllImport("avifil32.dll")]
        public static extern
            int AVIStreamGetFrame(
                uint pGetFrameObj,
                int lPos
            );

        [DllImport("avifil32.dll", PreserveSig = true)]
        public static extern 
            int AVIStreamStart(IntPtr pavi);

        [DllImport("avifil32.dll")]
        public static extern
            int AVIStreamGetFrameClose(uint pGetFrameObj);

        [DllImport("avifil32.dll")]
        public static extern
            int AVIMakeCompressedStream(
            out IntPtr              ppsCompressed,
            IntPtr                  psSource,
            ref AviCompressOptions  options,
            IntPtr                  pclsidHandler
            );


        /// <summary>
        /// Returns the fourCC code from the given characters
        /// </summary>
        /// <param name="code">A string consisting of four characters</param>
        /// <returns>The fourCC code</returns>
        public static uint MakeFourCC(string code)
        {
            return  ((uint)(byte)code[0]        | (uint)(byte)code[1] << 8) |
                    ((uint)(byte)code[2] << 16  | (uint)(byte)code[3] << 24);
        }
    }

    #endregion

    /// <summary>
    /// BitMapHeader information
    /// 
    /// BITMAPHEADER structure:
    /// offset   size    description
    /// 0	    2       signature, must be 4D42 hex
    /// 2    	4   	size of BMP file in bytes (unreliable)
    /// 6       2   	reserved, must be zero
    /// 8    	2   	reserved, must be zero
    /// 10      4   	offset to start of image data in bytes
    /// 14   	4   	size of BITMAPINFOHEADER structure, must be 40
    /// 18   	4   	image width in pixels
    /// 22   	4   	image height in pixels
    /// 26   	2   	number of planes in the image, must be 1
    /// 28   	2   	number of bits per pixel (1, 4, 8, or 24)
    /// 30      4   	compression type (0=none, 1=RLE-8, 2=RLE-4)
    /// 34   	4   	size of image data in bytes (including padding)
    /// 38   	4   	horizontal resolution in pixels per meter (unreliable)
    /// 42   	4   	vertical resolution in pixels per meter (unreliable)
    /// 46   	4   	number of colors in image, or zero
    /// 50   	4   	number of important colors, or zero
    /// </summary>
    enum BitmapHeader
    {
        /// <summary>
        /// The bmp file header size
        /// in bytes.
        /// </summary>
        HeaderSize = 54,

        /// <summary>
        /// The bmp file signature "BM"
        /// </summary>
        BMPsignature = 0x00004D42,

        /// <summary>
        /// The default bbp configuration.
        /// </summary>
        BitPerPixel = 24
    }

    /// <summary>
    /// Open file method flags
    /// 
    /// @XXX Documentation based on the MSDN
    ///      documentation
    /// </summary>
    [Flags]
    enum OpenMethod
    {
        /// <summary>
        /// Ignored.
        /// </summary>
        Cancel = 0x00000800,

        /// <summary>
        /// Creates a new file.
        /// </summary>
        Create = 0x00001000,

        /// <summary>
        /// Deletes a file.
        /// </summary>
        Delete = 0x00000200,

        /// <summary>
        /// Opens a file and then closes it.
        /// </summary>
        Exist = 0x00004000,

        /// <summary>
        /// Fills the OFSTRUCT structure, but does not do anything else.
        /// </summary>
        Parse = 0x00000100,

        /// <summary>
        /// Displays a dialog box if a requested file does not exist.
        /// </summary>
        Prompt = 0x00002000,

        /// <summary>
        /// Opens a file for reading only.
        /// </summary>
        Read = 0x00000000,

        /// <summary>
        /// Opens a file with read/write permissions.
        /// </summary>
        ReadWrite = 0x00000002,

        /// <summary>
        /// Opens a file by using information in the reopen buffer.
        /// </summary>
        Reopen = 0x00008000,
        
        /// <summary>
        /// For MS-DOS–based file systems, opens a file with compatibility
        /// mode, allows any process on a specified computer to open the 
        /// file any number of times.
        /// </summary>
        ShareCompat = 0x00000000,

        /// <summary>
        /// Opens a file without denying read or write access to other processes.
        /// </summary>
        ShareDenyNone = 0x00000040,

        /// <summary>
        /// Opens a file and denies read access to other processes.
        /// </summary>
        ShareDenyRead = 0x00000030,

        /// <summary>
        /// Opens a file and denies write access to other processes.
        /// </summary>
        ShareDenyWrite = 0x00000020,

        /// <summary>
        /// Opens a file with exclusive mode, and denies both read/write access to other processes.
        /// </summary>
        ShareExclusive = 0x00000010,

        /// <summary>
        /// Opens a file for write access only.
        /// </summary>
        Write = 0x00000001
    }

    /// <summary>
    /// The streamtype information
    /// </summary>
    static class StreamType
    {
        /// <summary>
        /// Video stream fcc
        /// </summary>
        public static uint Video
        {
            get { return AviFile.MakeFourCC("vids"); }
        }
    }
    /// <summary>
    /// Compression information
    /// </summary>
    static class AviCompression
    {
        /// <summary>
        /// Highest quallity video,
        /// with the worst compmression
        /// </summary>
        public static uint HighestQuality
        {
            get { return 1000; }
        }

        /// <summary>
        /// Huffyuv video codec fcc
        /// HFYU
        /// </summary>
        public static uint Huffyuv
        {
            get { return AviFile.MakeFourCC("HFYU");}
        }

        public static uint Default
        {
            get { return AviFile.MakeFourCC("msvc"); }
        }
    }

    /// <summary>
    /// Creates a temporary file, which
    /// will be deleted when the object
    /// gets disposed.
    /// </summary>
    class TempFile : IDisposable
    {
        /// <summary>
        /// Path to the temporary files
        /// </summary>
        private string path;

        public TempFile(string extension = ".tmp")
        {
            //generate a random file name
            this.path = (Path.GetTempPath() + Path.GetRandomFileName()).Replace(".", "") + extension;
        }

        ~TempFile()
        {
            this.Dispose();
        }

        /// <summary>
        /// GC Dispose callback
        /// </summary>
        public void Dispose()
        {
            this.Delete();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The path to the
        /// temporary file
        /// </summary>
        public string file
        {
            get { return this.path; }
        }

        /// <summary>
        /// Delete the temporary file
        /// if it exists
        /// </summary>
        private void Delete()
        {
            if (File.Exists(this.path))
            {
                try
                {
                    File.Delete(this.path);
                }
                catch { }
            }
        }
    }

    class TempStream
    {
        public Stream stream;
        private TempFile tmpfile = new TempFile();

        public TempStream()
        {
            stream = InputFile.CreateBufferedStream(tmpfile.file, true);
        }
    }

    /// <summary>
    /// Calls the ffmpeg.exe to encode files
    /// </summary>
    class FFmpegConverter
    {
        /// <summary>
        /// Path to the ffmpeg exe
        /// </summary>
        static private string ffmpegPath = "./lib/ffmpeg/bin/ffmpeg";

        /// <summary>
        /// Encodes a file
        /// </summary>
        /// <param name="src">The video to encode</param>
        /// <param name="dest">The destination of the encoded file</param>
        /// <param name="type">The encoding type</param>
        static public void Encode(string src, string dest, string type)
        {
            if (File.Exists(src))
            {
                string[] args = { src, dest, "-vcodec" + type + " -an "};
                FFmpegConverter.startFFmpeg(args);
            }
        }

        /// <summary>
        /// Copies the audio from one file and the video 
        /// from another, merging it to one video
        /// </summary>
        /// <param name="src">The source of the audio</param>
        /// <param name="dest">The destination of the audio stream</param>
        /// <returns>A tempfile containing the new video</returns>
        static public TempFile CopyAudioFrom(string src, string dest)
        {
            TempFile tmpFile = new TempFile();
            string[] args = { dest, tmpFile.file, "-i "+ src + " -codec copy -map 0:v -map1:a "};
            FFmpegConverter.startFFmpeg(args);

            return tmpFile;
        }

        /// <summary>
        /// Estimate the uncompressed video size
        /// </summary>
        /// <param name="height">The height of the video</param>
        /// <param name="width">The width of the video</param>
        /// <param name="bpp">The bit per pixel count</param>
        /// <param name="fps">The framerate</param>
        /// <param name="audioByterate">The audio byte rate per second</param>
        /// <param name="frameCount">The amount of frames</param>
        /// <returns>The estimated megabytes needed for the uncompressed video</returns>
        static public int EstimateUncompressedSize(int height, int width, ushort bpp, ushort fps, int audioByterate, int frameCount)
        {
            return ((height * width * frameCount * bpp) / 8 + (frameCount / fps * audioByterate))/(1024*1024);
        }

        /// <summary>
        /// Calls the ffmpeg exe with the given 
        /// arguments.
        /// </summary>
        /// <param name="args">
        /// String array
        /// [0] Source file
        /// [1] Ffmpeg arugments
        /// [2] Destination path
        /// </param>
        static private void startFFmpeg(string[] args)
        {
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.FileName = FFmpegConverter.ffmpegPath;
            proc.StartInfo.Arguments = "-i " + args[0] +  args[2] + args[1];

            if (proc.Start())
            {
                StreamReader console = proc.StandardOutput;
                string output;
                while((output = console.ReadLine()) != null)
                {
                    Debug.WriteLine(output);
                }

                proc.Close();
            }
            else
            {
                throw new Exception("Video encoding failed");
            }
        }

    }

    /// <summary>
    /// An avi file reader stream
    /// </summary>
    /// <example>
    /// 
    /// AviStream avi = new AviStream(OpenMethod.ShareDenyNone, "./test.avi");
    /// avi.GetFrame(0).Save("temp.bmp");
    /// for (int i = 0; i < 100; i++)
    ///     avi.SetFrame(avi.GetFrame(i),i);
    /// avi.SaveCachedVideo("./","hiddenmessage.avi");
    /// avi.Close();
    /// </example>
    class AviStream : IDisposable
    {
        public uint streamLength;
        public bool ReadyToUse = true;
        private MemoryStream    pictureBuffer   = new MemoryStream();
        private TempFile        cachedVid       = new TempFile();
        private IntPtr          aviStream       = IntPtr.Zero,
                                aviReadFile     = IntPtr.Zero;
        private BinaryWriter    pictureWriter;
        private AviStreamInfo   streamInfo;
        private AviWriter       writeStream;
        private OpenMethod      openMethod;
        private int     streamStart;
        private uint    frameReader;

        public AviStream(OpenMethod openMethod, string openReadFile = "")
        {
            AviFile.AVIFileInit();

            this.openMethod = openMethod;
            this.pictureWriter = new BinaryWriter(this.pictureBuffer);

            if (openReadFile != String.Empty)
                this.ReadyToUse = this.OpenRead(openReadFile);

            if (this.canWrite())
            {
                this.writeStream = new AviWriter();
                this.writeStream.openWrite(this.cachedVid.file,this.getHeight(),this.getWidth());
            }

        }

        ~AviStream()
        {
            this.Close();
            this.pictureWriter.Close();
            this.pictureBuffer.Close();

            AviFile.AVIFileExit();
        }

        #region AVIREAD
        public bool OpenRead(string fileName)
        {
            
            if (File.Exists(fileName))
            {
                AviReadError error = new AviReadError();

                error.IeFileOpen = AviFile.AVIFileOpen(
                    out this.aviReadFile, fileName,
                    (uint)OpenMethod.ShareDenyWrite, 0
                    );

                if (error.CanContinue())
                {

                    error.IeGetStream = AviFile.AVIFileGetStream(
                        this.aviReadFile, out this.aviStream,
                        (int)StreamType.Video, 0
                        );

                    if (error.CanContinue())
                    {

                        this.streamLength = AviFile.AVIStreamLength(this.aviStream);
                        this.streamStart = AviFile.AVIStreamStart(this.aviStream);

                        error.IeStreamInfo = AviFile.AVIStreamInfo(
                            this.aviStream, ref this.streamInfo,
                            Marshal.SizeOf(streamInfo)
                            );

                        if (error.CanContinue())
                        {
                            BitmapInfoHeader header = BitmapInfoHeader.New(this.getHeight(), this.getWidth());

                            this.frameReader = AviFile.AVIStreamGetFrameOpen(this.aviStream, ref header);
                            error.IeGetFrame = (int)this.frameReader;
                        }
                    }

                    if (!error.CanContinue())
                    {
                        OpenAVIError aviError = new OpenAVIError();
                        aviError.OnThrowError(new Exception());
                        return false;
                    }
                }

            }
            return true;
        }

        public void SetFrame(Bitmap frame, int position)
        {
            this.canUseWriteMethod();

            if (position > this.streamLength)
                throw new Exception("Trying to reach a non existing frame, \n frame out of range");

            this.writeStream.SetFrame(frame, (uint)position);
        }

        public Bitmap GetFrame(int position)
        {
            if (!this.ReadyToUse || this.frameReader == 0)
                return null;

            if (position > this.streamLength)
                throw new Exception("Trying to reach a non existing frame, \n frame out of range");

            this.resetMemStream();

            int StreamFrame = AviFile.AVIStreamGetFrame(this.frameReader, this.streamStart + position);

            BitmapInfoHeader infoHeader = this.getInfoHeader(StreamFrame);

            BitmapFileHeader fileHeader = BitmapFileHeader.New(
                (int)BitmapHeader.HeaderSize + infoHeader.biSizeImage, 
                (uint)Marshal.SizeOf(infoHeader)
                );

            TempFile tmpFile = new TempFile();

            BinaryWriter bWriter = new BinaryWriter(this.pictureBuffer);

            bWriter.Write(fileHeader.bfType);
            bWriter.Write(fileHeader.bfSize);
            bWriter.Write(fileHeader.bfReserved1);
            bWriter.Write(fileHeader.bfReserved2);
            bWriter.Write(fileHeader.bfOffBits);
            bWriter.Write(this.getBitmapInfo(new IntPtr(StreamFrame), ref infoHeader));
            bWriter.Write(this.getPixelData(new IntPtr(StreamFrame), ref infoHeader));

            return new Bitmap(this.pictureBuffer);
        }

        public void Close()
        {
            this.closeReadFrame();
            this.closeReadStream();
            this.releaseReadFile();
            this.pictureWriter.Dispose(); 
            
            if (canWrite())
                this.writeStream.Close();

            this.pictureBuffer.Dispose();

        }

        public string GetCachedVideo()
        {
            this.canUseWriteMethod();

            return this.cachedVid.file;
        }

        public void SaveCachedVideo(string targetPath, string filename)
        {
            this.canUseWriteMethod();

            this.writeStream.SaveFile(targetPath, filename);
        }

        public void Dispose()
        {
            if (canWrite())
                this.writeStream.Dispose();

            this.cachedVid.Dispose();
            this.Close();
            GC.SuppressFinalize(this);
        }

        #endregion

        #region PRVTAVIREAD
        private void resetMemStream()
        {
            byte[] buffer = this.pictureBuffer.GetBuffer();
            Array.Clear(buffer, 0, buffer.Length);
            pictureBuffer.Position = 0;
            pictureBuffer.SetLength(0);
        }

        private byte[] getPixelData(IntPtr StreamFrame, ref BitmapInfoHeader infoHeader)
        {
            byte[] pixelData = new byte[infoHeader.biSizeImage];
            int pixelBlock = StreamFrame.ToInt32() + (int)infoHeader.biSize;

            for (int blockI = 0; blockI < infoHeader.biSizeImage; blockI++, pixelBlock++)
                pixelData[blockI] = Marshal.ReadByte(new IntPtr(pixelBlock));
                
            return pixelData;
        }

        private byte[] getBitmapInfo(IntPtr StreamFrame, ref BitmapInfoHeader infoHeader)
        {
            byte[] infoData = new byte[Marshal.SizeOf(infoHeader)];
            IntPtr memBuffer = Marshal.AllocHGlobal(infoData.Length);
            int dataBlock = memBuffer.ToInt32();

            Marshal.StructureToPtr(infoHeader, memBuffer, false);

            for (int blockI = 0; blockI <infoData.Length; blockI++, dataBlock++)
                infoData[blockI] = Marshal.ReadByte(new IntPtr(dataBlock));

            return infoData;
        }

        private BitmapInfoHeader getInfoHeader(int StreamFrame)
        {
            BitmapInfoHeader header = new BitmapInfoHeader();
            return (BitmapInfoHeader)Marshal.PtrToStructure(new IntPtr(StreamFrame), header.GetType());
        }

        private void closeReadFrame()
        {
            if (this.frameReader != 0)
            {
                AviFile.AVIStreamGetFrameClose(this.frameReader);
                this.frameReader = 0;
            }
        }

        private void closeReadStream()
        {
            if (this.aviStream != IntPtr.Zero)
            {
                AviFile.AVIStreamRelease(this.aviStream);
                this.aviStream = IntPtr.Zero;
            }
        }

        private void releaseReadFile()
        {
            if (this.aviReadFile != IntPtr.Zero)
            {
                Debug.Write(AviFile.AVIFileRelease(this.aviReadFile));
                this.aviReadFile = IntPtr.Zero;
            }
        }

        private int getHeight()
        {
            return this.streamInfo.rcFrame.bottom;
        }

        private int getWidth()
        {
            return this.streamInfo.rcFrame.right;
        }

        private void canUseWriteMethod()
        {
            if (!this.canWrite())
                throw new Exception("You cannot write to this stream, please use another open method.");
        }

        private bool canWrite()
        {
            return ((this.openMethod & OpenMethod.Write) == OpenMethod.Write) ||
                   ((this.openMethod & OpenMethod.ShareDenyNone) == OpenMethod.ShareDenyNone);
        }
        #endregion
    }

    class AviWriter : IDisposable
    {
        private IntPtr aviWriteStream   = IntPtr.Zero,
                       aviCompStream    = IntPtr.Zero,
                       aviWriteFile     = IntPtr.Zero;
        private TempFile tmpFile = new TempFile(".tmp.avi");
        private string filename;
        private int width           = 0,
                    height          = 0,
                    nChannels       = 3,
                    stride          = 0;
        private uint    fps             = 24,
                        vidFCC          = StreamType.Video,
                        vidCodec        = AviCompression.Default,
                        quality         = AviCompression.HighestQuality;
                    

        public AviWriter()
        {
            AviFile.AVIFileInit();
        }

        ~AviWriter()
        {
            this.Close();
            AviFile.AVIFileExit();
        }

        public void openWrite(string filename, int height, int width)
        {
            //check if the resolution is a multiple of two
            if( (width & 1) != 0 || (height & 1) != 0)
            {
                throw new Exception("The resolution has to be a multiple of two");
            }

            this.height = height;
            this.width  = width;
            this.filename = filename;
            this.setStride();

            AviWriteError error = new AviWriteError();

            error.IeFileOpen = AviFile.AVIFileOpen(
                out this.aviWriteFile, this.tmpFile.file,
                (uint)(OpenMethod.ShareDenyNone | OpenMethod.Create),
                0);

            if (error.CanContinue())
            {
                BitmapInfoHeader infoHeader = BitmapInfoHeader.New(this.height, this.width);


                AviStreamInfo asi = AviStreamInfo.New(
                    this.vidFCC, this.vidCodec, this.fps,
                    (uint)(this.stride),
                    this.quality, this.width, this.height
                    );

                error.IeCreateStream = AviFile.AVIFileCreateStream(
                    this.aviWriteFile,
                    out this.aviWriteStream, 
                    ref asi
                    );

                if (error.CanContinue())
                {
                    #if __USECOMPRESSION__
                        AviCompressOptions compOptions = AviCompressOptions.New(this.vidFCC,this.quality);

                        error.IeMakeCompressedStream = AviFile.AVIMakeCompressedStream(out this.aviCompStream, this.aviWriteStream, ref compOptions, IntPtr.Zero);
                    #endif

                    if (error.CanContinue())
                    {
                        #if __USECOMPRESSION__
                            error.IeSetStreamFormat = AviFile.AVIStreamSetFormat(this.aviCompStream, 0, ref infoHeader, Marshal.SizeOf(infoHeader));
                        #else
                            error.IeSetStreamFormat = AviFile.AVIStreamSetFormat(this.aviWriteStream, 0, ref infoHeader,Marshal.SizeOf(infoHeader));
                        #endif
                    }
                }
            }

            if (!error.CanContinue())
            {
                this.Close();
                throw new Exception(error.report);
            }
        }

        public void SetFrame(Bitmap frame, uint position)
        {
            AviWriteError error = new AviWriteError();

            frame.RotateFlip(RotateFlipType.RotateNoneFlipY);

            BitmapData imgData = frame.LockBits(new Rectangle(0, 0, frame.Width, frame.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            #if __USECOMPRESSION__
                error.IeStreamWrite = AviFile.AVIStreamWrite(this.aviCompStream, position, 1, imgData.Scan0, this.height * imgData.Stride, 0, 0, 0);
            #else
                error.IeStreamWrite = AviFile.AVIStreamWrite(this.aviWriteStream, position, 1, imgData.Scan0, this.height * imgData.Stride, 0, 0, 0);
            #endif


            frame.UnlockBits(imgData);

            if (!error.CanContinue())
            {
                this.Close();
                throw new Exception(error.report);
            }
        }

        public TempFile CopyAudio(string src)
        {
            return FFmpegConverter.CopyAudioFrom(src, this.filename);
        }

        public void SaveFile(string targetPath, string fileName)
        {
            if (File.Exists(this.tmpFile.file))
            {
                if (Directory.Exists(targetPath))
                {
                    File.Copy(this.tmpFile.file, targetPath + fileName,true);
                }
            }
        }

        public void Dispose()
        {
            this.Close();
            this.tmpFile.Dispose();
            GC.SuppressFinalize(this);
            AviFile.AVIFileExit();
        }

        private void setStride()
        {
            this.stride = this.width * this.nChannels;
            if ((this.stride & 3) != 0)
                this.stride += 4 - (this.stride & 3);
        }

        public void Close(bool disposed = false)
        {
            #if __USECOMPRESSION__
            this.closeCompressedStream();
            #endif
            this.closeWriteStream();
            this.releaseWriteFile();
        }

        private void closeWriteStream()
        {
            if (this.aviWriteStream != IntPtr.Zero)
            {
                AviFile.AVIStreamRelease(this.aviWriteStream);
                this.aviWriteStream = IntPtr.Zero;
            }
        }

        private void closeCompressedStream()
        {
            if (this.aviCompStream != IntPtr.Zero)
            {
                AviFile.AVIStreamRelease(this.aviCompStream);
                this.aviCompStream = IntPtr.Zero;
            }
        }

        private void releaseWriteFile()
        {
            if (this.aviWriteFile != IntPtr.Zero)
            {
                AviFile.AVIFileRelease(this.aviWriteFile);
                this.aviWriteFile = IntPtr.Zero;
            }
        }
    }
}
