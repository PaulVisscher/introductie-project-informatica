﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace steganography
{
    partial class GUIform : Form
    {
        public Core Core = new Core();

        public GUIform()
        {
            InitializeComponent();
            this.imageTab.setParent(this);
            this.videoTab.setParent(this);
            this.soundTab.setParent(this);
            this.recoverTab.setParent(this);
        }

        public Error Error
        {
            set
            {
                this.OnError(value);
            }
        }

        public void OnError(Error e)
        {
            if( this.MediaTabs.SelectedTab == this.tabImage)
            {
                this.imageTab.infoBox.Text = e.InfoText;
            }
            else if( this.MediaTabs.SelectedTab == this.tabSound)
            {
                this.soundTab.infoBox.Text = e.InfoText;
            }            
            else if( this.MediaTabs.SelectedTab == this.tabVideo)
            {
                this.videoTab.infoBox.Text = e.InfoText;
            }
            else if( this.MediaTabs.SelectedTab == this.tabRecover)
            {
                this.recoverTab.infoBox.Text = e.InfoText;
            }
        }

        private void imageTab_Load(object sender, EventArgs e)
        {

        }
    }
}
