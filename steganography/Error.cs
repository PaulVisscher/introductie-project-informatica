﻿/**
 * @file        Error.cs
 * @brief       Holds the programs error handling
 *
 * @details     This file discribes the way errors should be managed, and how 
 *              they are called from the error reporters.
 *
 * @author      Paul Visscher
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace steganography
{
    interface IError
    {
        void OnThrowError(Exception e);
    }

    /// <summary>
    /// Handles the throwing of errors
    /// </summary>
    static class ErrorHandler
    {
        /// <summary>
        /// Throws an error specified by the given error object
        /// </summary>
        /// <param name="errorObj">The error object</param>
        /// <param name="e">The exception object</param>
        /// <returns>True/false if the error was handled propperly</returns>
        public static void ThrowError(Error errorObj, Exception e = null)
        {
            if (e == null)
                e = new Exception();

           errorObj.OnThrowError(e);
        }
    }

    /// <summary>
    /// Abstract class with a few default methods to handle errors.
    /// </summary>
    abstract class Error : EventArgs, IError
    {
        public string InfoText = "";

        /// <summary>
        /// Callback when an event occurs
        /// </summary>
        /// <param name="e">Exception object</param>
        /// <returns>True/false if the error was handled propperly</returns>
        public virtual void OnThrowError(Exception em)
        {
            this.onError(em);

            GUIform MainForm = this.getMainForm();
            if (MainForm != null)
            {
                MainForm.BeginInvoke((MethodInvoker)delegate
                {
                    MainForm.Error = this;
                });
            }

        }
        
        /// <summary>
        /// Shows a message box to the user with the given arguments.
        /// </summary>
        /// <param name="Title">The title</param>
        /// <param name="Message">The message</param>
        /// <param name="Buttons">The used buttons</param>
        /// <param name="Icon">The icon used</param>
        /// <returns>The clicked button (DialogResult)</returns>
        public DialogResult ShowMessage(
            String Title, 
            String Message, 
            MessageBoxButtons Buttons = MessageBoxButtons.OK,
            MessageBoxIcon Icon = MessageBoxIcon.None
            )
        {
            return MessageBox.Show(Message, Title, Buttons, Icon);
        }

        /// <summary>
        /// Retrieve the main form from the open form list
        /// </summary>
        /// <returns>The main form</returns>
        public GUIform getMainForm()
        {
            GUIform main = null;
            foreach (Form f in Application.OpenForms)
            {
                if (f is GUIform)
                {
                    main = (GUIform)f;
                }
            }
            return main;
        }

        /// <summary>
        /// Check if an object has a method
        /// </summary>
        /// <param name="obj">The object to check on</param>
        /// <param name="methodName">The method name</param>
        /// <returns>True/false if the object has the method</returns>
        private bool HasMethod(Object obj, string methodName)
        {
            Type type = obj.GetType();
            return type.GetMethod(methodName) != null;
        }

        protected virtual void onError(Exception e) { }
    }

    /// <summary>
    /// Allows other error objects to inherit common messagebox properties
    /// </summary>
    class ErrorWithMessageBox : Error
    {
        protected String Message, Title;
        protected MessageBoxButtons Buttons;
        protected MessageBoxIcon Icon;

        /// <summary>
        /// Set common button and icon for the messagebox
        /// </summary>
        public ErrorWithMessageBox()
        {
            this.Buttons = MessageBoxButtons.OK;
            this.Icon = MessageBoxIcon.Warning;
        }

        /// <summary>
        /// Display a messagebox containg error-relevant information
        /// </summary>
        /// <param name="e">Exception object</param>
        protected override void onError(Exception e)
        {
            this.ShowMessage(this.Title, this.Message + "\n\n Error code:\t" + e.Message, this.Buttons, this.Icon);
        }

    }

    class ImportImageError : Error
    {
        public ImportImageError()
            : base()
        {
            this.InfoText = "Import Error" + Environment.NewLine + "There was an error importing your image, please check your file path.";
        }
    }

    class InputError : Error
    {
        public InputError()
            : base()
        {
            this.InfoText = "Input Error" + Environment.NewLine + "The file you want hide could not be opened correctly. Please try a different file.";
        }
    }

    class OutputError : Error
    {
        public OutputError()
            : base()
        {
            this.InfoText = "Output Error" + Environment.NewLine + "The output could not be written to the specified path. Please choose a different path.";
        }
    }

    class SaveKeyError : Error
    {
        public SaveKeyError()
            : base()
        {
            this.InfoText = "Save Key Error" + Environment.NewLine + "The savepath for the key is invalid.";
        }
    }

    class ReadKeyError : Error
    {
        public ReadKeyError()
            : base()
        {
            this.InfoText = "Read Key Error" + Environment.NewLine + "The keyfile could not be read correctly from the specified loadpath.";
        }
    }

    class RSADecryptError : Error
    {
        public RSADecryptError()
            : base()
        {
            this.InfoText = "RSA Decrypt Error Error" + Environment.NewLine + "Your data could not be decrypted. Are you sure the RSA key in the key file is valid?";
        }
    }

    class BitmapError : Error
    {
        public BitmapError()
            : base()
        {
            this.InfoText = "Bitmap Error" + Environment.NewLine + "The specified image is not a losless bitmap.";
        }
    }

    class CapacityError : Error
    {
        public CapacityError()
            : base()
        {
            this.InfoText = "Capacity Error" + Environment.NewLine + "The amount of data to be hidden is too large. Please decrease the size or try a different file to hide in.";
        }
    }

    class ImageFormatError : Error
    {
        public ImageFormatError()
            : base()
        {
            this.InfoText = "Image format Error" + Environment.NewLine + "The format of the image is not supported.";
        }
    }

    class CoverError : Error
    {
        public CoverError()
            : base()
        {
            this.InfoText = "Cover Error" + Environment.NewLine + "The carrier file is invalid.";
        }
    }

    class WAVEError : Error
    {
        public WAVEError()
            : base()
        {
            this.InfoText = "Wav Error" + Environment.NewLine + "The wav file is invalid.";
        }
    }

    class OpenAVIError : Error
    {
        public OpenAVIError()
            : base()
        {
            this.InfoText = "AVI Error" + Environment.NewLine + "Failed to open the AVI file. Have you installed the necessary codec pack?";
        }
    }
}