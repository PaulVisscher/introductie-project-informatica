﻿namespace steganography
{
    partial class RecoverTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelRecCarrier = new System.Windows.Forms.Label();
            this.labelRecKey = new System.Windows.Forms.Label();
            this.buttonProcessRec = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.carrierFilePath = new System.Windows.Forms.TextBox();
            this.carrierRecButton = new System.Windows.Forms.Button();
            this.labelRecOutput = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.outputPath = new System.Windows.Forms.TextBox();
            this.outputRecButton = new System.Windows.Forms.Button();
            this.passwordRecLabel = new System.Windows.Forms.Label();
            this.passwordRecBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.KeyPath = new System.Windows.Forms.TextBox();
            this.SaveKeyButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.aboutLabel = new System.Windows.Forms.Label();
            this.infoBox = new System.Windows.Forms.TextBox();
            this.logoBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.67911F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.64179F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.67911F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.logoBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.95253F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.08985F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.95762F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(458, 434);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.labelRecCarrier, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelRecKey, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.buttonProcessRec, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelRecOutput, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.passwordRecLabel, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.passwordRecBox, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(125, 124);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(207, 198);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // labelRecCarrier
            // 
            this.labelRecCarrier.AutoSize = true;
            this.labelRecCarrier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecCarrier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelRecCarrier.Location = new System.Drawing.Point(2, 5);
            this.labelRecCarrier.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.labelRecCarrier.Name = "labelRecCarrier";
            this.labelRecCarrier.Size = new System.Drawing.Size(56, 13);
            this.labelRecCarrier.TabIndex = 55;
            this.labelRecCarrier.Text = "Carrier file:";
            this.labelRecCarrier.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelRecCarrier.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.labelRecCarrier.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // labelRecKey
            // 
            this.labelRecKey.AutoSize = true;
            this.labelRecKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelRecKey.Location = new System.Drawing.Point(2, 25);
            this.labelRecKey.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.labelRecKey.Name = "labelRecKey";
            this.labelRecKey.Size = new System.Drawing.Size(56, 13);
            this.labelRecKey.TabIndex = 37;
            this.labelRecKey.Text = "Key file:";
            this.labelRecKey.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelRecKey.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.labelRecKey.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // buttonProcessRec
            // 
            this.buttonProcessRec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonProcessRec.Enabled = false;
            this.buttonProcessRec.Location = new System.Drawing.Point(70, 105);
            this.buttonProcessRec.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.buttonProcessRec.Name = "buttonProcessRec";
            this.buttonProcessRec.Size = new System.Drawing.Size(127, 88);
            this.buttonProcessRec.TabIndex = 59;
            this.buttonProcessRec.Text = "Process!";
            this.buttonProcessRec.UseVisualStyleBackColor = true;
            this.buttonProcessRec.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonProcessRec_MouseClick);
            this.buttonProcessRec.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.buttonProcessRec.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel3.Controls.Add(this.carrierFilePath, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.carrierRecButton, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(60, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(147, 20);
            this.tableLayoutPanel3.TabIndex = 53;
            // 
            // carrierFilePath
            // 
            this.carrierFilePath.BackColor = System.Drawing.Color.White;
            this.carrierFilePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carrierFilePath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.carrierFilePath.Location = new System.Drawing.Point(2, 2);
            this.carrierFilePath.Margin = new System.Windows.Forms.Padding(2);
            this.carrierFilePath.Name = "carrierFilePath";
            this.carrierFilePath.ReadOnly = true;
            this.carrierFilePath.Size = new System.Drawing.Size(98, 23);
            this.carrierFilePath.TabIndex = 22;
            this.carrierFilePath.TextChanged += new System.EventHandler(this.carrierFilePath_TextChanged);
            // 
            // carrierRecButton
            // 
            this.carrierRecButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carrierRecButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.carrierRecButton.Location = new System.Drawing.Point(102, 0);
            this.carrierRecButton.Margin = new System.Windows.Forms.Padding(0);
            this.carrierRecButton.Name = "carrierRecButton";
            this.carrierRecButton.Size = new System.Drawing.Size(45, 20);
            this.carrierRecButton.TabIndex = 0;
            this.carrierRecButton.Text = "Browse";
            this.carrierRecButton.UseVisualStyleBackColor = true;
            this.carrierRecButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.carrierRecButton_MouseClick);
            // 
            // labelRecOutput
            // 
            this.labelRecOutput.AutoSize = true;
            this.labelRecOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelRecOutput.Location = new System.Drawing.Point(2, 85);
            this.labelRecOutput.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.labelRecOutput.Name = "labelRecOutput";
            this.labelRecOutput.Size = new System.Drawing.Size(56, 13);
            this.labelRecOutput.TabIndex = 58;
            this.labelRecOutput.Text = "Output directory:";
            this.labelRecOutput.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelRecOutput.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.labelRecOutput.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.Controls.Add(this.outputPath, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.outputRecButton, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(60, 80);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(147, 20);
            this.tableLayoutPanel4.TabIndex = 54;
            // 
            // outputPath
            // 
            this.outputPath.BackColor = System.Drawing.Color.White;
            this.outputPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputPath.Enabled = false;
            this.outputPath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.outputPath.Location = new System.Drawing.Point(2, 2);
            this.outputPath.Margin = new System.Windows.Forms.Padding(2);
            this.outputPath.Name = "outputPath";
            this.outputPath.ReadOnly = true;
            this.outputPath.Size = new System.Drawing.Size(98, 23);
            this.outputPath.TabIndex = 22;
            this.outputPath.TextChanged += new System.EventHandler(this.outputPath_TextChanged);
            // 
            // outputRecButton
            // 
            this.outputRecButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputRecButton.Enabled = false;
            this.outputRecButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.outputRecButton.Location = new System.Drawing.Point(102, 0);
            this.outputRecButton.Margin = new System.Windows.Forms.Padding(0);
            this.outputRecButton.Name = "outputRecButton";
            this.outputRecButton.Size = new System.Drawing.Size(45, 20);
            this.outputRecButton.TabIndex = 0;
            this.outputRecButton.Text = "Browse";
            this.outputRecButton.UseVisualStyleBackColor = true;
            this.outputRecButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.outputRecButton_MouseClick);
            // 
            // passwordRecLabel
            // 
            this.passwordRecLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordRecLabel.AutoSize = true;
            this.passwordRecLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.passwordRecLabel.Location = new System.Drawing.Point(2, 45);
            this.passwordRecLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.passwordRecLabel.Name = "passwordRecLabel";
            this.passwordRecLabel.Size = new System.Drawing.Size(56, 13);
            this.passwordRecLabel.TabIndex = 60;
            this.passwordRecLabel.Text = "Password:";
            this.passwordRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.passwordRecLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.passwordRecLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // passwordRecBox
            // 
            this.passwordRecBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordRecBox.BackColor = System.Drawing.Color.White;
            this.passwordRecBox.Enabled = false;
            this.passwordRecBox.Location = new System.Drawing.Point(62, 45);
            this.passwordRecBox.Margin = new System.Windows.Forms.Padding(2, 5, 8, 2);
            this.passwordRecBox.Name = "passwordRecBox";
            this.passwordRecBox.PasswordChar = '*';
            this.passwordRecBox.Size = new System.Drawing.Size(137, 20);
            this.passwordRecBox.TabIndex = 61;
            this.passwordRecBox.TextChanged += new System.EventHandler(this.passwordRecBox_TextChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.Controls.Add(this.KeyPath, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.SaveKeyButton, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(60, 20);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(147, 20);
            this.tableLayoutPanel6.TabIndex = 52;
            // 
            // KeyPath
            // 
            this.KeyPath.BackColor = System.Drawing.Color.White;
            this.KeyPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeyPath.Enabled = false;
            this.KeyPath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.KeyPath.Location = new System.Drawing.Point(2, 2);
            this.KeyPath.Margin = new System.Windows.Forms.Padding(2);
            this.KeyPath.Name = "KeyPath";
            this.KeyPath.Size = new System.Drawing.Size(98, 23);
            this.KeyPath.TabIndex = 22;
            this.KeyPath.TextChanged += new System.EventHandler(this.KeyPath_TextChanged);
            // 
            // SaveKeyButton
            // 
            this.SaveKeyButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveKeyButton.Enabled = false;
            this.SaveKeyButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.SaveKeyButton.Location = new System.Drawing.Point(102, 0);
            this.SaveKeyButton.Margin = new System.Windows.Forms.Padding(0);
            this.SaveKeyButton.Name = "SaveKeyButton";
            this.SaveKeyButton.Size = new System.Drawing.Size(45, 20);
            this.SaveKeyButton.TabIndex = 0;
            this.SaveKeyButton.Text = "Browse";
            this.SaveKeyButton.UseVisualStyleBackColor = true;
            this.SaveKeyButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.KeyButton_MouseClick);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.aboutLabel, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.infoBox, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(122, 325);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(213, 109);
            this.tableLayoutPanel8.TabIndex = 54;
            // 
            // aboutLabel
            // 
            this.aboutLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutLabel.AutoSize = true;
            this.aboutLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aboutLabel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.aboutLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.aboutLabel.Location = new System.Drawing.Point(102, 99);
            this.aboutLabel.Margin = new System.Windows.Forms.Padding(0);
            this.aboutLabel.Name = "aboutLabel";
            this.aboutLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.aboutLabel.Size = new System.Drawing.Size(111, 10);
            this.aboutLabel.TabIndex = 44;
            this.aboutLabel.Text = "About this program";
            this.aboutLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.aboutLabel_MouseClick);
            // 
            // infoBox
            // 
            this.infoBox.BackColor = System.Drawing.SystemColors.Control;
            this.infoBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoBox.Location = new System.Drawing.Point(3, 20);
            this.infoBox.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.infoBox.Multiline = true;
            this.infoBox.Name = "infoBox";
            this.infoBox.ReadOnly = true;
            this.infoBox.Size = new System.Drawing.Size(207, 76);
            this.infoBox.TabIndex = 43;
            // 
            // logoBox
            // 
            this.logoBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoBox.Image = global::steganography.Properties.Resources.steglogo;
            this.logoBox.Location = new System.Drawing.Point(125, 3);
            this.logoBox.Name = "logoBox";
            this.logoBox.Size = new System.Drawing.Size(207, 115);
            this.logoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.logoBox.TabIndex = 55;
            this.logoBox.TabStop = false;
            // 
            // RecoverTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "RecoverTab";
            this.Size = new System.Drawing.Size(458, 434);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label labelRecKey;
        private System.Windows.Forms.Label labelRecCarrier;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.TextBox KeyPath;
        private System.Windows.Forms.Button SaveKeyButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.TextBox carrierFilePath;
        private System.Windows.Forms.Button carrierRecButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.TextBox outputPath;
        private System.Windows.Forms.Button outputRecButton;
        private System.Windows.Forms.Label labelRecOutput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label aboutLabel;
        public System.Windows.Forms.TextBox infoBox;
        private System.Windows.Forms.PictureBox logoBox;
        private System.Windows.Forms.Button buttonProcessRec;
        private System.Windows.Forms.Label passwordRecLabel;
        private System.Windows.Forms.TextBox passwordRecBox;
    }
}
