﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace steganography
{
    partial class OptionsImage : UserControl
    {
        private TabControl tabcontr;
        private bool usesRandomMode = true;
        private bool usesEncryption = true;

        public OptionsImage(TabControl tc)
        {
            InitializeComponent();
            this.steganoType.SelectedIndex = 0;
            this.tabcontr = tc;
        }

        private void setPasswordUse()
        {
            if (this.usesEncryption || this.usesRandomMode)
            {
                this.passwordBox.ReadOnly = false;
                this.passwordBox.BackColor = Color.White;
            }
            else
            {
                this.passwordBox.ReadOnly = true;
                this.passwordBox.BackColor = SystemColors.ControlLight;
            }
        }

        private void control_MouseEnter(object sender, EventArgs e)
        {
            this.tabcontr.control_MouseEnter(sender, e);
        }

        private void control_MouseLeave(object sender, EventArgs e)
        {
            this.tabcontr.control_MouseLeave(sender, e);
        }

        private void steganoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabcontr != null)
                this.tabcontr.prnt.Core.Cimage.ScatterData = this.steganoType.SelectedIndex == 0;
            this.usesRandomMode = this.steganoType.SelectedIndex == 0;
            this.setPasswordUse();

            if(this.tabcontr != null)
                this.tabcontr.setPreview();
        }

        private void useEncryption_CheckedChanged(object sender, EventArgs e)
        {
            this.usesEncryption = this.useEncryption.Checked;
            this.tabcontr.prnt.Core.Cimage.UseEncryption = this.useEncryption.Checked;
            this.setPasswordUse();
        }

        private void LSBbits_ValueChanged(object sender, EventArgs e)
        {
            this.tabcontr.prnt.Core.Cimage.LSBCount = (int)this.LSBbits.Value;
            this.tabcontr.setPreview();
        }

        private void passwordBox_TextChanged(object sender, EventArgs e)
        {
            this.tabcontr.prnt.Core.Cimage.Password = this.passwordBox.Text;
        }
    }
}
