﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Windows.Forms;

namespace steganography
{
    public static class Config
    {
        public static string About
        {
            get
            {
                return Config.Read("About");
            }
        }

        public static string Info(string name)
        {
            try
            {
                return Config.Read(name);
            }
            catch
            {
                return "";
            }
        }       

        private static string Read(string s)
        {
            string read = "";
            if (s != "")
            {
                try
                {
                    read = ConfigurationManager.AppSettings[s].ToString();
                }
                catch
                {
                }
            }
            return read;
        }
    }
}
