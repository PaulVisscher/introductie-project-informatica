﻿namespace steganography
{
    partial class TabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.StegOptionsTable = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.aboutLabel = new System.Windows.Forms.Label();
            this.infoBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.carrierFilePath = new System.Windows.Forms.TextBox();
            this.carrierFileButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.SaveKeyPath = new System.Windows.Forms.TextBox();
            this.SaveKeyButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.SaveOutputPath = new System.Windows.Forms.TextBox();
            this.SaveOutputButon = new System.Windows.Forms.Button();
            this.SaveKeyLabel = new System.Windows.Forms.Label();
            this.SaveLabel = new System.Windows.Forms.Label();
            this.CarrierLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.inputTextbox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.HideFilePath = new System.Windows.Forms.TextBox();
            this.hideFileButton = new System.Windows.Forms.Button();
            this.rbHideText = new System.Windows.Forms.RadioButton();
            this.rbHideFile = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.previewTableCont = new System.Windows.Forms.TableLayoutPanel();
            this.PreviewOptionsTable = new System.Windows.Forms.TableLayoutPanel();
            this.saveImageHideButton = new System.Windows.Forms.Button();
            this.previewTable = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.StegOptionsTable.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.previewTableCont.SuspendLayout();
            this.PreviewOptionsTable.SuspendLayout();
            this.previewTable.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.StegOptionsTable, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.previewTableCont, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 720);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // StegOptionsTable
            // 
            this.StegOptionsTable.ColumnCount = 2;
            this.StegOptionsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.StegOptionsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.StegOptionsTable.Controls.Add(this.tableLayoutPanel8, 1, 1);
            this.StegOptionsTable.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.StegOptionsTable.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.StegOptionsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StegOptionsTable.Location = new System.Drawing.Point(0, 360);
            this.StegOptionsTable.Margin = new System.Windows.Forms.Padding(0);
            this.StegOptionsTable.Name = "StegOptionsTable";
            this.StegOptionsTable.RowCount = 2;
            this.StegOptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.StegOptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StegOptionsTable.Size = new System.Drawing.Size(800, 360);
            this.StegOptionsTable.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.aboutLabel, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.infoBox, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(400, 100);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(400, 260);
            this.tableLayoutPanel8.TabIndex = 53;
            // 
            // aboutLabel
            // 
            this.aboutLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutLabel.AutoSize = true;
            this.aboutLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aboutLabel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.aboutLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.aboutLabel.Location = new System.Drawing.Point(289, 250);
            this.aboutLabel.Margin = new System.Windows.Forms.Padding(0);
            this.aboutLabel.Name = "aboutLabel";
            this.aboutLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.aboutLabel.Size = new System.Drawing.Size(111, 10);
            this.aboutLabel.TabIndex = 44;
            this.aboutLabel.Text = "About this program";
            this.aboutLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.aboutLabel_MouseClick);
            // 
            // infoBox
            // 
            this.infoBox.BackColor = System.Drawing.SystemColors.Control;
            this.infoBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoBox.Location = new System.Drawing.Point(3, 20);
            this.infoBox.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.infoBox.Multiline = true;
            this.infoBox.Name = "infoBox";
            this.infoBox.ReadOnly = true;
            this.infoBox.Size = new System.Drawing.Size(394, 227);
            this.infoBox.TabIndex = 43;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.SaveKeyLabel, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.SaveLabel, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.CarrierLabel, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(400, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(400, 100);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel9.Controls.Add(this.carrierFilePath, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.carrierFileButton, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(50, 0);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(350, 18);
            this.tableLayoutPanel9.TabIndex = 53;
            // 
            // carrierFilePath
            // 
            this.carrierFilePath.BackColor = System.Drawing.Color.White;
            this.carrierFilePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carrierFilePath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.carrierFilePath.Location = new System.Drawing.Point(2, 2);
            this.carrierFilePath.Margin = new System.Windows.Forms.Padding(2);
            this.carrierFilePath.Name = "carrierFilePath";
            this.carrierFilePath.Size = new System.Drawing.Size(301, 23);
            this.carrierFilePath.TabIndex = 22;
            this.carrierFilePath.TextChanged += new System.EventHandler(this.carrierFilePath_TextChanged);
            // 
            // carrierFileButton
            // 
            this.carrierFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carrierFileButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.carrierFileButton.Location = new System.Drawing.Point(305, 0);
            this.carrierFileButton.Margin = new System.Windows.Forms.Padding(0);
            this.carrierFileButton.Name = "carrierFileButton";
            this.carrierFileButton.Size = new System.Drawing.Size(45, 18);
            this.carrierFileButton.TabIndex = 0;
            this.carrierFileButton.Text = "Open";
            this.carrierFileButton.UseVisualStyleBackColor = true;
            this.carrierFileButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.carrierFileButton_MouseClick);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.Controls.Add(this.SaveKeyPath, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.SaveKeyButton, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(50, 54);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(350, 18);
            this.tableLayoutPanel6.TabIndex = 51;
            // 
            // SaveKeyPath
            // 
            this.SaveKeyPath.BackColor = System.Drawing.Color.White;
            this.SaveKeyPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveKeyPath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.SaveKeyPath.Location = new System.Drawing.Point(2, 2);
            this.SaveKeyPath.Margin = new System.Windows.Forms.Padding(2);
            this.SaveKeyPath.Name = "SaveKeyPath";
            this.SaveKeyPath.Size = new System.Drawing.Size(301, 23);
            this.SaveKeyPath.TabIndex = 22;
            this.SaveKeyPath.TextChanged += new System.EventHandler(this.SaveKeyPath_TextChanged);
            // 
            // SaveKeyButton
            // 
            this.SaveKeyButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveKeyButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.SaveKeyButton.Location = new System.Drawing.Point(305, 0);
            this.SaveKeyButton.Margin = new System.Windows.Forms.Padding(0);
            this.SaveKeyButton.Name = "SaveKeyButton";
            this.SaveKeyButton.Size = new System.Drawing.Size(45, 18);
            this.SaveKeyButton.TabIndex = 0;
            this.SaveKeyButton.Text = "Browse";
            this.SaveKeyButton.UseVisualStyleBackColor = true;
            this.SaveKeyButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SaveKeyButton_MouseClick);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel7.Controls.Add(this.SaveOutputPath, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.SaveOutputButon, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(50, 36);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(350, 18);
            this.tableLayoutPanel7.TabIndex = 50;
            // 
            // SaveOutputPath
            // 
            this.SaveOutputPath.BackColor = System.Drawing.Color.White;
            this.SaveOutputPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveOutputPath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.SaveOutputPath.Location = new System.Drawing.Point(2, 2);
            this.SaveOutputPath.Margin = new System.Windows.Forms.Padding(2);
            this.SaveOutputPath.Name = "SaveOutputPath";
            this.SaveOutputPath.Size = new System.Drawing.Size(301, 23);
            this.SaveOutputPath.TabIndex = 22;
            this.SaveOutputPath.TextChanged += new System.EventHandler(this.SaveOutputPath_TextChanged);
            // 
            // SaveOutputButon
            // 
            this.SaveOutputButon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveOutputButon.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.SaveOutputButon.Location = new System.Drawing.Point(305, 0);
            this.SaveOutputButon.Margin = new System.Windows.Forms.Padding(0);
            this.SaveOutputButon.Name = "SaveOutputButon";
            this.SaveOutputButon.Size = new System.Drawing.Size(45, 18);
            this.SaveOutputButon.TabIndex = 0;
            this.SaveOutputButon.Text = "Browse";
            this.SaveOutputButon.UseVisualStyleBackColor = true;
            this.SaveOutputButon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SaveOutputButon_MouseClick);
            // 
            // SaveKeyLabel
            // 
            this.SaveKeyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveKeyLabel.AutoSize = true;
            this.SaveKeyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.SaveKeyLabel.Location = new System.Drawing.Point(16, 59);
            this.SaveKeyLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.SaveKeyLabel.Name = "SaveKeyLabel";
            this.SaveKeyLabel.Size = new System.Drawing.Size(32, 11);
            this.SaveKeyLabel.TabIndex = 49;
            this.SaveKeyLabel.Text = "Save Key:";
            this.SaveKeyLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.SaveKeyLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.SaveKeyLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // SaveLabel
            // 
            this.SaveLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveLabel.AutoSize = true;
            this.SaveLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.SaveLabel.Location = new System.Drawing.Point(8, 41);
            this.SaveLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.SaveLabel.Name = "SaveLabel";
            this.SaveLabel.Size = new System.Drawing.Size(40, 11);
            this.SaveLabel.TabIndex = 48;
            this.SaveLabel.Text = "Save output:";
            this.SaveLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.SaveLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.SaveLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // CarrierLabel
            // 
            this.CarrierLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CarrierLabel.AutoSize = true;
            this.CarrierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.CarrierLabel.Location = new System.Drawing.Point(11, 5);
            this.CarrierLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.CarrierLabel.Name = "CarrierLabel";
            this.CarrierLabel.Size = new System.Drawing.Size(37, 11);
            this.CarrierLabel.TabIndex = 52;
            this.CarrierLabel.Text = "Carrier File:";
            this.CarrierLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.CarrierLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.CarrierLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.inputTextbox, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.rbHideText, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.rbHideFile, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 100);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(400, 260);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // inputTextbox
            // 
            this.inputTextbox.BackColor = System.Drawing.Color.White;
            this.inputTextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputTextbox.Enabled = false;
            this.inputTextbox.Font = new System.Drawing.Font("Arial", 10F);
            this.inputTextbox.Location = new System.Drawing.Point(42, 42);
            this.inputTextbox.Margin = new System.Windows.Forms.Padding(2);
            this.inputTextbox.Multiline = true;
            this.inputTextbox.Name = "inputTextbox";
            this.inputTextbox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.inputTextbox.Size = new System.Drawing.Size(356, 216);
            this.inputTextbox.TabIndex = 52;
            this.inputTextbox.TextChanged += new System.EventHandler(this.inputTextbox_TextChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel5.Controls.Add(this.HideFilePath, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.hideFileButton, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(40, 20);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(360, 20);
            this.tableLayoutPanel5.TabIndex = 51;
            // 
            // HideFilePath
            // 
            this.HideFilePath.BackColor = System.Drawing.Color.White;
            this.HideFilePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HideFilePath.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.HideFilePath.Location = new System.Drawing.Point(2, 2);
            this.HideFilePath.Margin = new System.Windows.Forms.Padding(2);
            this.HideFilePath.Name = "HideFilePath";
            this.HideFilePath.Size = new System.Drawing.Size(311, 23);
            this.HideFilePath.TabIndex = 22;
            this.HideFilePath.TextChanged += new System.EventHandler(this.HideFilePath_TextChanged);
            // 
            // hideFileButton
            // 
            this.hideFileButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.hideFileButton.Location = new System.Drawing.Point(315, 0);
            this.hideFileButton.Margin = new System.Windows.Forms.Padding(0);
            this.hideFileButton.Name = "hideFileButton";
            this.hideFileButton.Size = new System.Drawing.Size(44, 20);
            this.hideFileButton.TabIndex = 15;
            this.hideFileButton.Text = "Open";
            this.hideFileButton.UseVisualStyleBackColor = true;
            this.hideFileButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.hideFileButton_MouseClick);
            // 
            // rbHideText
            // 
            this.rbHideText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbHideText.AutoSize = true;
            this.rbHideText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.rbHideText.Location = new System.Drawing.Point(2, 42);
            this.rbHideText.Margin = new System.Windows.Forms.Padding(2);
            this.rbHideText.Name = "rbHideText";
            this.rbHideText.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbHideText.Size = new System.Drawing.Size(36, 17);
            this.rbHideText.TabIndex = 50;
            this.rbHideText.Text = "Text";
            this.rbHideText.UseVisualStyleBackColor = true;
            this.rbHideText.CheckedChanged += new System.EventHandler(this.rbHideText_CheckedChanged);
            this.rbHideText.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.rbHideText.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // rbHideFile
            // 
            this.rbHideFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbHideFile.AutoSize = true;
            this.rbHideFile.Checked = true;
            this.rbHideFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.rbHideFile.Location = new System.Drawing.Point(2, 22);
            this.rbHideFile.Margin = new System.Windows.Forms.Padding(2);
            this.rbHideFile.Name = "rbHideFile";
            this.rbHideFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbHideFile.Size = new System.Drawing.Size(36, 16);
            this.rbHideFile.TabIndex = 48;
            this.rbHideFile.TabStop = true;
            this.rbHideFile.Text = "File";
            this.rbHideFile.UseVisualStyleBackColor = true;
            this.rbHideFile.CheckedChanged += new System.EventHandler(this.rbHideFile_CheckedChanged);
            this.rbHideFile.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.rbHideFile.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Hide";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // previewTableCont
            // 
            this.previewTableCont.ColumnCount = 2;
            this.previewTableCont.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.previewTableCont.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.previewTableCont.Controls.Add(this.PreviewOptionsTable, 1, 0);
            this.previewTableCont.Controls.Add(this.previewTable, 0, 0);
            this.previewTableCont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewTableCont.Location = new System.Drawing.Point(0, 0);
            this.previewTableCont.Margin = new System.Windows.Forms.Padding(0);
            this.previewTableCont.Name = "previewTableCont";
            this.previewTableCont.RowCount = 1;
            this.previewTableCont.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.previewTableCont.Size = new System.Drawing.Size(800, 360);
            this.previewTableCont.TabIndex = 1;
            // 
            // PreviewOptionsTable
            // 
            this.PreviewOptionsTable.ColumnCount = 1;
            this.PreviewOptionsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PreviewOptionsTable.Controls.Add(this.saveImageHideButton, 0, 1);
            this.PreviewOptionsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PreviewOptionsTable.Location = new System.Drawing.Point(703, 3);
            this.PreviewOptionsTable.Name = "PreviewOptionsTable";
            this.PreviewOptionsTable.RowCount = 2;
            this.PreviewOptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PreviewOptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.PreviewOptionsTable.Size = new System.Drawing.Size(94, 354);
            this.PreviewOptionsTable.TabIndex = 0;
            // 
            // saveImageHideButton
            // 
            this.saveImageHideButton.Enabled = false;
            this.saveImageHideButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveImageHideButton.Location = new System.Drawing.Point(0, 294);
            this.saveImageHideButton.Margin = new System.Windows.Forms.Padding(0);
            this.saveImageHideButton.Name = "saveImageHideButton";
            this.saveImageHideButton.Size = new System.Drawing.Size(94, 60);
            this.saveImageHideButton.TabIndex = 9;
            this.saveImageHideButton.Text = "Process!";
            this.saveImageHideButton.UseVisualStyleBackColor = true;
            this.saveImageHideButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.saveImageHideButton_MouseClick);
            // 
            // previewTable
            // 
            this.previewTable.ColumnCount = 1;
            this.previewTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.previewTable.Controls.Add(this.tableLayoutPanel10, 0, 0);
            this.previewTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewTable.Location = new System.Drawing.Point(0, 0);
            this.previewTable.Margin = new System.Windows.Forms.Padding(0);
            this.previewTable.Name = "previewTable";
            this.previewTable.RowCount = 2;
            this.previewTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.previewTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.previewTable.Size = new System.Drawing.Size(700, 360);
            this.previewTable.TabIndex = 1;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(700, 20);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(2, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 48;
            this.label3.Text = "Input";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(352, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "Output";
            // 
            // TabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "TabControl";
            this.Size = new System.Drawing.Size(800, 720);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.StegOptionsTable.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.previewTableCont.ResumeLayout(false);
            this.PreviewOptionsTable.ResumeLayout(false);
            this.previewTable.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.TextBox inputTextbox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.TextBox HideFilePath;
        private System.Windows.Forms.Button hideFileButton;
        public System.Windows.Forms.RadioButton rbHideText;
        public System.Windows.Forms.RadioButton rbHideFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label aboutLabel;
        public System.Windows.Forms.TextBox infoBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.TextBox SaveKeyPath;
        private System.Windows.Forms.Button SaveKeyButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.TextBox SaveOutputPath;
        private System.Windows.Forms.Button SaveOutputButon;
        private System.Windows.Forms.Label SaveLabel;
        private System.Windows.Forms.Label SaveKeyLabel;
        private System.Windows.Forms.Label CarrierLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        public System.Windows.Forms.TextBox carrierFilePath;
        private System.Windows.Forms.Button carrierFileButton;
        public System.Windows.Forms.Button saveImageHideButton;
        public System.Windows.Forms.TableLayoutPanel StegOptionsTable;
        public System.Windows.Forms.TableLayoutPanel PreviewOptionsTable;
        private System.Windows.Forms.TableLayoutPanel previewTableCont;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TableLayoutPanel previewTable;
    }
}
