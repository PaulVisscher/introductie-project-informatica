/**
 * @file        Output.cs
 * @brief       
 *
 * @details     
 *
 * @author      Tom de Jong
 * 
 * @license     � Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Xml;
using Ionic.Zip;

namespace steganography
{
    /// <summary>
    /// Writes the modified image/audio to a file
    /// </summary>
    class OutputFile : IDisposable
    {
        private string savePath;
        public Stream OutStream;

        public OutputFile()
        {       
        }

        /// <summary>
        /// Garbage collection Dispose callback
        /// </summary>
        public void Dispose()
        {
            this.OutStream.Close();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Sets the file path
        /// </summary>
        /// <param name="filepath">Specifies the file path</param>
        public void SetFilePath(string filepath)
        {
            savePath = filepath;
        }

        /// <summary>
        /// Sets the output stream
        /// </summary>
        /// <param name="save">Specifies the file path</param>
        public void SetStream(string savePath)
        {
            try
            {
                this.OutStream = new FileStream(savePath, FileMode.Create, FileAccess.ReadWrite);
            }
            catch (Exception e)
            {
                Error OutputError = new OutputError();
                OutputError.OnThrowError(e);
            }
        }

        /// <summary>
        /// Save the key file and parse the XML
        /// </summary>
        /// <param name="savePath">String containing the filepath of the XML file to be saved</param>
        /// <param name="fileExtension">The file extension of the hidden file (string)</param>
        /// <param name="originalFileSize">The original file size of the hidden file (long)</param>
        /// <param name="lsbCount">The LSBbits (Quality) used (int)</param>
        /// <param name="scatterData">True if random mode was used, false otherwise (bool)</param>
        /// <param name="useEncryption">True if encryption was used, false otherwise (bool)</param>
        /// <param name="rsaKey">The rsaKey in base64-encoded form (strinf)</param>
        /// <param name="aesKeyLength">The length of the AES key (int)</param>
        public static void Save(string savePath, string fileExtension, long originalFileSize, int lsbCount, bool scatterData, bool useEncryption, string rsaKey, int aesKeyLength)
        {
            try
            {
                using (XmlWriter writer = XmlWriter.Create(savePath))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("config");

                    writer.WriteElementString("fileExtension", fileExtension);
                    writer.WriteElementString("originalFileSize", originalFileSize.ToString());
                    writer.WriteElementString("lsbCount", lsbCount.ToString());
                    writer.WriteElementString("scatterData", scatterData.ToString());
                    writer.WriteElementString("useEncryption", useEncryption.ToString());
                    if (scatterData || useEncryption)
                    {
                        writer.WriteElementString("rsaKey", rsaKey);
                        if (useEncryption)
                            writer.WriteElementString("aesKeyLength", aesKeyLength.ToString());
                    }

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
            catch (Exception e)
            {
                SaveKeyError saveError = new SaveKeyError();
                saveError.OnThrowError(e);
            }
        }

        /// <summary>
        /// Decompresses the input file using zip
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        public void Decompress(Stream zipStream, string filepath)
        {
            zipStream.Seek(0, SeekOrigin.Begin); // make sure the entire file is read
            using (ZipFile zip = ZipFile.Read(zipStream))
            {
                zip.ExtractAll(Path.GetDirectoryName(filepath)); // extract the file in the temp dir
            }
            zipStream.Close();

            this.OutStream.Close();
        }
    }
}