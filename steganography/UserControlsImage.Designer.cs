﻿namespace steganography
{
    partial class UserControlsImage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImagePanel = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePanel)).BeginInit();
            this.SuspendLayout();
            // 
            // ImagePanel
            // 
            this.ImagePanel.BackgroundImage = global::steganography.Properties.Resources.Chess;
            this.ImagePanel.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.ImagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImagePanel.Location = new System.Drawing.Point(0, 0);
            this.ImagePanel.Margin = new System.Windows.Forms.Padding(0);
            this.ImagePanel.Name = "ImagePanel";
            this.ImagePanel.Size = new System.Drawing.Size(300, 300);
            this.ImagePanel.TabIndex = 0;
            this.ImagePanel.TabStop = false;
            this.ImagePanel.SizeChanged += new System.EventHandler(this.ImagePanel_SizeChanged);
            this.ImagePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ImagePanel_Paint);
            this.ImagePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ImagePanel_MouseDown);
            this.ImagePanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ImagePanel_Move);
            this.ImagePanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ImagePanel_MouseUp);
            // 
            // UserControlsImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ImagePanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UserControlsImage";
            this.Size = new System.Drawing.Size(300, 300);
            ((System.ComponentModel.ISupportInitialize)(this.ImagePanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ImagePanel;

    }
}
