﻿/**
 * @file        Steganography.cs
 * @brief       
 *
 * @details     
 *
 * @author      Mick van Duijn
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using NAudio.Wave;

namespace steganography
{
    public enum SampleBitDepth
    {
        Short16,
        Medium24,
        Long32
    }

    interface ISteganography
    {
        void HideData(Stream sourceStream, int LSBCount, bool UseFisherYates, long startBitIndex, bool doResize);
        void RecoverData(Stream targetStream, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex, bool doResize);
        void HideData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long startBitIndex, bool doResize);
        void RecoverData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex, bool doResize);
        long _GetCapacity(Bitmap bmp, ImageParams imageParams, int LSBCount, bool useAlpha);
        long _GetCapacity(string wavePath, int LSBCount);
        void Save(string path, ImageFormat format);
    }

    /// <summary>
    /// Basis for all steganography classes with some methods that are universal for the subclasses
    /// </summary>
    abstract class Steganography : ISteganography
    {
        protected byte[] key;
        protected string password;

        public abstract void HideData(Stream sourceStream, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true);
        public abstract void RecoverData(Stream targetStream, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex = 0, bool doResize = true);
        public abstract void HideData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true);
        public abstract void RecoverData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex = 0, bool doResize = true);
        public abstract void Save(string path, ImageFormat format);
        public abstract void Save(string path, bool suppressDispose = false);
        public abstract long _GetCapacity(Bitmap bmp, ImageParams imageParams, int LSBCount, bool useAlpha);
        public abstract long _GetCapacity(string sourcePath, int LSBCount);

        /// <summary>
        /// Set the key for the PRNG
        /// </summary>
        /// <param name="key">The key in bytes</param>
        /// <param name="password">The password to salt the key with</param>
        public void SetKey(byte[] key, string password)
        {
            this.key = key;
            this.password = password;
        }

        /// <summary>
        /// Set the key for the PRNG
        /// </summary>
        /// <param name="key">The key as string</param>
        /// <param name="password">The password to salt the key with</param>
        public void SetKey(string key, string password)
        {
            this.SetKey(Convert.FromBase64String(key), password);
        }

    }

    /// <summary>
    /// This class is for hiding and recovering data from images
    /// </summary>
    class LSBImageSteganography : Steganography
    {
        public bool UseAlpha;
        public Bitmap Bitmap;
        public ImageParams ImageParams;

        private int stride;
        private BitmapData BMPData;

        /// <summary>
        /// Initialize an LSBImageSteganography object with a bitmap to read from and write to and given some parameters of the image
        /// </summary>
        /// <param name="bmp">The bitmap to read from or write to</param>
        /// <param name="imageParams">The parameters of the image</param>
        public LSBImageSteganography(Bitmap bmp, ImageParams imageParams)
        {
            this.Bitmap = bmp;
            this.UseAlpha = false;

            this.ImageParams = imageParams;
        }

        public LSBImageSteganography()
        {
        }

        /// <summary>
        /// Hide the data from the given stream in the last 'LSBCount' bits of every channel of the bitmap
        /// </summary>
        /// <param name="sourceStream">Stream to read</param>
        /// <param name="LSBCount">The number of bits at the end of every channel to write to</param>
        /// <param name="UseFisherYates">Choose to use the fisher-yates algorithm to shuffle the bits of the data to hide</param>
        public override void HideData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            if (doResize && UseFisherYates)
                hiddenFileData.SetArrayBitLength(LSBImageSteganography.GetCapacity(this.Bitmap, this.ImageParams, LSBCount, this.UseAlpha));

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(ref hiddenFileData, this.key, this.password, ShuffleMode.Shuffle);

            this.LockBits();

            //create a byte with all but the last LSBCount bits set to 1 to later remove the last bits of the channel byte
            byte mask = (byte)~(byte.MaxValue >> 8 - LSBCount);

            int width, height;
            width = this.Bitmap.Width;
            height = this.Bitmap.Height;

            //determine if the user wants to skip the alpha channel, provided that the image has an alpha channel
            int channelEnd = this.ImageParams.Channels; ;

            if (!this.UseAlpha && this.ImageParams.HasAlpha)
                channelEnd -= 1;

            long bitIndex = startBitIndex;
            unsafe
            {
                byte* pix = (byte*)this.BMPData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int channel = 0; channel <  channelEnd; channel++)
                        {
                            long byteLocation = x * this.ImageParams.Channels + y * this.stride + channel + (this.ImageParams.BytesChannel - 1);

                            //calculate the byte to hide in the channel, setting the last LSBCount number of bits of the data byte
                            byte data = 0;
                            for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                            {
                                data |= (byte)((hiddenFileData[bitIndex] ? 1 : 0) << LSBIndex);
                                bitIndex++;
                                if (!(bitIndex < hiddenFileData.Length))
                                {
                                    bitIndex = hiddenFileData.Length;
                                    y = height;
                                    x = width;
                                    channel = channelEnd;
                                    LSBIndex = LSBCount;
                                }
                            }

                            //use the mask to remove the last bits of the channel byte and use data to fill those bits
                            pix[byteLocation] = (byte)((pix[byteLocation] & mask) | data);
                        }
                    }
                }
            }//end unsafe

            this.UnlockBits();
        }

        /// <summary>
        /// Recover the data hidden in the last bits of every channel of the bitmap and write them to the given stream
        /// </summary>
        /// <param name="targetStream">The stream to write to</param>
        /// <param name="LSBCount">The number of bits at the end of every channel to read from</param>
        /// <param name="UseFisherYates">Choose to use the fisher-yates algorithm to deshuffle the bits of the data to recover</param>
        /// <param name="OriginalSize">The original size of the hidden data in bytes</param>
        public override void RecoverData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null&&UseFisherYates)
                throw new Exception("Key not set");

            if (doResize&&UseFisherYates)
                hiddenFileData.SetArrayBitLength(LSBImageSteganography.GetCapacity(this.Bitmap, this.ImageParams, LSBCount, this.UseAlpha));

            this.LockBits();


            int width, height;
            width = this.Bitmap.Width;
            height = this.Bitmap.Height;

            //determine if the user wants to skip the alpha channel, provided that the image has an alpha channel
            int channelEnd = this.ImageParams.Channels; ;

            if (!this.UseAlpha && this.ImageParams.HasAlpha)
                channelEnd -= 1;

            long bitIndex = startBitIndex;
            unsafe
            {
                byte* pix = (byte*)this.BMPData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int channel = 0; channel < channelEnd; channel++)
                        {
                            long byteLocation = x * this.ImageParams.Channels + y * this.stride + channel + (this.ImageParams.BytesChannel - 1);

                            //read the last 'LSBCount' bits of the data byte into the LargeBitArray
                            byte data = pix[byteLocation];
                            for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                            {
                                hiddenFileData[bitIndex] = ((data >> LSBIndex) & 1) == 1;
                                bitIndex++;
                                if (!(bitIndex < hiddenFileData.Length))
                                {
                                    bitIndex = hiddenFileData.Length;
                                    y = height;
                                    x = width;
                                    channel = channelEnd;
                                    LSBIndex = LSBCount;
                                }
                            }
                        }
                    }
                }
            }//end unsafe

            this.UnlockBits();

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(ref hiddenFileData, key, this.password, ShuffleMode.DeShuffle);

            if (doResize)
                hiddenFileData.SetArrayByteLength(OriginalSize);
        }

        public override void HideData(Stream sourceStream, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null&&UseFisherYates)
                throw new Exception("Key not set");

            StreamBitHandler streamHandler;

            long capacity = sourceStream.Length << 3;
            if (doResize)
            {
                if (UseFisherYates)
                    capacity = LSBImageSteganography.GetCapacity(this.Bitmap, this.ImageParams, LSBCount, this.UseAlpha);
                else
                    capacity = sourceStream.Length << 3;
                streamHandler = new StreamBitHandler(sourceStream, capacity);
            }
            else
                streamHandler = new StreamBitHandler(sourceStream, capacity);

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(streamHandler, this.key, this.password, ShuffleMode.Shuffle);

            this.LockBits();

            //create a byte with all but the last LSBCount bits set to 1 to later remove the last bits of the channel byte
            byte mask = (byte)~(byte.MaxValue >> 8 - LSBCount);

            int width, height;
            width = this.Bitmap.Width;
            height = this.Bitmap.Height;

            //determine if the user wants to skip the alpha channel, provided that the image has an alpha channel
            int channelEnd = this.ImageParams.Channels; ;

            if (!this.UseAlpha && this.ImageParams.HasAlpha)
                channelEnd -= 1;

            long bitIndex = startBitIndex;
            byte message = 0;
            unsafe
            {
                byte* pix = (byte*)this.BMPData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int channel = 0; channel <  channelEnd; channel++)
                        {
                            long byteLocation = x * this.ImageParams.Channels + y * this.stride + channel + (this.ImageParams.BytesChannel - 1);


                            //calculate the byte to hide in the channel, setting the last LSBCount number of bits of the data byte
                            byte data = 0;
                            for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                            {
                                int bitPos = (int)bitIndex & 7;
                                if ((bitPos) == 0)
                                    message = streamHandler.GetByte(bitIndex >> 3);

                                if (message != 0)
                                    data |= (byte)(((message >> bitPos) & 1) << LSBIndex);
                                bitIndex++;
                                if (!(bitIndex < capacity))
                                {
                                    bitIndex = capacity;
                                    y = height;
                                    x = width;
                                    channel = channelEnd;
                                    LSBIndex = LSBCount;
                                }
                            }

                            //use the mask to remove the last bits of the channel byte and use data to fill those bits
                            pix[byteLocation] = (byte)((pix[byteLocation] & mask) | data);
                        }
                    }
                }
            }//end unsafe

            this.UnlockBits();
        }

        /// <summary>
        /// Recover the data hidden in the last bits of every channel of the bitmap and write them to the given stream
        /// </summary>
        /// <param name="targetStream">The stream to write to</param>
        /// <param name="LSBCount">The number of bits at the end of every channel to read from</param>
        /// <param name="UseFisherYates">Choose to use the fisher-yates algorithm to deshuffle the bits of the data to recover</param>
        /// <param name="OriginalSize">The original size of the hidden data in bytes</param>
        public override void RecoverData(Stream targetStream, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            long capacity = LSBImageSteganography.GetCapacity(this.Bitmap, this.ImageParams, LSBCount, this.UseAlpha);
            if (doResize)
            {
                if (!UseFisherYates)
                    capacity = OriginalSize << 3;
            }
            else
            {
                capacity = targetStream.Length << 3;
            }
            StreamBitHandler streamHandler = streamHandler = new StreamBitHandler(targetStream, capacity);

            this.LockBits();


            int width, height;
            width = this.Bitmap.Width;
            height = this.Bitmap.Height;

            //determine if the user wants to skip the alpha channel, provided that the image has an alpha channel
            int channelEnd = this.ImageParams.Channels; ;

            if (!this.UseAlpha && this.ImageParams.HasAlpha)
                channelEnd -= 1;

            long bitIndex = startBitIndex;
            byte message = 0;
            unsafe
            {
                byte* pix = (byte*)this.BMPData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int channel = 0; channel < channelEnd; channel++)
                        {
                            long byteLocation = x * this.ImageParams.Channels + y * this.stride + channel + (this.ImageParams.BytesChannel - 1);

                            //read the last 'LSBCount' bits of the data byte into the LargeBitArray
                            byte data = pix[byteLocation];
                            for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                            {
                                int bitPos = (int)(bitIndex & 7);
                                message |= (byte)(((data >> LSBIndex) & 1) << bitPos);


                                if (bitPos == 7)
                                {
                                    if (message != 0)
                                        streamHandler.SetByte(bitIndex >> 3, message);
                                    message = 0;
                                }
                                bitIndex++;
                                if (!(bitIndex < capacity))
                                {
                                    bitIndex = capacity;
                                    y = height;
                                    x = width;
                                    channel = channelEnd;
                                    LSBIndex = LSBCount;
                                }
                            }
                        }
                    }
                }
                streamHandler.SetByte(bitIndex >> 3, message);
            }//end unsafe

            this.UnlockBits();

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(streamHandler, key, this.password, ShuffleMode.DeShuffle);

            if (doResize)
                streamHandler.SetStreamByteLength(OriginalSize);
        }

        /// <summary>
        /// Calculate the maximum capacity the bitmap can hold (overload used for the static method)
        /// </summary>
        /// <param name="bitmap">The bitmap</param>
        /// <param name="imageParams">Parameters of the image</param>
        /// <param name="LSBCount">The number of bits of every channel to hide in</param>
        /// <param name="useAlpha">Choice to use the alpha channel or not</param>
        /// <returns>The maximum capacity the bitmap can hold</returns>
        public override long _GetCapacity(Bitmap bitmap, ImageParams imageParams, int LSBCount, bool useAlpha = false)
        {
            return LSBImageSteganography.GetCapacity(bitmap, imageParams, LSBCount, useAlpha);
        }

        /// <summary>
        /// Calculate the maximum capacity the bitmap can hold
        /// </summary>
        /// <param name="bitmap">The bitmap</param>
        /// <param name="imageParams">Parameters of the image</param>
        /// <param name="LSBCount">The number of bits of every channel to hide in</param>
        /// <param name="useAlpha">Choice to use the alpha channel or not</param>
        /// <returns>The maximum capacity the bitmap can hold</returns>
        public static long GetCapacity(Bitmap bitmap, ImageParams imageParams, int LSBCount, bool useAlpha = false)
        {
            if (bitmap == null)
                return 0;

            long capacity = 0;

            capacity = imageParams.Channels;

            if (imageParams.HasAlpha && !useAlpha)
                capacity -= 1;

            capacity *= bitmap.Width;
            capacity *= bitmap.Height;
            capacity *= LSBCount;

            return capacity;
        }

        public override long _GetCapacity(string wavePath, int LSBCount)
        {
            throw new Exception("Not implemented, use _GetCapacity(Bitmap bitmap, ImageParams imageParams, int LSBCount, bool useAlpha) instead");
        }

        /// <summary>
        /// Lock all the bits of the bitmap
        /// </summary>
        public void LockBits()
        {
            this.BMPData = this.Bitmap.LockBits(new Rectangle(Point.Empty, this.Bitmap.Size), ImageLockMode.ReadWrite, this.Bitmap.PixelFormat);
            this.stride = this.BMPData.Stride;
        }

        /// <summary>
        /// Unlock all the bits of the bitmap
        /// </summary>
        public void UnlockBits()
        {
            this.Bitmap.UnlockBits(BMPData);
        }

        /// <summary>
        /// Save the bitmap
        /// </summary>
        /// <param name="path">Path to save to</param>
        /// <param name="format">Format to save as</param>
        public override void Save(string path, ImageFormat format)
        {
            try
            {
                this.Bitmap.Save(path, format);
            }
            catch { }
        }

        public override void Save(string path, bool suppressDispose = false)
        {
            throw new Exception("Not implemented, use Save(string path, ImageFormat format) instead");
        }
    }

    class LSBVideoSteganography : Steganography
    {
        public static ImageParams DefaultImageParams = new ImageParams(3, 1, false);

        private string sourcePath;
        private AviStream sourceStream;

        public LSBVideoSteganography(string sourcePath)
        {
            this.sourcePath = sourcePath;
        }

        public bool setSourceStream()
        {
            this.sourceStream = new AviStream(OpenMethod.ShareDenyNone, this.sourcePath);
            return this.sourceStream.ReadyToUse;
        }

        public override void HideData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            uint streamLength = sourceStream.streamLength;
            long frameCapacity = LSBVideoSteganography.GetFrameCapacity(sourceStream, LSBCount);

            long capacity = hiddenFileData.Length;

            if (UseFisherYates)
            {
                capacity = LSBVideoSteganography.GetCapacity(this.sourcePath, LSBCount);
                hiddenFileData.SetArrayBitLength(capacity);
                BitMethods.FisherYatesShuffle(ref hiddenFileData, this.key, this.password, ShuffleMode.Shuffle);
            }

            Bitmap bmp;
            LSBImageSteganography image = new LSBImageSteganography();
            image.ImageParams = LSBVideoSteganography.DefaultImageParams;
            image.UseAlpha = false;

            for (int i = 0; i < streamLength; i++)
            {
                bmp = sourceStream.GetFrame(i);

                image.Bitmap = bmp;

                image.HideData(ref hiddenFileData, LSBCount, false, i * frameCapacity, false);

                sourceStream.SetFrame(bmp, i);
            }

            sourceStream.Close();
        }

        public override void RecoverData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long OriginalSize, long startbitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            AviStream sourceStream = new AviStream(OpenMethod.ShareDenyNone, this.sourcePath);

            uint streamLength = sourceStream.streamLength;
            long frameCapacity = LSBVideoSteganography.GetFrameCapacity(sourceStream, LSBCount);

            long capacity = hiddenFileData.Length;
            if (UseFisherYates)
            {
                capacity = LSBVideoSteganography.GetCapacity(this.sourcePath, LSBCount);
                hiddenFileData.SetArrayBitLength(capacity);
            }

            Bitmap bmp;
            LSBImageSteganography image = new LSBImageSteganography();
            image.ImageParams = LSBVideoSteganography.DefaultImageParams;
            image.UseAlpha = false;

            for (int i = 0; i < streamLength; i++)
            {
                bmp = sourceStream.GetFrame(i);

                image.Bitmap = bmp;

                image.RecoverData(ref hiddenFileData, LSBCount, false, 0, i * frameCapacity, false);
            }

            sourceStream.Close();
            sourceStream.Dispose();

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(ref hiddenFileData, this.key, this.password, ShuffleMode.DeShuffle);

            hiddenFileData.SetArrayByteLength(OriginalSize);
        }

        public override void HideData(Stream messageStream, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            this.sourceStream = new AviStream(OpenMethod.ShareDenyNone, this.sourcePath);

            uint streamLength = sourceStream.streamLength;
            long frameCapacity = LSBVideoSteganography.GetFrameCapacity(sourceStream, LSBCount);

            int framesUsed = (int)Math.Ceiling((double)(messageStream.Length << 3) / frameCapacity);

            long capacity = 0;
            if (UseFisherYates)
                capacity = framesUsed * frameCapacity;
            else
                capacity = messageStream.Length << 3;

            StreamBitHandler streamHandler = new StreamBitHandler(messageStream, capacity);

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(streamHandler, this.key, this.password, ShuffleMode.Shuffle);

            Bitmap bmp;
            LSBImageSteganography image = new LSBImageSteganography();
            image.ImageParams = LSBVideoSteganography.DefaultImageParams;
            image.UseAlpha = false;

            for (int i = 0; i < framesUsed; i++)
            {
                bmp = sourceStream.GetFrame(i);

                image.Bitmap = bmp;

                image.HideData(messageStream, LSBCount, false, i * frameCapacity, false);

                sourceStream.SetFrame(bmp, i);
            }

            for (int i = framesUsed; i < streamLength; i++)
                sourceStream.SetFrame(sourceStream.GetFrame(i), i);

            sourceStream.Close();
        }

        public override void RecoverData(Stream targetStream, int LSBCount, bool UseFisherYates, long OriginalSize, long startbitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            AviStream sourceStream = new AviStream(OpenMethod.ShareDenyNone, this.sourcePath);

            uint streamLength = sourceStream.streamLength;
            long frameCapacity = LSBVideoSteganography.GetFrameCapacity(sourceStream, LSBCount);

            int framesUsed = (int)Math.Ceiling((double)(OriginalSize << 3) / frameCapacity);

            long capacity = 0;
            if (UseFisherYates)
                capacity = framesUsed * frameCapacity;
            else
                capacity = OriginalSize << 3;

            StreamBitHandler streamHandler = new StreamBitHandler(targetStream, capacity);

            Bitmap bmp;
            LSBImageSteganography image = new LSBImageSteganography();
            image.ImageParams = LSBVideoSteganography.DefaultImageParams;
            image.UseAlpha = false;

            if (UseFisherYates)
                streamHandler.SetStreamBitLength(framesUsed * frameCapacity);

            for (int i = 0; i < framesUsed; i++)
            {
                bmp = sourceStream.GetFrame(i);

                image.Bitmap = bmp;

                image.RecoverData(targetStream, LSBCount, false, OriginalSize, i * frameCapacity, false);
            }

            sourceStream.Close();
            sourceStream.Dispose();

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(streamHandler, this.key, this.password, ShuffleMode.DeShuffle);

            streamHandler.SetStreamByteLength(OriginalSize);
        }

        public override void Save(string path, bool suppressDispose = false)
        {
            this.sourceStream.SaveCachedVideo(Path.GetDirectoryName(path)+"\\", Path.GetFileName(path));
        }

        public override void Save(string path, ImageFormat format)
        {
        }

        public override long _GetCapacity(string sourcePath, int LSBCount)
        {
            return LSBVideoSteganography.GetCapacity(sourcePath, LSBCount);
        }

        public override long _GetCapacity(Bitmap bmp, ImageParams imageParams, int LSBCount, bool useAlpha)
        {
            throw new Exception("Not implemented, use _GetCapacity(string sourcePath, int LSBCount) instead");
        }

        public static long GetCapacity(string sourcePath, int LSBCount)
        {
            AviStream stream = new AviStream(OpenMethod.Read, sourcePath);

            if (stream.ReadyToUse)
            {
                long frameCapacity = LSBVideoSteganography.GetFrameCapacity(stream, LSBCount);
                uint streamLength = stream.streamLength;

                stream.Close();

                return streamLength * frameCapacity;
            }
            return 0;
        }

        private static long GetFrameCapacity(AviStream stream, int LSBCount)
        {
            Bitmap bmp = stream.GetFrame(0);

            if (bmp != null)
            {
                long frameCapacity = LSBImageSteganography.GetCapacity(bmp, LSBVideoSteganography.DefaultImageParams, LSBCount);

                bmp.Dispose();

                return frameCapacity;
            }
            return 0;
        }
    }

    class LSBAudioSteganography : Steganography
    {
        private string sourcePath;
        private SampleBitDepth targetDepth;

        private TempFile convertedWave = new TempFile(".wav"), tempResult = new TempFile(".wav");

        public LSBAudioSteganography(string sourcePath)
        {
            this.sourcePath = sourcePath;
        }

        public override void HideData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true)
        {
            this.ConvertWave(this.targetDepth);

            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            long capacity = hiddenFileData.Length;

            if (UseFisherYates)
            {
                capacity = LSBAudioSteganography.GetCapacity(this.sourcePath, LSBCount);
                hiddenFileData.SetArrayBitLength(capacity);
                BitMethods.FisherYatesShuffle(ref hiddenFileData, this.key, this.password, ShuffleMode.Shuffle);
            }

            long bitIndex = 0;

            WaveFileReader reader = new WaveFileReader(this.sourcePath);

            tempResult = new TempFile();

            WaveFileWriter writer = new WaveFileWriter(tempResult.file, reader.WaveFormat);

            long sampleCount = reader.SampleCount;
            int channels = reader.WaveFormat.Channels;
            int bytesChannel = reader.WaveFormat.BitsPerSample >> 3;

            byte[] sample = new byte[bytesChannel];
            byte[] samples;

            byte mask = (byte)~(byte.MaxValue >> 8 - LSBCount);
            long end = 0;

            while ((samples = reader.ReadNextSampleFrameBytes()) != null)
            {
                for (int channel = 0; channel < channels; channel++)
                {
                    for (int i = 0; i < bytesChannel; i++)
                    {
                        sample[i] = samples[channel * bytesChannel + i];
                    }

                    for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                    {
                        int bytePos = LSBIndex >> 3;
                        int bitPos = LSBIndex & 7;

                        sample[bytePos] = (byte)((sample[bytePos] & ~(1 << bitPos)) | ((hiddenFileData[bitIndex] ? 1 : 0) << bitPos));
                        bitIndex++;
                        if (!(bitIndex < capacity))
                        {
                            end = reader.Position;
                            reader.Position = reader.Length;
                            channel = channels;
                            LSBIndex = LSBCount;
                        }
                    }

                    writer.WriteSample(sample);
                }
            }

            reader.Position = end;
            float[] sampleFrame;
            while ((sampleFrame = reader.ReadNextSampleFrame()) != null)
                writer.WriteSamples(sampleFrame, 0, sampleFrame.Length);

            reader.Close();
            reader.Dispose();

            writer.Close();
            writer.Dispose();
        }

        public override void RecoverData(Stream targetStream, int LSBCount, bool UseFisherYates, long OriginalSize, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            long capacity = OriginalSize<<3;

            if(UseFisherYates)
                capacity = LSBAudioSteganography.GetCapacity(this.sourcePath, LSBCount);

            StreamBitHandler streamHandler = new StreamBitHandler(targetStream, capacity);

            long bitIndex = 0;

            WaveFileReader reader = new WaveFileReader(this.sourcePath);

            long sampleCount = reader.SampleCount;
            int channels = reader.WaveFormat.Channels;
            int bytesChannel = reader.WaveFormat.BitsPerSample >> 3;

            byte[] sample = new byte[bytesChannel];
            byte[] samples;

            byte message = 0;


            while ((samples = reader.ReadNextSampleFrameBytes()) != null)
            {
                for (int channel = 0; channel < channels; channel++)
                {
                    for (int i = 0; i < bytesChannel; i++)
                    {
                        sample[i] = samples[channel * bytesChannel + i];
                    }

                    for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                    {
                        int bytePos = LSBIndex >> 3;
                        int bitPos = LSBIndex & 7;

                        int sourceBitPos = (int)(bitIndex & 7);

                        message |= (byte)(((sample[bytePos] >> bitPos) & 1) << sourceBitPos);

                        if (sourceBitPos == 7)
                        {
                            streamHandler.SetByte(bitIndex >> 3, message);
                            message = 0;
                        }

                        bitIndex++;
                        if (!(bitIndex < capacity))
                        {
                            channel = channels;
                            LSBIndex = LSBCount;
                            reader.Position = reader.Length;
                        }
                    }
                }
            }

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(streamHandler, this.key, this.password, ShuffleMode.DeShuffle);

            streamHandler.SetStreamByteLength(OriginalSize);

            reader.Close();
            reader.Dispose();
        }

        public override void HideData(Stream sourceStream, int LSBCount, bool UseFisherYates, long startBitIndex = 0, bool doResize = true)
        {
            this.ConvertWave(this.targetDepth);

            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            long capacity = sourceStream.Length << 3;

            StreamBitHandler streamHandler = new StreamBitHandler(sourceStream,capacity);

            if (UseFisherYates)
            {
                capacity = LSBAudioSteganography.GetCapacity(this.sourcePath, LSBCount);
                streamHandler.SetStreamBitLength(capacity);
                BitMethods.FisherYatesShuffle(streamHandler, this.key, this.password, ShuffleMode.Shuffle);
            }

            long bitIndex = 0;

            WaveFileReader reader = new WaveFileReader(this.sourcePath);

            tempResult = new TempFile();

            WaveFileWriter writer = new WaveFileWriter(tempResult.file, reader.WaveFormat);

            long sampleCount = reader.SampleCount;
            int channels = reader.WaveFormat.Channels;
            int bytesChannel = reader.WaveFormat.BitsPerSample >> 3;

            byte[] sample = new byte[bytesChannel];
            byte[] samples;

            byte mask = (byte)~(byte.MaxValue >> 8 - LSBCount);
            byte message = 0;

            long end = 0;

            while ((samples = reader.ReadNextSampleFrameBytes()) != null)
            {
                for (int channel = 0; channel < channels; channel++)
                {
                    for (int i = 0; i < bytesChannel; i++)
                    {
                        sample[i] = samples[channel * bytesChannel + i];
                    }

                    for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                    {
                        int bytePos = LSBIndex >> 3;
                        int bitPos = LSBIndex & 7;

                        int sourceBitPos = (int)(bitIndex & 7);
                        if (sourceBitPos == 0)
                            message = streamHandler.GetByte(bitIndex >> 3);

                        sample[bytePos] = (byte)((sample[bytePos] & ~(1 << bitPos)) | (((message >> sourceBitPos) & 1) << bitPos));
                        bitIndex++;
                        if (!(bitIndex < capacity))
                        {
                            end = reader.Position;
                            reader.Position = reader.Length;
                            channel = channels;
                            LSBIndex = LSBCount;
                        }
                    }

                    writer.WriteSample(sample);
                }
            }

            reader.Position = end;
            float[] sampleFrame;
            while ((sampleFrame = reader.ReadNextSampleFrame()) != null)
                writer.WriteSamples(sampleFrame, 0, sampleFrame.Length);

            reader.Close();
            reader.Dispose();

            writer.Close();
            writer.Dispose();
        }

        public override void RecoverData(ref LargeByteArray hiddenFileData, int LSBCount, bool UseFisherYates, long originalSize, long startBitIndex = 0, bool doResize = true)
        {
            if (this.key == null && UseFisherYates)
                throw new Exception("Key not set");

            hiddenFileData = new LargeByteArray(0);

            long capacity = 0;

            if (UseFisherYates)
                capacity = LSBAudioSteganography.GetCapacity(this.sourcePath, LSBCount);
            else
                capacity = hiddenFileData.Length<<3;

            hiddenFileData.SetArrayBitLength(capacity);

            long bitIndex = 0;

            WaveFileReader reader = new WaveFileReader(this.sourcePath);

            long sampleCount = reader.SampleCount;
            int channels = reader.WaveFormat.Channels;
            int bytesChannel = reader.WaveFormat.BitsPerSample >> 3;

            byte[] sample = new byte[bytesChannel];
            byte[] samples;


            while ((samples = reader.ReadNextSampleFrameBytes()) != null)
            {
                for (int channel = 0; channel < channels; channel++)
                {
                    for (int i = 0; i < bytesChannel; i++)
                    {
                        sample[i] = samples[channel * bytesChannel + i];
                    }

                    for (int LSBIndex = 0; LSBIndex < LSBCount; LSBIndex++)
                    {
                        int bytePos = LSBIndex >> 3;
                        int bitPos = LSBIndex & 7;

                        hiddenFileData[bitIndex] = ((sample[bytePos] >> bitPos) & 1) == 1;
                        bitIndex++;
                        if (!(bitIndex < capacity))
                        {
                            channel = channels;
                            LSBIndex = LSBCount;
                            reader.Position = reader.Length;
                        }
                    }
                }
            }

            if (UseFisherYates)
                BitMethods.FisherYatesShuffle(ref hiddenFileData, this.key, this.password, ShuffleMode.DeShuffle);

            hiddenFileData.SetArrayByteLength(originalSize);

            reader.Close();
            reader.Dispose();
        }

        public void SetTargetBitDepth(SampleBitDepth targetDepth)
        {
            this.targetDepth = targetDepth;
        }

        public override long _GetCapacity(string wavePath, int LSBCount)
        {
            return LSBAudioSteganography.GetCapacity(wavePath, LSBCount);
        }

        public override long _GetCapacity(Bitmap bmp, ImageParams imageParams, int LSBCount, bool useAlpha)
        {
            throw new Exception("Not implemented, use _GetCapacity(string wavePath, int LSBCount) instead");
        }

        public static long GetCapacity(string wavePath, int LSBCount)
        {
            WaveFileReader reader = new WaveFileReader(wavePath);
            int bitDepth = reader.WaveFormat.BitsPerSample;
            long capacity = reader.SampleCount * reader.WaveFormat.Channels * LSBCount;

            reader.Close();
            reader.Dispose();

            return capacity;
        }

        private void ConvertWave(SampleBitDepth targetDepth)
        {
            WaveFileReader reader = new WaveFileReader(this.sourcePath);

            WaveFormat format = reader.WaveFormat;

            int targetBitDepth = 0;

            WaveFormatEncoding encoding = WaveFormatEncoding.IeeeFloat;

            switch (targetDepth)
            {
                case SampleBitDepth.Long32:
                    targetBitDepth = 32;
                    break;
                case SampleBitDepth.Medium24:
                    targetBitDepth = 24;
                    encoding = WaveFormatEncoding.Pcm;
                    break;
                case SampleBitDepth.Short16:
                default:
                    targetBitDepth = 16;
                    encoding = WaveFormatEncoding.Pcm;
                    break;
            }

            WaveFormat newFormat = WaveFormat.CreateCustomFormat(encoding, format.SampleRate, format.Channels, format.AverageBytesPerSecond * targetBitDepth / format.BitsPerSample, format.BlockAlign * targetBitDepth / format.BitsPerSample, targetBitDepth);

            this.convertedWave = new TempFile(".wav");

            WaveFileWriter writer = new WaveFileWriter(this.convertedWave.file, newFormat);

            float[] sampleFrame;

            while ((sampleFrame = reader.ReadNextSampleFrame()) != null)
            {
                for (int i = 0; i < sampleFrame.Length; i++)
                    writer.WriteSample(sampleFrame[i]);
            }

            reader.Close();
            reader.Dispose();

            writer.Close();
            writer.Dispose();

            this.sourcePath = this.convertedWave.file;
        }

        public override void Save(string path, bool suppressDispose = false)
        {
            if (File.Exists(path))
                File.Delete(path);

            File.Copy(tempResult.file, path);
            if (!suppressDispose)
            {
                tempResult.Dispose();
                convertedWave.Dispose();
            }
        }

        public override void Save(string path, ImageFormat format)
        {
            throw new Exception("Not implemented, use Save(string path) instead");
        }
    }
}