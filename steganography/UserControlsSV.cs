﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace steganography
{
    public partial class UserControlsSV : UserControl
    {
        public UserControlsSV brother;

        public UserControlsSV()
        {

            InitializeComponent();
            this.Player.settings.autoStart = false;
            this.Player.uiMode = "mini";

        }

        private void Player_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (this.brother == null)
                return;

            switch (e.newState)
            {
                case 1:    // Stopped
                    brother.Player.Ctlcontrols.stop();
                    break;
                case 2:    // Paused
                    brother.Player.Ctlcontrols.pause();
                    break;
                case 3:    // Playing
                    brother.Player.Ctlcontrols.play();
                    break;
            }
        }
    }
}
