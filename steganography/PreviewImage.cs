﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace steganography
{
    partial class PreviewImage : UserControl
    {
        TabControl tc;

        public PreviewImage(TabControl tc)
        {
            InitializeComponent();
            this.inputImagePanel.brother = this.outputImagePanel;
            this.outputImagePanel.brother = this.inputImagePanel;

            this.tc = tc;
        }

        private void tableLayoutPanel1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        private void tableLayoutPanel1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            this.tc.setCarrierFile(files[0]);
        }
    }
}
