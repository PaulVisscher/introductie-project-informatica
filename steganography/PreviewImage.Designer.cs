﻿namespace steganography
{
    partial class PreviewImage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.outputImagePanel = new steganography.UserControlsImage();
            this.inputImagePanel = new steganography.UserControlsImage();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AllowDrop = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.outputImagePanel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.inputImagePanel, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 320F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(460, 320);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.tableLayoutPanel1_DragDrop);
            this.tableLayoutPanel1.DragEnter += new System.Windows.Forms.DragEventHandler(this.tableLayoutPanel1_DragEnter);
            // 
            // outputImagePanel
            // 
            this.outputImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.outputImagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputImagePanel.Location = new System.Drawing.Point(230, 0);
            this.outputImagePanel.Margin = new System.Windows.Forms.Padding(0);
            this.outputImagePanel.Name = "outputImagePanel";
            this.outputImagePanel.Size = new System.Drawing.Size(230, 320);
            this.outputImagePanel.TabIndex = 5;
            // 
            // inputImagePanel
            // 
            this.inputImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.inputImagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputImagePanel.Location = new System.Drawing.Point(0, 0);
            this.inputImagePanel.Margin = new System.Windows.Forms.Padding(0);
            this.inputImagePanel.Name = "inputImagePanel";
            this.inputImagePanel.Size = new System.Drawing.Size(230, 320);
            this.inputImagePanel.TabIndex = 4;
            // 
            // PreviewImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PreviewImage";
            this.Size = new System.Drawing.Size(460, 320);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public UserControlsImage inputImagePanel;
        public UserControlsImage outputImagePanel;
    }
}
