﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace steganography
{
    /// <summary>
    /// The Crypto class allows you to encrypt and decrypt using both a RSA and a AES key.
    /// (The reason we combine both: RSA can only encrypt a limited bytes and AES is a more simple encryption)
    /// The data will be encrypted with a AES key, the AES key will be encrypted with a RSA key.
    /// The RSA key will be a private and an public key, who can be saved into a XML file.
    /// The encrypted data and key will be returned. And can be decrypted with the Decrypt function.
    /// </summary>
    /// <example>
    /// The Function below allows to Test this Class without the rest of the program
    /// <code>
    ///    string message = "Hello World"\n
    ///    ///Convert string message to Stream data\n
    ///    byte[] textArray = Encoding.UTF8.GetBytes(message);\n
    ///    Stream data = new MemoryStream(textArray);\n
    ///
    ///    Stream encryptData = StartEncrypt(data, "password");\n
    ///
    ///    ///Get Variables for Decryption (AES key Length, RSA Key)\n
    ///    int AESKeyLenght = getEncryptedKeyLength();\n
    ///    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();\n
    ///    rsa.FromXmlString(getRSAPrivateKey());\n
    ///
    ///    encryptData.Position = 0;\n
    ///
    ///    Stream decryptData = StartDecrypt(encryptData, rsa, AESKeyLenght, "password");\n
    ///
    ///    ///Make Decrypted Stream readable\n
    ///    StreamReader readData = new StreamReader(decryptData);\n
    ///    string decryptedData = readData.ReadToEnd();\n
    ///
    ///    Console.WriteLine(decryptedData);\n
    ///    Console.ReadLine();\n
    /// </code>
    /// </example>
    class Crypto
    {
        public RSACryptoServiceProvider RSA;
        public int AESKeyLength;

        /// <summary>
        /// Encrypt Data
        /// First Checks if there is a RSA Public key, or else create one
        /// Create a random AES key
        /// The data Stream will be encrypted with the AES key
        /// </summary>
        /// <param name="data">Data Stream</param>
        /// <param name="passwordAES">Password what will be used for AES encryption</param>
        /// <param name="RSAPublickey">RSA key</param>
        /// <returns>Stream that contains encrypted data + encrypted AES key </returns>
    public Stream StartEncrypt(Stream data, string passwordAES = "standaard", RSACryptoServiceProvider RSAPublickey = null)
        {
            data.Position = 0;

            //Create a key if there is no key included
            if (RSAPublickey == null)
                this.RSA = GenerateRSAKey();
            else
                this.RSA = RSAPublickey;

            RijndaelManaged aesKey = GenerateAESKey(passwordAES);

            TempStream tmpStream = new TempStream();
            Stream EncryptedData = tmpStream.stream;

            EncryptedData = EncryptAES(data, aesKey);

            return EncryptedData;
        }

        /// <summary>
        /// Encrypt Data
        /// Convert a String in a Stream, then call the function StartEncrypt for a stream
        /// </summary>
        /// <param name="dataString">Data String</param>
        /// <param name="passwordAES">Password what will be used for AES encryption</param>
        /// <param name="RSAPublickey">RSA key</param>
        /// <returns>Stream that contains encrypted data + encrypted AES key </returns>
        public Stream StartEncrypt(string dataString, string passwordAES = "standaard", RSACryptoServiceProvider RSAPublickey = null)
        {
            byte[] textArray = Encoding.UTF8.GetBytes(dataString);

            Stream data = new MemoryStream(textArray);

            Stream encryptData = StartEncrypt(data, passwordAES, RSAPublickey);

            return encryptData;

        }

        /// <summary>
        /// Returns the length in bytes of the encrypted AES key
        /// </summary>
        /// <returns>the length of the encrypted AES key</returns>
        public int getEncryptedKeyLength()
        {
            return AESKeyLength;
        }

        /// <summary>
        /// Decrypt a Stream
        /// First split the Stream in two strings, one with the encrypted key
        /// one with the encypted data
        /// Then Decrypt the encrypted AES Key with the given RSA key
        /// Then Decrypt the encrypted data with the decrypted AES key
        /// </summary>
        /// <param name="encryptData">Stream to be decrypt</param>
        /// <param name="RSAPrivatekey">RSA key with which will be encrypted</param>
        /// <param name="lengthEncryptedKey">Lenght of the encrypted Key</param>
        /// <param name="passwordAES">Password what will be used for AES encryption</param>
        /// <returns>Decrypted Stream containing the only decrypted data</returns>
        public Stream StartDecrypt(Stream encryptedData, RSACryptoServiceProvider RSAPrivatekey, int lengthEncryptedKey, string password = "standaard")
        {
            byte[] encryptedKey = new byte[lengthEncryptedKey];

            encryptedData.Read(encryptedKey, 0, lengthEncryptedKey);

            RijndaelManaged aes = new RijndaelManaged();

            HashAlgorithm md5 = new MD5Cng();
            byte[] salt = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
            Rfc2898DeriveBytes ivGen = new Rfc2898DeriveBytes(password, salt, 10000);

            aes.IV = ivGen.GetBytes(aes.BlockSize / 8);
            aes.Key = RSADecryptKey(encryptedKey, RSAPrivatekey);

            encryptedData.Position = 0;
            Stream DecryptedStreamData = DecryptAES(encryptedData, aes, lengthEncryptedKey);

            return DecryptedStreamData;
        }

        /// <summary>
        /// Generate a RSA (Public and Private) Key pair
        /// </summary>
        /// <returns>RSA Key Pair</returns>
        public RSACryptoServiceProvider GenerateRSAKey()
        {
            //Generate a public/private key pair.
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            //Save the public key information to an RSAParameters structure.
            RSAParameters RSAKeyInfo = rsa.ExportParameters(true);

            this.RSA = rsa;

            //Return the key
            return rsa;
        }

        /// <summary>
        /// Returns the current RSACryptoServiceProvider RSA Key that was used by Encryption the AES key
        /// </summary>
        /// <returns>XML string containing ONLY the public key</returns>
        public string getRSAPublicKey()
        {
            return this.RSA.ToXmlString(false);

        }

        /// <summary>
        /// Returns the current RSACryptoServiceProvider RSA Key that need to be used by Decrypting the AES key
        /// </summary>
        /// <returns>XML string containing both public and private key</returns>
        public string getRSAPrivateKey()
        {
            return this.RSA.ToXmlString(true);
        }

        /// <summary>
        /// Generates a Random AES Key
        /// </summary>
        /// <param name="password">Password on wich the aes.IV will be based</param>
        /// <param name="lengthKey">The AES Key length</param>
        /// <returns>An AES Key</returns>
        private RijndaelManaged GenerateAESKey(string password, int lengthKey = 32)
        {
            RijndaelManaged aes = new RijndaelManaged();
                

            aes.Key = new byte[lengthKey];

            //Generate a new key
            TripleDES provider = TripleDES.Create();
            aes.Key = provider.Key;

            HashAlgorithm md5 = new MD5Cng();
            byte[] salt = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
            Rfc2898DeriveBytes ivGen = new Rfc2898DeriveBytes(password, salt, 10000);

            aes.IV = ivGen.GetBytes(aes.BlockSize / 8);

            return aes;
        }

        /// <summary>
        /// Encrypt a Stream with a AES key
        /// </summary>
        /// <param name="data">Stream to be encrypted</param>
        /// <param name="aes">Key to encrypt</param>
        /// <returns>Encrypted String</returns>
        private Stream EncryptAES(Stream data, RijndaelManaged aes)
        {
            data.Position = 0;

            TempStream tmpStream = new TempStream();
            Stream memStream = tmpStream.stream;

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
            CryptoStream stream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write);

            byte[] aesKey = (RSAEncryptKey(aes, this.RSA));
            memStream.Write(aesKey, 0, aesKey.Length);

            int b;
            byte[] buffer = new byte[32768];
            while ((b = data.Read(buffer, 0, buffer.Length)) > 0)
            {
                stream.Write(buffer, 0, b);
            }

            ///Flush The final Block allows the Decrypt function to decrypt
            ///Because AES uses 16 byte blocks
            stream.Flush();
            stream.FlushFinalBlock();

            return memStream;
        }

        /// <summary>
        /// Decrypt a Stream with AES
        /// </summary>
        /// <param name="encryptedData"> Stream that need to be decrypt</param>
        /// <param name="aes">AES key</param>
        /// <returns>Decrypted Stream</returns>
        private Stream DecryptAES(Stream encryptedData, RijndaelManaged aes, int offset = 128)
        {
            encryptedData.Position = offset;

            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
            CryptoStream stream = new CryptoStream(encryptedData, decryptor, CryptoStreamMode.Read);

            int b;
            TempStream tmpStream = new TempStream();
            Stream memStream = tmpStream.stream;

            byte[] buffer = new byte[16];
            while ((b = stream.Read(buffer, 0, buffer.Length)) > 0)
                memStream.Write(buffer, 0, b);

            memStream.Position = 0;
            return memStream;
        }


        /// <summary>
        /// Encrypt a AES Key, with a RSA key
        /// </summary>
        /// <param name="aes">AES RijndealManaged</param>
        /// <param name="RSAPublic">RSA key</param>
        /// <returns>encrypt String</returns>
        private byte[] RSAEncryptKey(RijndaelManaged aes, RSACryptoServiceProvider RSAPublic)
        {
            byte[] keyEncrypted = RSAPublic.Encrypt(aes.Key, false);

            this.AESKeyLength = keyEncrypted.Length;

            return keyEncrypted;
        }

        /// <summary>
        /// Decrypt a byte array containting an encrypted AES key, with a RSA Key
        /// </summary>
        /// <param name="encryptedAes">Byte array which contain the encrypted AES key</param>
        /// <param name="RSAPrivate">RSA Key</param>
        /// <returns>Decrypt String</returns>
        private byte[] RSADecryptKey(byte[] encryptedAes, RSACryptoServiceProvider RSAPrivate)
        {
            //Decrypt the array back
            byte[] aesDecrypted = RSAPrivate.Decrypt(encryptedAes, false);

            //Change array into String
            return aesDecrypted;
        }
    }

}