﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace steganography
{
    public class DoubleBufferedPanel : Panel
    {
        public DoubleBufferedPanel()
        {
            DoubleBuffered = true;
        }
    }
}
