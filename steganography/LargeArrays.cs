﻿/**
 * @file        LargeArrays.cs
 * @brief       
 *
 * @details     
 *
 * @author      Mick van Duijn
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.IO;

namespace steganography
{
    /// <summary>
    /// This class manages large amounts of data in multiple bitarrays
    /// </summary>
    class LargeBitArray
    {
        protected BitArray[] bitBuffer;

        /// <summary>
        /// Returns the number of bits the Large array contains
        /// </summary>
        public long Length
        {
            get
            {
                long length = 0;
                for (int i = 0; i < this.bitBuffer.Length; i++)
                    length += this.bitBuffer[i].Length;
                return length;
            }
        }

        /// <summary>
        /// Returns the number of bytes the Large array contains
        /// </summary>
        public long ByteLength
        {
            get
            {
                long result = this.Length;


                //fast way to divide by 8 and ceil the answer
                bool doCeil = (result & 7) != 0;
                result >>= 3;
                result += doCeil ? 1 : 0;


                return result;
            }
        }

        /// <summary>
        /// Returns the number of arrays the Large array is divided into
        /// </summary>
        public int ArrayCount
        {
            get
            {
                return this.bitBuffer.Length;
            }
        }

        /// <summary>
        /// Return the value of the bit at 'index'. 
        /// This property transforms the long into a index of the desired array and an index inside that array
        /// </summary>
        /// <param name="index">Index of the desired bit</param>
        /// <returns>The value of the bit at 'index'</returns>
        public bool this[long index]
        {
            get
            {
                int arrayIndex = (int)(index / int.MaxValue);
                int bitIndex = (int)(index - arrayIndex * int.MaxValue);
                return this.bitBuffer[arrayIndex][bitIndex];
            }
            set
            {
                int arrayIndex = (int)(index / int.MaxValue);
                int bitIndex = (int)(index - arrayIndex * int.MaxValue);
                this.bitBuffer[arrayIndex][bitIndex] = value;
            }
        }

        /// <summary>
        /// Create an array to hold the desired amount of bits
        /// </summary>
        /// <param name="length">The number of bits the array will be able to hold</param>
        public LargeBitArray(long length)
        {
            this.SetArrayBitLength(length);
        }

        /// <summary>
        /// Sets the bit at 'index' to the desired value
        /// </summary>
        /// <param name="index">Index of the bit to change</param>
        /// <param name="b">Value to change it to</param>
        public void SetBit(long index, bool value)
        {
            this[index] = value;
        }

        /// <summary>
        /// Returns the value of the bit at index 'index'
        /// </summary>
        /// <param name="index">Index of the desired bit</param>
        /// <returns>The bit at 'index'</returns>
        public bool GetBit(long index)
        {
            return this[index];
        }

        /// <summary>
        /// Resize an existing array to a new length to hold the desired amount of bits
        /// </summary>
        /// <param name="newLength">The number of bits the new array size will be able to hold</param>
        public void SetArrayBitLength(long newLength)
        {
            //if the array does not exist, create a dummy array
            if (this.bitBuffer == null)
                this.bitBuffer = new BitArray[0];

            int NBitArraysNew = (int)Math.Ceiling((double)newLength / int.MaxValue);
            int NBitArraysOld = this.ArrayCount;

            Array.Resize(ref this.bitBuffer, NBitArraysNew);

            //add new arrays if necessary
            for (int i = NBitArraysOld; i < NBitArraysNew; i++)
                this.bitBuffer[i] = new BitArray(0);

            //set all but the last to maximum size, the size of the last one becomes the amount of remaining bits
            long maxArrayIndex = NBitArraysNew - 1;
            for (int i = 0; i < NBitArraysNew; i++)
            {
                if (i == maxArrayIndex)
                    this.bitBuffer[maxArrayIndex].Length = ((int)(newLength - (long)(maxArrayIndex) * int.MaxValue));
                else
                    this.bitBuffer[i].Length = int.MaxValue;
            }
        }

        /// <summary>
        /// Swap bit at index1 with bit at index2
        /// </summary>
        /// <param name="index1">Index of the first bit</param>
        /// <param name="index2">Index of the second bit</param>
        public void SwapBit(long index1, long index2)
        {
            bool tempFirst = this[index1];
            this[index1] = this[index2];
            this[index2] = tempFirst;
        }

        /// <summary>
        /// This is for debugging, will be removed in the final version
        /// </summary>
        public void Print()
        {
            for (int i = 0; i < this.Length; i++)
            {
                Debug.Write(this[i] ? 1 : 0);
                if ((i & 7) == 0)
                    Debug.WriteLine("");
            }
            Debug.WriteLine("\n");
        }

        /// <summary>
        /// Used to iterate through all the bit values as booleans
        /// Example:
        /// 
        /// foreach(bool b in LargeBitArray.Bits)
        /// {
        ///     //do stuff
        /// }
        /// </summary>
        public IEnumerable<bool> Bits
        {
            get
            {
                for (long i = 0; i < this.Length; i++)
                    yield return this[i];
            }
        }
    }


    /// <summary>
    /// Extends LargeBitArray so that the inforation can also be accessed as bytes
    /// </summary>
    class LargeByteArray : LargeBitArray
    {
        /// <summary>
        /// Create an array to hold the desired amount of bytes
        /// </summary>
        /// <param name="length">The number of bytes the array will be able to hold</param>
        public LargeByteArray(long length)
            : base(length <<3)
        { 
        }

        /// <summary>
        /// Set the byte at 'index' to the desired value
        /// </summary>
        /// <param name="index">The index of the byte to change</param>
        /// <param name="b">The value to change to</param>
        public void SetByte(long index, byte value)
        {
            index <<= 3;
            for (int i = 0; i < 8; i++)
                this[index + i] = ((value >> i) & 1) == 1;
        }

        /// <summary>
        /// Returns the value of the byte at index 'index'
        /// </summary>
        /// <param name="index">Index of the desired byte</param>
        /// <returns>The byte at 'index'</returns>
        public byte GetByte(long index)
        {
            byte result = 0;
            index <<= 3;
            for (int i = 0; i < 8; i++)
                result |= (byte)((this[index + i] ? 1 : 0) << i);//shift the bit so it represents a power of 2 in the byte
            return result;
        }

        /// <summary>
        /// Resize an existing array to a new length to hold the desired amount of bits
        /// </summary>
        /// <param name="newLength">The number of bytes the new array size will be able to hold </param>
        public void SetArrayByteLength(long newLength)
        {
            this.SetArrayBitLength(newLength << 3);
        }

        /// <summary>
        /// Swap the byte at index1 with the byte at index2
        /// </summary>
        /// <param name="index1">Index of the first byte</param>
        /// <param name="index2">Index of the second byte</param>
        public void SwapByte(long index1, long index2)
        {
            index1 <<= 3;
            index2 <<= 3;
            for (int i = 0; i < 8; i++)
            {
                this.SwapBit(index1 + i, index2 + i);
            }
        }

        /// <summary>
        /// Used to iterate through all the byte values
        /// Example:
        /// 
        /// foreach(byte b in LargeByteArray.Bytes)
        /// {
        ///     //do stuff
        /// }
        /// </summary>
        public IEnumerable<byte> Bytes
        {
            get
            {
                long byteLength = this.ByteLength;
                for (long i = 0; i < byteLength; i++)
                    yield return this.GetByte(i);
            }
        }
    }


    class StreamByteHandler
    {
        public long Length;

        protected Stream Stream;

        public StreamByteHandler(Stream stream, long byteLength)
        {
            this.Stream = stream;
            this.SetStreamByteLength(byteLength);
        }

        public void SetByte(long index, byte value)
        {
            this.Stream.Position = index;
            this.Stream.WriteByte(value);
        }

        public byte GetByte(long index)
        {
            this.Stream.Position = index;

            int result = this.Stream.ReadByte();
            if (result == -1)
            {
                return 0;
            }
            else
            {
                return (byte)result;
            }
        }

        public void SetStreamByteLength(long newLength)
        {
            this.Stream.SetLength(newLength);
            this.Length = newLength*8;
        }

        public void SwapByte(long index1, long index2)
        {
            byte temp = this.GetByte(index1);
            this.SetByte(index1, this.GetByte(index2));
            this.SetByte(index2, temp);
        }

    }

    class StreamBitHandler : StreamByteHandler
    {

        public StreamBitHandler(Stream stream, long bitLength)
            : base(stream, (((bitLength & 7) != 0) ? 1 : 0) + (bitLength >> 3))
        {
            this.SetStreamBitLength(bitLength);
        }

        public bool GetBit(long index)
        {
            int data = this.GetByte(index >> 3); //get the byte corresponding with the bit index by dividing by 8 bitwise
            data >>= (int)(index & 7); //shift the data so the required bit is the last bit
            return (data & 1) == 1;
        }

        public void SetBit(long index, bool value)
        {
            long byteIndex = index >> 3;
            int data = this.GetByte(byteIndex); //get the byte corresponding with the bit index
            data &= ~(1 << (int)(index & 7)); //keep all bits except the bit we want to alter
            data |= (value ? 1 : 0) << (int)(index & 7); //shift the bitvalue to the position of the bit
            this.SetByte(byteIndex, (byte)data); //write the new byte value to the stream
        }

        public void SetStreamBitLength(long newLength)
        {
            long length = newLength;

            //divide the length by 8 and ceil the answer
            bool doCeil = (newLength & 7) != 0;
            newLength >>= 3;
            newLength += doCeil ? 1 : 0;
            this.SetStreamByteLength(newLength);

            this.Length = length;
        }

        public void SwapBit(long index1, long index2)
        {
            long byteIndex1, byteIndex2;
            byte byte1, byte2, bitIndex1, bitIndex2;

            byteIndex1 = index1 >> 3;
            byteIndex2 = index2 >> 3;

            bool areSame = byteIndex1 == byteIndex2;

            bitIndex1 = (byte)(index1 & 7);
            bitIndex2 = (byte)(index2 & 7);

            byte1 = this.GetByte(byteIndex1);

            if (areSame)
                byte2 = byte1;
            else
                byte2 = this.GetByte(byteIndex2);

            int bit1 = (byte1 >> bitIndex1) & 1;

            byte1 = (byte)((byte1 & ~(1 << bitIndex1)) | (((byte2 >> bitIndex2) & 1) << bitIndex1));

            if (areSame)
                byte2 = byte1;

            byte2 = (byte)((byte2 & ~(1 << bitIndex2)) | (bit1 << bitIndex2));

            if (!areSame)
                this.SetByte(byteIndex1, byte1);

            this.SetByte(byteIndex2, byte2);
        }
    }
}
