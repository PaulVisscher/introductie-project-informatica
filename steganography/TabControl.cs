﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace steganography
{
    partial class TabControl : UserControl
    {
        public MedType outputType = MedType.None, inputType = MedType.None;
        public GUIform prnt;
        public Thread processor;
        protected CoverFile cf;        

        public TabControl()
        {
            InitializeComponent();
        }

        ~TabControl()
        {
            if(this.processor != null)
                this.processor.Abort();
        }

        public virtual void setParent(GUIform gf)
        {
            this.prnt = gf;
        }

        private void switchRadiobuttons(bool fileAsInput)
        {
            if (fileAsInput)
            {
                this.inputTextbox.Enabled = false;
                this.hideFileButton.Enabled = true;
                this.HideFilePath.Enabled = true;
            }
            else
            {
                this.inputTextbox.Enabled = true;
                this.hideFileButton.Enabled = false;
                this.HideFilePath.Enabled = false;
            }
        }

        private string GetFilePath(MedType type)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = DialogFilter.getFilter(type);
            ofd.ShowDialog();

            return ofd.FileName;
        }

        private string SetFilePath(MedType type)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = DialogFilter.getFilter(type);
            sfd.ShowDialog();

            return sfd.FileName;
        }

        public virtual void dragDrop(string path) { }

        public virtual void setCarrierFile(string path = "") { }

        public virtual void setPreview(bool saveOutput = false) { }

        public virtual void SaveOutputPath_TextChanged(object sender, EventArgs e) { }

        public virtual void SaveKeyPath_TextChanged(object sender, EventArgs e) { }

        public virtual void HideFilePath_TextChanged(object sender, EventArgs e) { }

        public virtual void inputTextbox_TextChanged(object sender, EventArgs e) { }

        public virtual void saveImageHideButton_MouseClick(object sender, MouseEventArgs e) 
        {
            this.setPreview(true);
        }

        public void control_MouseLeave(object sender, EventArgs e)
        {
            if (this.infoBox.Text != "Processing...")
                this.infoBox.Text = "";
        }

        public void control_MouseEnter(object sender, EventArgs e)
        {
            if (this.infoBox.Text != "Processing...")
                this.infoBox.Text = Config.Info(((Control)sender).Name.ToString());
        }

        public virtual void rbHideFile_CheckedChanged(object sender, EventArgs e)
        {
            this.switchRadiobuttons(true);
        }

        public virtual void rbHideText_CheckedChanged(object sender, EventArgs e)
        {
            this.switchRadiobuttons(false);
        }

        private void carrierFilePath_TextChanged(object sender, EventArgs e)
        {
            setCarrierFile();
        }

        private void hideFileButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.HideFilePath.Text = this.GetFilePath(MedType.None);
        }

        private void SaveKeyButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.SaveKeyPath.Text = this.SetFilePath(MedType.Key);
        }

        private void carrierFileButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.carrierFilePath.Text = this.GetFilePath(this.inputType);
        }

        private void SaveOutputButon_MouseClick(object sender, MouseEventArgs e)
        {
            this.SaveOutputPath.Text = this.SetFilePath(this.outputType);
        }

        private void aboutLabel_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(Config.About,
               "About this program",
               MessageBoxButtons.OK,
               MessageBoxIcon.Information
               );
        }
    }
}
