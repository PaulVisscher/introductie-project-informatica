﻿/**
 * @file        RijndaelPRNG.cs
 * @brief       
 *
 * @details     
 *
 * @author      Mick van Duijn
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Security.Cryptography;

namespace steganography
{
    enum KeyLength
    {
        Bit128 = 16,
        Bit192 = 24,
        Bit256 = 32,
        Unknown
    }

    /// <summary>
    /// This class generates pseudorandom numbers using a key as seed
    /// </summary>
    class RijndaelPRNG
    {
        private ICryptoTransform encryptor;
        private RijndaelManaged rdm;

        /// <summary>
        /// Initialise the PRNG with a byte[] as key
        /// </summary>
        /// <param name="key">The key for the PRNG</param>
        public RijndaelPRNG(byte[] key, string password)
        {
            rdm = new RijndaelManaged();

            this.SetKey(key, password);
        }

        /// <summary>
        /// Initialise the PRNG with a Base64 string as key
        /// </summary>
        /// <param name="s">The key for the PRNG</param>
        public RijndaelPRNG(string s, string password)
            : this(Convert.FromBase64String(s), password)
        { 
        }

        /// <summary>
        /// Generate a random number for the given seed given the key in the constructor
        /// </summary>
        /// <param name="encSeed">The seed to generate for</param>
        /// <returns></returns>
        public long Random(long encSeed)
        {
            byte[] oldBytes = BitConverter.GetBytes(encSeed);
            byte[] eBytes = encryptor.TransformFinalBlock(oldBytes, 0, oldBytes.Length);


            BitMethods.Hash(ref eBytes, 8);

            long result = Math.Abs(BitConverter.ToInt64(eBytes, 0));

            return result;
        }

        /// <summary>
        /// Generate a random number for the given seed in the range 0 to MaxRange
        /// </summary>
        /// <param name="encSeed">The seed to generate for</param>
        /// <param name="MaxRange">The modulo to apply to the generated value</param>
        /// <returns></returns>
        public long RandRange(long encSeed, long MaxRange)
        {
            return this.Random(encSeed) % MaxRange;
        }

        /// <summary>
        /// Set the key of the PRNG to the new value
        /// </summary>
        /// <param name="key">The key as byte[]</param>
        public void SetKey(byte[] key, string password, KeyLength length = KeyLength.Unknown)
        {
            if (password == null)
                password = "";
            int keyLength = (int)length - (int)length % 16;

            Rfc2898DeriveBytes seedGen = new Rfc2898DeriveBytes(password, key, 1000);
            key = seedGen.GetBytes(keyLength);

            byte[] iv = key;
            this.CreateValidLength(ref iv, KeyLength.Bit128);

            rdm.Key = key;
            rdm.IV = iv;

            this.encryptor = rdm.CreateEncryptor();
        }

        /// <summary>
        /// Set the key of the PRNG to the new value
        /// </summary>
        /// <param name="key">Key as Base64 string</param>
        public void SetKey(string key, string password = "")
        {
            if (password == null)
                password = "";
            this.SetKey(Convert.FromBase64String(key), password);
        }

        /// <summary>
        /// Determine a valid length for the byte[] array and resize it to that length
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="newLength"></param>
        private void CreateValidLength(ref byte[] bytes, KeyLength newLength = KeyLength.Unknown)
        {
            int size;
            if (newLength == KeyLength.Unknown)
            {
                int length = bytes.Length;

                if (length >= (int)KeyLength.Bit256)
                    size = (int)KeyLength.Bit256;
                else if (length >= (int)KeyLength.Bit192)
                    size = (int)KeyLength.Bit192;
                else if (length >= (int)KeyLength.Bit128)
                    size = (int)KeyLength.Bit128;
                else
                    throw new Exception("Invalid byte[] length");
            }
            else
            {
                size = (int)newLength;
            }

            BitMethods.Hash(ref bytes, size);
        }
    }
}