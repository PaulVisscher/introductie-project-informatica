﻿/**
 * @file        Input.cs
 * @brief       
 *
 * @details     
 *
 * @author      Tom de Jong
 * 
 * @license     © Ikiwisi 2012 
 *              This file is part of Steganographer.
 *
 *              Steganographer is free software: you can redistribute it and/or modify
 *              it under the terms of the GNU General Public License as published by
 *              the Free Software Foundation, either version 3 of the License, or
 *              (at your option) any later version.
 *
 *              Steganographer is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public License
 *              along with Steganographer.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using Ionic.Zip;
using System.Xml;

namespace steganography
{
    /// <summary>
    /// Filter for the file dialogs in the GUI
    /// </summary>
    static class DialogFilter
    {
        /// <summary>
        /// Sets the extension and the stream of the input file
        /// </summary>
        /// <param name="type">The media type (Audio/Video/Image/Key input and output) (MedType)</param>
        /// <returns> Appropriate filter (string)</returns>
        public static string getFilter(MedType type)
        {
            string filter = "";
            switch (type)
            {
                case MedType.Audio:
                    filter = "Audio files (*.wav) | *.wav| All formats (*.*) |*.*";
                    break;

                case MedType.AudioOut:
                    filter = "*.wav| *.wav| All formats (*.*) |*.*";
                    break;
                case MedType.Image:
                    filter = "Image files (*.bmp, *.jpg, *.jpeg, *.png, *.tiff) | *.bmp; *.jpg; *.jpeg; *.png; *.tiff| All formats (*.*) |*.*";
                    break;
                case MedType.ImageOut:
                    filter = "*.png| *.png|*.bmp| *.bmp|*.tiff | *.tiff";
                    break;
                case MedType.Video:
                    filter = " Avi file (*.avi) | *.avi|All formats (*.*) |*.*";
                    break;
                case MedType.Key:
                    filter = "Xml file (*.xml) | *.xml| All formats (*.*) |*.*";
                    break;
                case MedType.None:
                    filter = "All formats (*.*) |*.*";
                    break;
                case MedType.All:
                    filter = "Supported files (*.bmp, *.png, *.tiff, *.flac, *.wma, *.wav, *.avi) | *.bmp; *.png; *.tiff; *.flac; *.wma; *.wav; *.avi";
                    break;
            }

            return filter;
        }

        /// <summary>
        /// Help method to set the media type based on the extensions
        /// </summary>
        /// <param name="filepath">String containing the filepath of the file</param>
        /// <param name="type">The media type (Audio/Video/Image/Key input and output)</param>
        /// <returns> True if it matches the given Media type, otherwise false (bool)</returns>
        public static bool FileMatchesPatern(string filepath, MedType type)
        {
            string[] pattern = {};

            switch (type)
            {
                case MedType.Image:
                    pattern = new string[] { ".bmp", ".png", ".tiff", ".jpg", ".jpeg" };
                    break;

                case MedType.Audio:
                    pattern = new string[] { ".flac", ".wma", ".mp3", ".wav" };
                    break;

                case MedType.Video:
                    pattern = new string[] { ".avi" };
                    break;
                case MedType.All:
                    pattern = new string[] { ".bmp", ".png", ".tiff", ".jpg", ".jpeg", ".flac", ".wma", ".mp3", ".wav", ".avi" };
                    break;
            }
            bool matches = false;

            string path = Path.GetExtension(filepath);
            path = path.ToLower();
            for (int i = 0; i < pattern.Length && !matches; i++ )
            {
                if (path == pattern[i])
                    matches = true;
            }

            return matches;
        }
    }

    /// <summary>
    /// Provides information on the hidden file
    /// </summary>
    class InputFile : IDisposable
    {
        public Stream FileBuffer;
        public string FileExtension;
        private string tempPath;
        private string zipPath;

        public bool UseEncryption = true;

        public string RSAKey;
        public int AESKeyLength;

        public long OriginalFileSize;

        public string FilePath;
        public bool UseCompression = false;

        public InputFile()
        { }

        /// <summary>
        /// Alternative to SetFile, is needed when hiding files directly after each other
        /// </summary>
        /// <param name="filepath">String containing the filepath of the file</param>
        public void Reset(string filepath)
        {
            TempFile tempF = new TempFile("." + this.FileExtension.ToString().ToLower());
            this.FileBuffer = CreateBufferedStream(tempF.file);
            this.copyToBufferedStream(filepath);
            this.FileBuffer.Position = 0;

            this.tempPath = tempF.file;
            this.zipPath = getZipFilepath(tempPath);
        }

        /// <summary>
        /// Sets the extension and the stream of the input file
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        public void SetFile(string filepath)
        {
            this.FilePath = filepath;

            this.FileExtension = getExtension(filepath);

            TempFile temp = new TempFile("."+this.FileExtension.ToString().ToLower());

            this.FileBuffer = CreateBufferedStream(temp.file);
            this.copyToBufferedStream(filepath);
            this.FileBuffer.Position = 0;

            this.tempPath = temp.file;
            this.zipPath = getZipFilepath(tempPath);
        }

        /// <summary>
        /// Garbage collection Dispose callback
        /// </summary>
        public void Dispose()
        {
            this.FileBuffer.Close();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the size of the input file
        /// </summary>
        /// <returns>The size of the file (long)</returns>
        public long GetFileSize()
        {
            return this.FileBuffer.Length;
        }

        /// <summary>
        /// Compresses the input file using zip
        /// </summary>
        /// <remarks>Could not be implemented in the GUI</remarks>
        public void Compress()
        {
            Stream zipOutput = CreateBufferedStream(zipPath);

            using (ZipFile zip = new ZipFile())
            {
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.Level8; 
                this.FileBuffer.Seek(0, SeekOrigin.Begin); // make sure to zip the entire file
                ZipEntry e = zip.AddEntry(Path.GetFileName(this.FilePath), this.FileBuffer); // add the file to be zipped, using the filestream and filename
                zip.Save(zipOutput);
            }

            this.FileBuffer.Close();
            deleteFile(tempPath); // delete the temp copy of the uncompressed file

            this.FileBuffer = zipOutput; // the filestream now points to the compressed version of the file
        }

        /// <summary>
        /// Load the key file and parse the XML
        /// </summary>
        /// <param name="loadPath">String containing the filepath of the XML file to be loaded</param>
        public virtual void Load(string loadPath)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(loadPath);

                XmlNode node = doc.DocumentElement.SelectSingleNode("/config/originalFileSize");
                this.OriginalFileSize = long.Parse(node.InnerText);

                node = doc.DocumentElement.SelectSingleNode("/config/fileExtension");
                string extStr = node.InnerText;
                this.FileExtension = node.InnerText;

                node = doc.DocumentElement.SelectSingleNode("/config/useEncryption");
                this.UseEncryption = bool.Parse(node.InnerText);

                node = doc.DocumentElement.SelectSingleNode("/config/rsaKey");
                if (node != null) // there's not always a RSA-key present
                {
                    this.RSAKey = node.InnerText;

                    node = doc.DocumentElement.SelectSingleNode("/config/aesKeyLength");
                    if (node != null) // only there when encryption is used
                    {
                        this.UseEncryption = true;
                        this.AESKeyLength = int.Parse(node.InnerText);
                    }
                }
            }
            catch (Exception e)
            {
                ReadKeyError readError = new ReadKeyError(); // the key file was invalid
                readError.OnThrowError(e);
            }
        }

        /// <summary>
        /// Create a copy of the input file in the temp dir
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        /// <param name="deleteOnClose">Necessary for cleaning up the files in temp at the right time</param>
        /// <returns>FileStream of the input file in the temp dir (Stream)</returns>
        public static Stream CreateBufferedStream(string filepath, bool deleteOnClose = false)
        {
            string tempPath = getTempFilepath(filepath);

            try
            {
                FileStream bufferedFile;

                if(deleteOnClose) // clean up the temp files at the right time
                    bufferedFile = new FileStream(tempPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite, 32768, FileOptions.DeleteOnClose);
                else
                    bufferedFile = new FileStream(tempPath, FileMode.Create, FileAccess.ReadWrite);

                return bufferedFile;
            }
            catch (Exception e)
            {
                InputError streamError = new InputError(); 
                streamError.OnThrowError(e);
                return null;
            }
        }

        /// <summary>
        /// Copy the contents of the input file to the bufferedStream
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        private void copyToBufferedStream(string filepath)
        {
            FileStream fileContent = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            fileContent.Seek(0, SeekOrigin.Begin); // make sure the entire file is copied
            fileContent.CopyTo(this.FileBuffer);
            fileContent.Close();
        }


        /// <summary>
        /// Create a temporary path based on the filename of the input file
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        private static string getTempFilepath(string filepath)
        {
            string tempPath = System.IO.Path.GetTempPath();
            return tempPath + Path.GetFileName(filepath);
        }

        /// <summary>
        /// Create a zip-filepath based on the filename of the input file
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        private static string getZipFilepath(string filepath)
        {
            return Path.GetFileNameWithoutExtension(filepath) + ".zip";
        }

        /// <summary>
        /// Deletes a file
        /// </summary>
        /// <param name="filepath">The filepath pointing to the file to be deleted</param>
        /// <remarks>Only used in not-implemented Compress()</remarks>
        private static void deleteFile(string filepath)
        {
            try
            {
                if (File.Exists(filepath))
                    File.Delete(filepath);
            }
            catch (IOException)
            {
                // for some reason the file was in use or had an open file handle
                // there's not much we can do about this
                // so we just continue executing the rest of the program
            }
            catch (UnauthorizedAccessException)
            {
                // we don't have the required permissions to delete the file
                // too bad, but we go right along
            }
        }

        /// <summary>
        /// Gets the extension of the input file
        /// </summary>
        /// <param name="filepath">The filepath of the input file</param>
        /// <returns>The extension of the input file (Enum Extension)</returns>
        private static string getExtension(string filepath)
        {
            string extStr = Path.GetExtension(filepath);
            extStr = extStr.ToLower();
            extStr = extStr.Remove(0, 1); // remove leading . in the extension
            return extStr;
        }

        /// <summary>
        /// Decodes a base64 string (necessary when dealing RSA keys)
        /// </summary>
        /// <param name="encodedData">String containing the base64-encoded data</param>
        /// <returns>Decoded data (string)</returns>
        /// <remarks>As the key is given in XML format by default and saved in a XML-file, the key is encoded in order to avoid XML-markup conflicts</remarks>
        public static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            return System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);
        }
    }
}