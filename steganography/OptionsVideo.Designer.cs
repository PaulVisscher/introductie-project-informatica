﻿namespace steganography
{
    partial class OptionsVideo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.modeLabel = new System.Windows.Forms.Label();
            this.QualityLabel = new System.Windows.Forms.Label();
            this.steganoType = new System.Windows.Forms.ComboBox();
            this.LSBbits = new System.Windows.Forms.NumericUpDown();
            this.useEncryption = new System.Windows.Forms.CheckBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LSBbits)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.modeLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.QualityLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.steganoType, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.LSBbits, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.useEncryption, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.passwordLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.passwordBox, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(512, 276);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // modeLabel
            // 
            this.modeLabel.AutoSize = true;
            this.modeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.modeLabel.Location = new System.Drawing.Point(2, 5);
            this.modeLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.modeLabel.Name = "modeLabel";
            this.modeLabel.Size = new System.Drawing.Size(51, 11);
            this.modeLabel.TabIndex = 36;
            this.modeLabel.Text = "Mode:";
            this.modeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.modeLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.modeLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // QualityLabel
            // 
            this.QualityLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.QualityLabel.AutoSize = true;
            this.QualityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.QualityLabel.Location = new System.Drawing.Point(11, 23);
            this.QualityLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.QualityLabel.Name = "QualityLabel";
            this.QualityLabel.Size = new System.Drawing.Size(42, 11);
            this.QualityLabel.TabIndex = 37;
            this.QualityLabel.Text = "Quality:";
            this.QualityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.QualityLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.QualityLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // steganoType
            // 
            this.steganoType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.steganoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.steganoType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.steganoType.FormattingEnabled = true;
            this.steganoType.Items.AddRange(new object[] {
            "Random",
            "Sequential"});
            this.steganoType.Location = new System.Drawing.Point(57, 5);
            this.steganoType.Margin = new System.Windows.Forms.Padding(2, 5, 8, 2);
            this.steganoType.Name = "steganoType";
            this.steganoType.Size = new System.Drawing.Size(447, 21);
            this.steganoType.TabIndex = 39;
            this.steganoType.SelectedIndexChanged += new System.EventHandler(this.steganoType_SelectedIndexChanged);
            // 
            // LSBbits
            // 
            this.LSBbits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LSBbits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.LSBbits.Location = new System.Drawing.Point(57, 23);
            this.LSBbits.Margin = new System.Windows.Forms.Padding(2, 5, 8, 2);
            this.LSBbits.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.LSBbits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LSBbits.Name = "LSBbits";
            this.LSBbits.Size = new System.Drawing.Size(447, 20);
            this.LSBbits.TabIndex = 40;
            this.LSBbits.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LSBbits.ValueChanged += new System.EventHandler(this.LSBbits_ValueChanged);
            // 
            // useEncryption
            // 
            this.useEncryption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.useEncryption.AutoSize = true;
            this.useEncryption.Checked = true;
            this.useEncryption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useEncryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.useEncryption.Location = new System.Drawing.Point(57, 56);
            this.useEncryption.Margin = new System.Windows.Forms.Padding(2);
            this.useEncryption.Name = "useEncryption";
            this.useEncryption.Size = new System.Drawing.Size(101, 14);
            this.useEncryption.TabIndex = 38;
            this.useEncryption.Text = " Use Encryption";
            this.useEncryption.UseVisualStyleBackColor = true;
            this.useEncryption.CheckedChanged += new System.EventHandler(this.useEncryption_CheckedChanged);
            this.useEncryption.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.useEncryption.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // passwordLabel
            // 
            this.passwordLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.passwordLabel.Location = new System.Drawing.Point(6, 41);
            this.passwordLabel.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(47, 11);
            this.passwordLabel.TabIndex = 42;
            this.passwordLabel.Text = "Password:";
            this.passwordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.passwordLabel.MouseEnter += new System.EventHandler(this.control_MouseEnter);
            this.passwordLabel.MouseLeave += new System.EventHandler(this.control_MouseLeave);
            // 
            // passwordBox
            // 
            this.passwordBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordBox.Location = new System.Drawing.Point(57, 41);
            this.passwordBox.Margin = new System.Windows.Forms.Padding(2, 5, 8, 2);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.PasswordChar = '*';
            this.passwordBox.Size = new System.Drawing.Size(447, 20);
            this.passwordBox.TabIndex = 43;
            this.passwordBox.TextChanged += new System.EventHandler(this.passwordBox_TextChanged);
            // 
            // OptionsVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "OptionsVideo";
            this.Size = new System.Drawing.Size(512, 276);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LSBbits)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label modeLabel;
        private System.Windows.Forms.Label QualityLabel;
        private System.Windows.Forms.CheckBox useEncryption;
        private System.Windows.Forms.ComboBox steganoType;
        public System.Windows.Forms.NumericUpDown LSBbits;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox passwordBox;

    }
}
