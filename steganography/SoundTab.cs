﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace steganography
{
    partial class SoundTab : TabControl
    {
        public PreviewSV prevSound;
        public OptionsSound optSound;

        public SoundTab()
        {
            InitializeComponent();

            this.optSound = new OptionsSound(this);
            this.prevSound = new PreviewSV(this);

            this.previewTable.Controls.Add(this.prevSound, 0, 1);
            this.StegOptionsTable.Controls.Add(this.optSound, 0, 0);

            this.outputType = MedType.AudioOut;
            this.inputType = MedType.Audio;
        }

        public override void setParent(GUIform par)
        {
            base.setParent(par);
            this.cf = this.prnt.Core.Caudio;
        }

        public override void setCarrierFile(string path = "")
        {
            if (path == "")
                path = this.carrierFilePath.Text;
            else
                this.carrierFilePath.Text = path;

            if (File.Exists(path))
            {
                if (DialogFilter.FileMatchesPatern(path, this.inputType))
                {
                    this.cf.SetFile(path);

                    this.prevSound.inputPanel.Player.URL = path;
                    this.setPreview();
                }
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void SaveOutputPath_TextChanged(object sender, EventArgs e)
        {
            string path = this.SaveOutputPath.Text;

            if (Directory.Exists(Path.GetDirectoryName(path)))
            {
                this.prnt.Core.Caudio.OutputPath = path;
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void SaveKeyPath_TextChanged(object sender, EventArgs e)
        {
            string path = this.SaveKeyPath.Text;

            if (Directory.Exists(Path.GetDirectoryName(path)))
            {
                this.prnt.Core.Caudio.SavePath = path;
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void HideFilePath_TextChanged(object sender, EventArgs e)
        {
            string path = this.HideFilePath.Text;

            if (File.Exists(path))
            {
                this.prnt.Core.Caudio.HiddenFile.SetFile(path);
                this.setPreview();
            }

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void inputTextbox_TextChanged(object sender, EventArgs e)
        {
            this.prnt.Core.Caudio.HiddenText = this.inputTextbox.Text;

            if (this.enableProcess())
                base.saveImageHideButton.Enabled = true;
        }

        public override void rbHideFile_CheckedChanged(object sender, EventArgs e)
        {
            base.rbHideFile_CheckedChanged(sender, e);
            this.prnt.Core.Caudio.UseText = this.rbHideText.Checked;
        }

        public override void rbHideText_CheckedChanged(object sender, EventArgs e)
        {
            base.rbHideText_CheckedChanged(sender, e);
            this.prnt.Core.Caudio.UseText = this.rbHideText.Checked;
        }

        public override void setPreview(bool saveOutput = false)
        {
            if (this.cf.HiddenFile.FileBuffer != null && this.cf.HiddenFile.FileBuffer.CanSeek)
                this.infoBox.Text = "Processing...";

            if (this.processor != null && this.processor.IsAlive)
                this.processor.Abort();

            this.processor = new Thread(new ParameterizedThreadStart(threadPreview));
            this.processor.Start(new ParamThreadObj(this.prnt.Core, this.inputType, saveOutput));
        }

        private bool enableProcess()
        {
            if (this.cf.FilePath != "" &&
                this.prnt.Core.Caudio.SavePath != null &&
                this.prnt.Core.Caudio.OutputPath != null &&
                this.prnt.Core.Caudio.OutputPath != "recoveredFile" &&
                ((this.cf.HiddenFile.FileBuffer != null && this.cf.HiddenFile.FileBuffer.CanSeek) || this.prnt.Core.Caudio.HiddenText != null))
                return true;
            return false;
        }

        private void threadPreview(object p)
        {
            string path = "";
            ParamThreadObj par = (ParamThreadObj)p;

            path = par.core.GeneratePreview(par.type, !par.saveOutput);

            if (path != "")
            {
                this.prevSound.outputPanel.Player.URL = path;
                if (par.saveOutput)
                {
                    System.Media.SystemSounds.Beep.Play();
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.infoBox.Text = "Finished!";

                    });
                }
                else
                {
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.infoBox.Text = "Done creating preview.";
                    });
                }
            }
        }
    }

    class ParamThreadObj
    {
        public Core core;
        public bool saveOutput;
        public MedType type;

        public ParamThreadObj(Core cr, MedType type, bool saveOutput)
        {
            this.core = cr;
            this.type = type;
            this.saveOutput = saveOutput;
        }
    }
}
