﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace steganography
{
    public partial class UserControlsImage : UserControl
    {
        public UserControlsImage brother;
        public Point oldMousePos, delta, imagePos;
        public bool isDragging = false;

        private Image inputImage;
        
        public UserControlsImage()
        {
            InitializeComponent();

            this.imagePos = new Point(0, 0);
        }

        public Image Image
        {
            set
            {
                this.SetInputImage(value);
            }
        }

        public void SetInputImage(Image img)
        {
            this.inputImage = img;
            this.imagePos = new Point(0, 0);
            this.ImagePanel.Invalidate();
        }

        public Image GetInputImage()
        {
            return this.inputImage;
        }

        public void Synch()
        {
            if (this.brother == null)
                return;

            this.brother.oldMousePos = this.oldMousePos;
            this.brother.delta = this.delta;
            this.brother.imagePos = this.imagePos;
            this.brother.ImagePanel.Invalidate();
            this.brother.ImagePanel.Update();
        }

        private void fixPosition()
        {
            if (this.inputImage == null)
                return;

            int minX = -this.inputImage.Width + this.Width;
            int minY = -this.inputImage.Height + this.Height;

            if (this.imagePos.X < minX)
                this.imagePos.X = minX;
            if (this.imagePos.Y < minY)
                this.imagePos.Y = minY;
            if (this.imagePos.X > 0)
                this.imagePos.X = 0;
            if (this.imagePos.Y > 0)
                this.imagePos.Y = 0;
        }

        private void ImagePanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            this.ImagePanel.Select();

            this.oldMousePos = e.Location;
            this.isDragging = true;
        }

        private void ImagePanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                this.isDragging = false;
        }

        private void ImagePanel_Move(object sender, MouseEventArgs e)
        {
            if (!this.isDragging)
                return;

            this.delta = new Point(e.X - this.oldMousePos.X, e.Y - this.oldMousePos.Y);
            this.oldMousePos = e.Location;
            this.imagePos.X += this.delta.X;
            this.imagePos.Y += this.delta.Y;
            this.fixPosition();
            this.ImagePanel.Invalidate();
            this.ImagePanel.Update();

            this.Synch();
        }

        private void ImagePanel_Paint(object sender, PaintEventArgs e)
        {
            if (this.inputImage == null)
                return;

            Graphics g = e.Graphics;
            g.InterpolationMode = InterpolationMode.Low;
            g.DrawImage(this.inputImage, this.imagePos);
        }

        private void ImagePanel_SizeChanged(object sender, EventArgs e)
        {
            this.ImagePanel.Invalidate();
        }

    }
}
